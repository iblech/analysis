\chapter{Metrische Räume und stetige Abbildungen}
\label{chap:metric_spaces}

\section{Metrische Räume}
\label{sec:metric_spaces}

\begin{definition}
  Seien $E$ eine Menge und $d\colon E \times E \to \set R$ eine
  Funktion.  Dann heißt $d$ eine \emph{Metrik} oder
  \emph{Distanzfunktion} auf $E$ und das Paar $(E, d)$ ein
  \emph{metrischer Raum}, wenn für $d$ die folgenden Axiome gelten:
  \begin{axiomlist}[label=(M\arabic*),start=0]
  \item
    $d \ge 0$, d.\,h.~$\forall p, q \in E\colon d(p, q) \geq 0$.
  \item
    $\forall p, q \in E\colon \bigl(d(p, q) = 0 \iff p = q\bigr)$,
  \item
    $\forall p, q \in E\colon d(p, q) = d(q, p)$,
  \item
    \label{it:metric_triangle}
    $\forall p, q, z \in E\colon d(p, z) \leq d(p, q) + d(q, z)$.
  \end{axiomlist}
  Die Ungleichung in \ref{it:metric_triangle} heißt auch die
  \emph{Dreiecksungleichung} für metrische Räume.

  Anstatt zu sagen, daß $(E, d)$ ein metrischer Raum ist, werden wir
  auch sagen, daß $E$ ein metrischer Raum mit Metrik $d$ sei oder noch
  kürzer, daß $E$ ein metrischer Raum ist, dessen Metrik wir dann in
  der Regel mit $d$ bezeichnen.
\end{definition}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    $E \coloneqq \set R$ mit $d(a, b) \coloneqq \abs{b - a}$.
  \item $E \coloneqq \set R^2$ mit
    $d(p, q) \coloneqq \sqrt{(q_1 - p_1)^2 + (q_2 - p_2)^2}$.  Dieses
    ist nicht die einzige sinnvolle Metrik auf $\set R^2$ und wir werden
    in Abschnitt \ref{sec:metric_product} eine andere als diese
    \emph{euklidische} Metrik auf $\set R^2$ einführen, aber auch
    zeigen, daß der Stetigkeitsbegriff für beide Metriken übereinstimmt.
  \end{enumerate}
\end{examples*}

\begin{definition}
  Ist $E$ ein metrischer Raum, so definieren wir für jedes $p \in E$ und $\epsilon \in \set R_+$ die
  \emph{$\epsilon$-Umgebung}
  \[
  U_\epsilon(p) \coloneqq \{q \in E \mid d(p, q) < \epsilon\}.
  \]
\end{definition}
Offensichtlich stimmt diese Definition für $E = \set R$ mit der
Definition in Abschnitt \ref{sec:epsilon} überein.  Wie dort, gilt auch allgemeiner hier:

\begin{namedthm}{Hausdorffsche Trennungseigenschaft}
  Sei $E$ ein metrischer Raum.  Dann gilt:
  \[
  \forall p, q \in E\colon \bigl(p \neq q \implies \exists \epsilon \in \set R_+\colon
  U_\epsilon(p) \cap U_\epsilon(q) = \emptyset\bigr).
  \]
\end{namedthm}

\section{Stetige Abbildungen}

\label{sec:continuous}

Im folgenden betrachten wir zwei metrische Räume $E$ und $\tilde E$,
deren $\epsilon$-Umgebungen wir zur Verdeutlichung mit $U_\epsilon(p)$
und $\tilde U_\epsilon(q)$ ausnahmsweise unterschiedlich bezeichnen.

\begin{definition*}
  Sei $f\colon E \to \tilde E$ eine Abbildung, und sei $p_0 \in E$ ein Punkt in $E$.
  \begin{enumerate}
  \item
    \label{it:continuous}
    Es heißt $f$ in $p_0$ \emph{stetig}, falls $\forall \epsilon \in \set R_+\,
    \exists \delta \in \set R_+ \colon f(U_\delta(p_0)) \subseteq \tilde U_\epsilon(f(p_0))$.
  \item
    Es heißt $f$ \emph{stetig}, falls $f$ in allen Punkten $p \in E$ stetig ist.
  \end{enumerate}
\end{definition*}

Anschaulich gesprochen wird in \ref{it:continuous} folgendes
formuliert: Mit $\tilde U_\epsilon(f(p_0))$ wird ein
"`Toleranzbereich"' um den Wert $f(p_0)$ vorgegeben.  Stetigkeit von
$f$ an $p_0$ garantiert mir, daß es hierzu einen "`Toleranzbereich"'
$U_\delta(p_0)$ um die Stelle $p_0$ gibt, so daß $f(p)$ für
$p \in U_\delta(p_0)$ sicher im Toleranzbereich $U_\epsilon(f(p_0))$
um $f(p_0)$ liegt.

\begin{namedthm}{Umformulierung der Stetigkeit in einem Punkt im Falle
    einer Funktion $f\colon \set R \to \set R$}
  Es ist $f$ genau dann in $t_0 \in \set R$ stetig, falls
  \[
  \forall \epsilon \in \set R_+\, \exists \delta \in \set R_+ \,
  \forall t \in \set R\colon \bigl(\abs{t - t_0} < \delta \implies \abs{f(t) - f(t_0)} 
  < \epsilon\bigr).
  \]
\end{namedthm}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    \label{it:continuous_const}
    Jede konstante Abbildung $E \to \tilde E$ ist stetig.
  \item
    \label{it:continuous_id}
    Die Identität $\id_E\colon E \to E$ ist stetig.

    Insbesondere ist die Abbildung
    \[
    x\colon \set R \to \set R, t \mapsto t
    \]
    stetig.
    \begin{namedthm}{Festsetzung}
      Ab sofort bezeichnen wir mit $x$ immer
      die Identität von $\set R$.
    \end{namedthm}
  \item
    Die Funktion
    \[
    \abs x\colon \set R \to \set R, t \mapsto \abs t
    \]
    ist stetig.
  \end{enumerate}
\end{examples*}

\begin{theorem*}[Komposition stetiger Abbildungen]
  Es seien $E$, $E'$ und $E''$ metrische Räume, $p_0 \in E$,
  $g\colon E \to E'$ eine in $p_0$ stetige Abbildung und
  $f\colon E' \to E''$ eine in $g(p_0)$ stetige Abbildung.  Dann ist
  $f \circ g\colon E \to E''$ in $p_0$ stetig.
\end{theorem*}

\begin{corollary*}
  Ist $f\colon E \to \set R$ eine in $p_0$ stetige Funktion, so ist
  $\abs f\colon E \to \set R, p \mapsto \abs{f(p)}$ auch in $p_0$ stetig.
\end{corollary*}

\section{Produkträume}
\label{sec:metric_product}

\begin{definition*}
Sei $n \in \set N_2$.  Weiter seien $(E_i, d_i)$ für $i = 1$, $2$, \dots,
$n$ metrische Räume.  Dann wird auf dem \emph{Produktraum}
\[
E \coloneqq \prod_{i = 1}^n E_i
\coloneqq \{p = (p_i) = (p_i)_{i = 1, 2, \ldots, n}
\mid \text{$p_i \in E_i$ für alle $i = 1$, $2$, \dots, $n$}\}
\]
durch
\[
d(p, q) \coloneqq \max\{d_i(p_i, q_i)\mid i = 1, 2, \ldots, n\}
\]
eine Metrik definiert.
\end{definition*}
Die $\epsilon$-Umgebung von $p \in E$ ist
\[
U_\epsilon(p) = \prod_{i = 1}^n U^i_\epsilon(p_i).
\]

Im Falle $n = 2$ haben wir $\displaystyle \prod_{i = 1}^2 E_i = E_1 \times E_2$.

Insbesondere erhalten wir auf dem $\set R^n$ eine Metrik durch
\[
d(p, q) \coloneqq \max\{\abs{q_i - p_i} \mid i = 1, 2, \ldots, n\}.
\]

\begin{namedthm}{Umformulierung der Stetigkeit in einem Punkt im Falle
    einer Funktion $f\colon \set R^n \to \set R$}
  Es ist $f$ genau dann in $a \in \set R^n$ stetig, falls
  \begin{multline*}
  \forall \epsilon \in \set R_+\, \exists \delta \in \set R_+ \,
  \forall b \in \set R^n\colon \\
  \Bigl(\bigl(\forall i = 1, 2,\ldots,
  n\colon \abs{b_i - a_i} < \delta \bigr) \implies \abs{f(b) - f(a)}
  < \epsilon\Bigr).
  \end{multline*}
\end{namedthm}

\begin{proposition}
  \label{sec:continuous_pr1}
  In der in der Definition beschriebenen Situation ist für jedes
  $k \in \{1, 2, \ldots, n\}$ die \emph{kanonische Projektion}
  \[
  \pr_k\colon E \to E_k, (p_i) \mapsto p_k
  \]
  stetig.
\end{proposition}

\begin{namedthm}{Festsetzung}
  Im Spezialfall $E = \set R^n$ bezeichnen wir die kanonische Projektion
  $\pr_k\colon \set R^n \to \set R$ mit $x_k$; wir nennen sie auch die
  $k$-te \emph{Koordinatenfunktion}.  Sie ist also stetig.  Im Falle
  $n = 2$ schreiben wir auch $x \coloneqq x_1$ und $y \coloneqq x_2$;
  im Falle $n = 3$ auch $x \coloneqq x_1$, $y \coloneqq x_2$ und
  $z \coloneqq x_3$.
\end{namedthm}

\begin{proposition}
  \label{sec:continuous_pr2}
  In der in der Definition beschriebenen Situation sei $D$ ein
  weiterer metrischer Raum, und für jedes $i = 1$, $2$, \dots, $n$ sei
  $f_i\colon D \to E_i$ eine Abbildung.  Dann können wir
  \[
  f = (f_1, f_2, \ldots, f_n)\colon D \to E, p \mapsto (f_i(p))_{i = 1, 2, \ldots, n}
  \]
  definieren.  Damit gilt für jedes $p_0 \in D$: Es ist $f$ genau dann
  in $p_0$ stetig, wenn für jedes $i = 1$, $2$, \dots, $n$ die
  Abbildung $f_i$ in $p_0$ stetig ist.
\end{proposition}

Dieses Kriterium findet besonders für Abbildungen
$f = (f_1, \ldots, f_n)\colon D \to \set R^n$ Verwendung.

\begin{exercise}
  Es seien $E'$ und $E''$ metrische Räume,
  $E \coloneqq E' \times E''$ ihr metrischer Pro\-dukt\-raum und $q \in E''$ ein fester Punkt. 
  Man zeige, daß dann die Abbildung
  \[
  i^q\colon E' \to E, p \mapsto (p, q)
  \]
  stetig ist.
\end{exercise}

\begin{exercise}
  Man zeige, daß für jeden metrischen Raum $(E, d)$ gilt:
  \begin{enumerate}
  \item
    Für jedes Quadrupel $(p, q, p_0, q_0)$ von Punkten aus $E$ gilt
    \[
    \abs{d(p, q) - d(p_0, q_0)} \leq d(p, p_0) + d(q, q_0).
    \]
    Daher (?) ist $d\colon E \times E \to \set R$ eine stetige Funktion.
  \item
    Es sei $A \in \powerset{E}$ eine nicht leere Teilmenge.  Für jedes $p \in E$ sei
    \[
    \dist(p, A) \coloneqq \inf\{d(p, q) \mid q \in A\}
    \]
    die sogenannte \emph{Distanz} von $p$ zu $A$.  Zeige, daß die Funktion
    \[
    f\colon E \to \set R, p \mapsto \dist(p, A)
    \]
    stetig ist.  

    (Tip: Man überlege sich, daß
    $\forall a \in A\colon \dist(q, A) \leq d(q, p) + d(p, a)$ und
    $\dist(q, A) - \dist(p, A) \leq d(p, q)$.)
  \end{enumerate}
\end{exercise}

\section{Verschiedene Metriken im $\set R^n$}
\label{sec:euclidean_metric}

\begin{exercise*}
  Für $v = (v_i) \in \set R^n$ definieren wir
  \[
  \norm \infty v \coloneqq \max\{\abs{v_1}, \abs{v_2}, \ldots, \abs{v_n}\}\quad\text{und}\quad
  \norm 2 v \coloneqq \sqrt{\sum_{i = 1}^n v_i^2}.
  \]
  Offenbar ist dann
  \[
  d(u, v) \coloneqq \norm \infty {v - u}
  \]
  die in \ref{sec:metric_product} eingeführte Metrik des $\set R^n$.  Durch
  \[
  \tilde d(u, v) \coloneqq \norm 2 {v - u}
  \]
  wird eine weitere Metrik des $\set R^n$, die sogenannte
  \emph{euklidische} Metrik definiert.  Die $\epsilon$-Umgebungen
  bezüglich $d$ und $\tilde d$ werden im folgenden mit $U_\epsilon(p)$
  bzw.\ $\tilde U_\epsilon(p)$ bezeichnet.  Man zeige:
  \begin{enumerate}
  \item
    $\forall v \in \set R^n\colon c \norm 2 v \leq \norm \infty v \leq C \norm 2 v$
    mit $c \coloneqq 1/\sqrt n$ und $C \coloneqq 1$.

    Sind die beiden Abschätzungen scharf, d.\,h.~wird die Aussage
    falsch, wenn wir $c$ durch eine größere bzw.~$C$ durch eine
    kleinere Konstante ersetzen?
  \item
    $\forall v \in \set R^n \, \forall \epsilon \in \set R_+\colon
    \tilde U_\epsilon(v) \subseteq U_\epsilon(v) \subseteq \tilde U_{\epsilon \sqrt n} (v)$.
  \item
    Sei $E$ ein weiterer metrischer Raum.  Dann gilt:
    \begin{enumerate}
    \item Eine Abbildung $f\colon E \to \set R^n$ ist genau dann in
      $p_0 \in E$ bezüglich $d$ stetig, wenn $f$ in $p_0$ bezüglich
      $\tilde d$ stetig ist.
    \item Eine Abbildung $g\colon \set R^n \to E$ ist genau dann in
      $v_0 \in \set R^n$ bezüglich $d$ stetig, wenn $g$ in $v_0$
      bezüglich $\tilde d$ stetig ist.
    \end{enumerate}
  \end{enumerate}
\end{exercise*}

\section{Stetigkeit der Addition und Multiplikation}

\begin{theorem*}
  Die Addition und Multiplikation sind stetige Funktionen $\set R^2 \to \set R$.
\end{theorem*}

\begin{definition*}
  Sei $M$ eine Menge, und seien $f$, $g\colon M \to \set R$ Funktionen
  und $c \in \set R$.  Dann definieren wir:
  \begin{align*}
    f + g & \colon M \to \set R, p \mapsto f(p) + g(p), \\
    f - g & \colon M \to \set R, p \mapsto f(p) - g(p), \\
    f \cdot g & \colon M \to \set R, p \mapsto f(p) \cdot g(p), \\
    f/g & \colon M \to \set R, p \mapsto f(p) / g(p),
          \quad\text{falls $g(p) \neq 0$ für alle $p \in M$} \\
    c \cdot f & \colon M \to \set R, p \mapsto c \cdot f(p), \\
    \intertext{und}
    -f & \colon M \to \set R, p \mapsto -f(p).
  \end{align*}
\end{definition*}
Natürlich ist $c \cdot f = g \cdot f$ mit der Funktion $g \equiv c$,
$-f = (-1) \cdot f$, $f - g = f + (-g)$ und $f/g = f \cdot (1/g)$.

\begin{corollary*}
  Ist $E$ ein metrischer Raum, und sind $f$, $g\colon E \to \set R$
  Funktionen, die in $p_0 \in E$ stetig sind, und ist
  $\alpha \in \set R$, so sind auch die Funktionen
  \[
  f + g, f \cdot g, \alpha \cdot f,\quad\text{und}\quad -f
  \]
  in $p_0$ stetig.  Folglich ist die Menge $C(E) \coloneqq C(E, \set R)$ der stetigen Funktionen
  $E \to \set R$ ein reeller Vektorraum, ja sogar eine $\set R$-Algebra.
\end{corollary*}

\section{Polynomfunktionen}
\label{sec:polynomials}

\begin{theorem*}
  Jede \emph{Polynomfunktion} (wir sprechen oft auch --- nicht ganz
  korrekt --- von \emph{Polynom})
  \[
  P = \sum_{k = 0}^n a_k \cdot x^k\colon \set R \to \set R
  \]
  mit $n \in \set N_0$ und $a_k \in \set R$ für $k = 0$, $1$, \dots, $n$ ist
  stetig.
\end{theorem*}
Polynomfunktionen heißen auch \emph{ganzrationale} Funktionen.

\begin{proposition}
  Sind $P$ und $Q$ Polynomfunktionen und ist $\alpha \in \set R$, so sind auch
  \[
  P + Q, P \cdot Q, \alpha \cdot P\quad\text{und}\quad P \circ Q
  \]
  Polynomfunktionen.
\end{proposition}

\begin{namedthm}{Satz vom Koeffizientenvergleich}
  Die Funktionen $f = 1$, $x$, $x^2$, \dots{} sind als Elemente des $\set R$-Vektorraumes
  $C(\set R)$ linear unabhängig.  Anders ausgedrückt:
  
  Sind $n$, $m \in \set N_0$, $a_0$, \dots, $a_n$, $b_0$, \dots, $b_m \in \set R$, ist $n \leq m$
  und gilt
  \[
  \sum_{k = 0}^n a_k \cdot x^k = \sum_{k = 0}^m b_k \cdot x^k,
  \]
  so ist $a_k = b_k$ für $k = 0$, \dots, $n$ und $b_{n + 1} = \dotsb = b_m = 0$.
\end{namedthm}

\begin{comment}
  Der Satz vom Koeffizientenvergleich ist für Polynome über beliebigen
  Körpern im allgemeinen falsch.  Genauer: Er ist falsch über endlichen
  Körpern.  In diesem Falle müssen wir zwischen Polynomen und den
  durch sie definierten Polynomfunktionen unterscheiden:
  Unterschiedliche Polynome können die gleichen Polynomfunktionen
  definieren.
\end{comment}

\begin{namedthm}{Der Grad einer Polynomfunktion}
  Ist $P = \sum_{k = 0}^n a_k \cdot x^k$ mit $a_0$, \dots, $a_n \in \set R$, so heißt
  \[
  \deg P \coloneqq \sup\{k \in \{0, \dotsc, n\} \mid a_k \neq 0\}
  \]
  der \emph{Grad} von $P$. Wir stellen fest, daß
  \[
  P \equiv 0 \iff \deg P = -\infty.
  \]
\end{namedthm}

\begin{proposition}
  Sind $P$ und $Q$ Polynomfunktionen und $\alpha \in \set R^*$, so gilt:
  \begin{align*}
    \deg (\alpha \cdot P) & = \deg P, \\
    \deg (P + Q) & \leq \max\{\deg P, \deg Q\}, \\
    \deg (P \cdot Q) & = \deg P + \deg Q.
  \end{align*}
\end{proposition}

\begin{namedthm}{Der euklidische Divisionsalgorithmus}
  Sind $P$ und $Q$ Polynomfunktionen und ist $Q \not\equiv 0$, so existiert genau ein Paar
  $(S, R)$ von Polynomfunktionen mit
  \[
  P = S \cdot Q + R\quad\text{und}\quad \deg R < \deg Q.
  \]
\end{namedthm}

\begin{corollary}
  \label{cor:linear_factor}
  Ist $\alpha \in \set R$ eine \emph{Nullstelle} einer Polynomfunktion
  $P$ (d.\,h.~$P(\alpha) = 0$), so existiert genau eine Polynomfunktion $Q$, so daß gilt
  \[
  P = (x - \alpha) \cdot Q.
  \]
\end{corollary}

\begin{corollary}
  \label{cor:number_of_zeroes}
  Jede Polynomfunktion vom Grad $n \in \set N_0$ hat höchstens $n$ verschiedene Nullstellen.
\end{corollary}

\begin{comment}
  Die Funktion $x^2 + 1$ vom Grad $2$ hat keine "`reellen"'
  Nullstellen.  Im Körper $\set C$ der komplexen Zahlen hat jedoch
  jede nicht konstante Polynomfunktion mindestens eine Nullstelle.
  Das ist die Aussage des sogenannten \emph{Fundamentalsatzes der
    Algebra}.
\end{comment}

\section{Metrische Teilräume}
\label{sec:metric_subspace}

\begin{definition*}
  Sei $(E, d)$ ein metrischer Raum und $M \in \powerset E$ eine Teilmenge von $E$.  Dann ist
  \[
  d_M \coloneqq d|(M \times M)
  \]
  eine Metrik auf $M$.  Der metrische Raum $(M, d_M)$ heißt ein (metrischer) \emph{Teilraum} von
  $(E, d)$.  Seine $\epsilon$-Umgebungen sind offensichtlich durch
  \[
  U^M_\epsilon(p) = U_\epsilon^E(p) \cap M
  \]
  gegeben, wobei der oben stehende Index angibt, aus welchem Raum die
  jeweilige $\epsilon$-Umgebung genommen wird.  Daher ist die
  Inklusion $M \injto E$ trivialerweise eine stetige Abbildung.
\end{definition*}

\begin{proposition*}
  Ist $E'$ ein weiterer metrischer Raum, so gilt:
  \begin{enumerate}
  \item Für jede in $p_0 \in M$ stetige Abbildung $f\colon E \to E'$
    ist die Restriktion auf $M$, $f|M\colon M \to E'$, ebenfalls in $p_0$ stetig.
  \item Eine Abbildung $f\colon M \to E'$ ist genau dann in $p_0 \in M$ stetig, wenn
    \[
    \forall \epsilon \in \set R_+ \, \exists \delta \in \set R_+\colon
    f(U_\delta^E(p_0) \cap M) \subseteq U_\epsilon(f(p_0)).
    \]
  \item Ist $f\colon E' \to E$ eine in $p_0 \in E'$ stetige Abbildung mit $f(E') \subseteq M$, so
    ist $f$ auch als Abbildung $E' \to M$ in $p_0$ stetig.
  \end{enumerate}
\end{proposition*}

\begin{exercise}
  Stetigkeit ist eine lokale Eigenschaft, d.\,h.: Eine Abbildung
  zwischen metrischen Räumen $E$ und $E'$, $f\colon E \to E'$, ist in
  $p_0 \in E$ stetig, wenn es ein $\epsilon \in \set R_+$ gibt, so daß
  $f|U_\epsilon(p_0)$ in $p_0$ stetig ist.
\end{exercise}

Insbesondere wissen wir jetzt auch, was unter der Stetigkeit einer
Funktion $f\colon I \to \set R$, die auf einem Intervall
$I \subseteq \set R$ definiert ist, zu verstehen ist.

\begin{exercise}[Aneinanderheften stetiger Funktionen]
  \label{ex:attach}
  Es seien $a$, $b$, $c \in \set R$ Zahlen mit $a < b < c$, $E$ ein
  metrischer Raum, sowie $f\colon [a, b] \to E$ und
  $g\colon [b, c] \to E$ stetige Abbildungen mit $f(b) = g(b)$.  Man
  zeige, daß dann genau eine stetige Abbildung $h\colon [a, c] \to E$
  mit
  \[
  h|[a, b] = f\quad\text{und}\quad h|[b, c] = g
  \]
  existiert.
\end{exercise}

\section{Stetigkeit rationaler Funktionen}

\begin{theorem*}
  Die Funktion $1/x\colon \set R^* \to \set R$ ist stetig.
\end{theorem*}

\begin{corollary}
  Ist $E$ ein metrischer Raum, sind $f$, $g\colon E \to \set R$ in
  $p_0 \in E$ stetige Funktionen und gilt $g(p_0) \neq 0$, so ist auch
  die Funktion
  \[
  f/g\colon E \setminus V(g) \to \set R, p \mapsto f(p)/g(p)
  \]
  in $p_0$ stetig.  Hierbei bezeichnet $V(g)$ die \emph{Nullstellen-}
  oder \emph{Verschwindungsmenge} von $g$, also
  \[
  V(g) \coloneqq g^{-1}(\{0\}) = \{p \in E \mid g(p) = 0\}.
  \]
\end{corollary}

\begin{corollary}
  Jede \emph{rationale Funktion}, d.\,h.\ jede Funktion, welche sich
  als Quotient $P/Q$ zweier Polynomfunktionen $P$ und $Q$ mit
  $Q \not\equiv 0$ schreiben läßt, ist auf ihrem ganzen
  Definitionsbereich $\set R \setminus V(Q)$ stetig.
\end{corollary}
Es sei beachtet, daß bei der Definition rationaler Funktionen auch
$Q \equiv 1$ zugelassen ist; Polynomfunktionen sind also spezielle
rationale Funktionen.

\section{Der Zwischenwertsatz}

\begin{theorem*}
  Sei $f\colon [a, b] \to \set R$ eine stetige Funktion auf einem
  beschränkten Intervall, und sei $c$ ein Wert echt zwischen $f(a)$
  und $f(b)$, d.\,h.~es gilt $f(a) < c < f(b)$ oder $f(b) < c < f(a)$.
  Dann existiert ein $t_0 \in \left]a, b\right[$ mit $f(t_0) = c$.
\end{theorem*}

Der erste Beweis des Zwischenwertsatzes wurde 1817 von Bernard Bolzano gegeben.

\begin{corollary}
  Ist $I$ ein Intervall von $\set R$ und $f\colon I \to \set R$ eine
  stetige Funktion ohne Nullstellen und nimmt $f$ an \emph{einer}
  Stelle einen positiven Wert an, so ist $f$ \emph{überall} positiv.
\end{corollary}

\begin{corollary}
  Jede Polynomfunktion ungeraden Grades besitzt mindestens eine Nullstelle.
\end{corollary}

\begin{exercise}[Charakterisierung von Intervallen]
  Für jede nicht leere Teilmenge
  $M \in \powerset{\set R} \setminus \{\emptyset\}$ von $\set R$ sind die
  folgenden Aussagen paarweise äquivalent:
  \begin{enumerate}
  \item
    \label{it:interval1}
    Für je zwei Zahlen $a$, $b \in M$ mit $a < b$ gilt $[a, b] \subseteq M$.
  \item
    \label{it:interval2}
    Es gilt $\left]\inf(M), \sup(M)\right[ \subseteq M$.
  \item
    \label{it:interval3}
    $M$ ist ein Intervall.
  \end{enumerate}
  
  (Tip:
  $\text{\ref{it:interval1}} \implies \text{\ref{it:interval2}}
  \implies \text{\ref{it:interval3}} \implies \text{\ref{it:interval1}}$;
  man beachte, daß auf jeden Fall $M \subseteq [\inf(M), \sup(M)]$
  gilt.)
\end{exercise}

\begin{exercise}[Intervalltreue stetiger Funktionen]
  Ist $I$ ein Intervall von $\set R$ und $f\colon I \to \set R$ eine
  stetige Funktion, so ist auch das Bild $f(I)$ von $I$ ein Intervall.
\end{exercise}

\begin{exercise}[Der eindimensionale Spezialfall des Brouwerschen Fixpunktsatzes]
  Sei $n \in \set N_1$, und sei $\set B^n$ die
  \emph{Einheitsvollkugel} des $\set R^n$ mit Mittelpunkt $0$,
  präzise~$\set B^n = \{p \in \set R^n \mid \norm 2 p \leq 1\}$.  Der
  \emph{Brouwersche Fixpunktsatz} besagt, daß jede stetige Abbildung
  $f\colon \set B^n \to \set B^n$ mindestens einen \emph{Fixpunkt}
  $p_0 \in \set B^n$ besitzt, d.\,h.~also, daß ein $p_0 \in \set B^n$
  mit $f(p_0) = p_0$ existiert.  Für $n \ge 2$ ist der Beweis schwer;
  man benutzt Methoden der algebraischen Topologie.  Man beweise den Fall $n = 1$.

  (Tip: $g = x - f$.)
\end{exercise}

\section{Monotone Funktionen}
\label{sec:monotony}

\begin{definition*}
  Sei $M \in \powerset{\set R}$ eine Teilmenge von $\set R$.  Dann
  heißt eine Funktion $f\colon M \to \set R$ \emph{streng monoton
    wachsend} (bzw.~\emph{fallend}), wenn gilt:
  \[
  \forall t, s \in M\colon \bigl(t < s \implies f(t) < f(s)\quad\text{(bzw.~$f(t) > f(s)$)}\bigr).
  \]

  Steht in dieser Defintion $f(t) \leq f(s)$ (bzw.~$f(t) \geq f(s)$)
  anstatt der strikten Ungleichung, so sprechen wir von einer
  \emph{monoton wachsenden} (bzw.~\emph{fallenden}) Funktion.
\end{definition*}

\begin{proposition*}
  Ist $f\colon M \to \set R$ streng monoton wachsend (bzw.~fallend),
  so ist $f$ injektiv, und die Umkehrfunktion
  $\check f\colon f(M) \to \set R$ ist ebenfalls streng monoton wachsend
  (bzw.~fallend).
\end{proposition*}

\begin{theorem}
  Ist $I$ ein Intervall von $\set R$ und $f\colon I \to \set R$ eine Funktion, so gilt:
  \begin{enumerate}
  \item
    Ist $f$ stetig und injektiv, so ist $f$ streng monoton wachsend oder fallend.
  \item Ist $f$ streng monoton wachsend oder fallend, so ist die
    zugehörige Umkehrfunktion $\check f\colon f(I) \to \set R$ stetig.
  \end{enumerate}
\end{theorem}

\begin{corollary*}
  Ist $f\colon I \to \set R$ eine stetige injektive Funktion auf einem Intervall $I$ von $\set R$,
  so ist die Umkehrfunktion $\check f\colon f(I) \to \set R$ ebenfalls stetig.
\end{corollary*}

\begin{theorem}
  Es sei $n \in \set N_1$ eine positive natürliche Zahl.
  \begin{enumerate}
  \item
    Ist $n$ gerade, so ist $x^n\colon \set R \to \set R$ eine \emph{gerade} Funktion, d.\,h.
    \[
    \forall t \in \set R\colon x^n (-t) = x^n(t),
    \]
    und $x^n|\left[0, \infty\right[$ ist streng monoton wachsend mit
    $x^n(\left[0, \infty\right[) = \left[0, \infty\right[$.  Daher
    besitzt $x^n|\left[0, \infty\right[$ eine stetige Umkehrfunktion
    \[
    \sqrt[n]{}\colon \left[0, \infty\right[ \to \set R.
    \]
    Es gilt
    \[
    x^n \circ \sqrt[n]{} = x|\left[0, \infty\right[\quad\text{und}\quad
    \sqrt[n]{} \circ x^n = \abs x.
    \]
  \item
    Ist $n$ ungerade, so ist $x^n\colon \set R \to \set R$ eine \emph{ungerade} Funktion, d.\,h.
    \[
    \forall t \in \set R\colon x^n (-t) = - x^n(t),
    \]
    und ist streng monoton wachsend mit $x^n(\set R) = \set R$. Daher besitzt sie eine stetige
    Umkehrfunktion
    \[
    \sqrt[n]{}\colon \set R \to \set R.
    \]
    Es gilt
    \[
    \sqrt[n]{} \circ x^n = x^n \circ \sqrt[n]{} = x.
    \]
  \end{enumerate}
\end{theorem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
