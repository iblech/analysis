\chapter{Zweiter Teil der Differentialrechnung in einer Veränderlichen}

\label{chap:diff_second}

\begin{namedthm}{Festsetzung}
  In diesem Kapitel bezeichne $E$ stets einen $\set K$-Banachraum.
\end{namedthm}

\section{Mehrmalige Differenzierbarkeit}
\label{sec:higher_derivatives-one_dim}

\begin{definition*}
  Sei $D \in \powerset{\set R}$.  Einer Funktion $f\colon D \to E$ ordnen wir die Folge
  \[
  (f^{(n)}\colon D_n \to E)_{n \in \set N_0}
  \]
  ihrer Ableitungen durch folgende rekursive Definition zu:
  \begin{align*}
    D_0 & \coloneqq D,
    &  D_{n + 1} & \coloneqq \{a \in D_n \mid \text{$f^{(n)}$ ist in $a$ differenzierbar}\},
    \\
    f^{(0)} & \coloneqq f,
                       & f^{(n + 1)} & \coloneqq \bigl(f^{(n)}\bigr)'.
  \end{align*}

  Insbesondere ist $f^{(1)} = f'$, $f^{(2)} = f'' \coloneqq (f')'$, $f^{(3)} = f''' \coloneqq
  (f'')'$.

  Sei $a \in D$ und sei $M \in \powerset{D}$.  Wir sagen:
  \begin{enumerate}
  \item $f$ ist in $a$ (bzw.~auf $M$) \emph{$n$-mal differenzierbar},
    wenn gilt $a \in D_n$ (bzw.~$M \subseteq D_n$).
  \item $f$ ist in $a$ (bzw.~auf $M$) \emph{$n$-mal stetig
      differenzierbar}, wenn $f$ in $a$ (bzw.~auf $M$) $n$-mal
    differenzierbar und wenn $f^{(n)}$ in $a$ (bzw.~auf $M$) noch
    stetig ist.
  \item
    $f$ ist in $a$ (bzw.~auf $M$) \emph{beliebig oft differenzierbar}, wenn gilt $a \in
    \bigcap\limits_{n = 0}^\infty D_n$ (bzw. $M \subseteq \bigcap\limits_{n = 0}^\infty D_n$).
  \item $f$ ist \emph{$n$-mal differenzierbar} (bzw.~\emph{$n$-mal
      stetig differenzierbar} bzw.~\emph{beliebig oft
      differenzierbar}), wenn $f$ auf ganz $D$ eine $n$-mal
    differenzierbare (bzw.~$n$-mal stetig differenzierbare bzw.~beliebig oft differenzierbare)
    Funktion ist.
  \end{enumerate}
\end{definition*}

\begin{example*}
  Ist $f = \sum\limits_{n = 0}^\infty a_n (x - a)^n$ eine Potenzreihe
  mit Konvergenzradius $\rho > 0$, so ist $f$ auf
  $\left]a - \rho, a + \rho\right[$ beliebig oft differenzierbar.  Insbesondere ist für jedes $m \in \set N_0$ die Funktion $x^m$ beliebig oft differenzierbar, und zwar gilt
  \[
  \forall n \in \set N_0\colon (x^m)^{(n)} = \begin{cases}
    \frac{m!}{(m - n)!} \cdot x^{m - n} & \text{für $n \leq m,$} \\
    0 & \text{für $n > m.$}
  \end{cases}
  \]
\end{example*}

\begin{theorem}
  Seien $E$, $E'$ und $F$ Banachräume über $\set K$, sei $B\colon E
  \times E' \to F$ eine stetige, bilineare Abbildung, seien $f_1$,
  $f_2$, $f\colon D \to E$ und $g\colon D \to E'$ $n$-mal
  (stetig) differenzierbare Funktionen, und sei $\alpha \in \set
  K$. Dann sind $f_1 + f_2$, $\alpha f$ und $h \coloneqq B(f, g)$ auch
  $n$-mal (stetig) differenzierbar, und es gilt:
  \begin{equation}
    \label{eq:higher_deriv}
    \begin{aligned}
      (f_1 + f_2)^{(n)} & = f_1^{(n)} + f_2^{(n)}, \\
      (\alpha f)^{(n)} & = \alpha \cdot f^{(n)}, \\
      h^{(n)} & = \sum_{k = 0}^n \binom n k \cdot B\bigl(f^{(k)}, g^{(n
        - k)}\bigr).
    \end{aligned}
  \end{equation}
  Sind $f_1$, $f_2$, $f$ und $g$ überdies in $a \in D$ sogar $(n +
  1)$-mal differenzierbar, so sind $f_1 + f_2$, $\alpha f$ und
  $h$ in $a$ auch $(n + 1)$-mal differenzierbar; ihre Ableitungen $(n
  + 1)$-ter Ordnung in $a$ sind durch die zu~\eqref{eq:higher_deriv}
  analogen Formeln gegeben.
\end{theorem}

\begin{exercise*}
  Für alle $\alpha$, $\beta \in \set C$ und $n \in \set N_0$ beweise
  man die Formel
  \[
  \binom{\alpha + \beta} n = \sum_{k = 0}^n \binom \alpha k \cdot
  \binom \beta {n - k}.
  \]
  (vgl.~Abschnitt~\ref{sec:binomial}), indem man für $f_\gamma\coloneqq (1 +
  x)^\gamma\colon \left]-1, \infty\right[ \to \set C$ mit $\gamma \in
  \{\alpha, \beta, \alpha + \beta\}$ und $k \in \set N_0$ die Formel
  \[
  f_\gamma^{(k)} = k! \binom{\gamma} k \cdot (1 + x)^{\gamma - k}
  \]
  beweist und in das vorangegangene Theorem einsetzt.
\end{exercise*}

\begin{proposition*}
  Sind $n$, $m \in \set N_0$ und ist $f\colon D \to E$ eine $n$-mal
  differenzierbare Funktion, so gilt: $f$ ist genau dann $(n + m)$-mal
  differenzierbar, wenn $f^{(n)}$ eine $m$-mal differenzierbare
  Funktion ist.  In diesem Falle ist $(f^{(n)})^{(m)} = f^{(n + m)}$.
\end{proposition*}

\begin{theorem}
  Seien $D$ und $\tilde D \in \powerset{\set R}$ Teilmengen von $\set
  R$, $g\colon \tilde D \to \set R$ und $f\colon D \to E$ zwei $n$-mal
  differenzierbare Funktionen, und es gelte $g(\tilde D) \subseteq
  D$.  Dann ist auch $f \circ g$ eine $n$-mal differenzierbare
  Funktion.
\end{theorem}

\section{Konvexe Funktionen}

\begin{definition*}
  Sei $I \in \powerset{\set R}$ ein Intervall.  Eine Funktion
  $f\colon I \to \set R$ heißt \emph{konvex}, wenn die Menge
  $C \coloneqq \{(t, s) \in I \times \set R\mid s \ge f(t)\}$ eine
  konvexe Teilmenge des Vektorraumes $\set R^2$ ist (vgl.\
  Abschnitt~\ref{sec:normed_vector_spaces_as_metric_spaces}),
  d.\,h.~wenn
  \[
  \forall t, s \in I\,\forall \lambda \in [0, 1]\colon
  f(t + \lambda (s - t)) \leq f(t) + \lambda \cdot (f(s) - f(t)),
  \]
  wenn also für alle $t$, $s \in I$ die Verbindungsstrecke von $(t,
  f(t))$ nach $(s, f(s))$ nirgends unter dem Graphen von $f$ liegt.

  Ist $-f$ konvex, so heißt $f$ \emph{konkav}.
\end{definition*}

% BILD

\begin{exercise}
  Seien $I \in \powerset{\set R}$ ein Intervall und $f\colon I \to
  \set R$ eine Funktion.
  \begin{enumerate}
  \item
    \label{it:conv_char}
    Für die Konvexität von $f$ gelten die folgenden
    Charakterisierungen:
    \begin{enumerate}
    \item
      Ohne besondere Voraussetzungen an $f$ gilt:
      \label{it:ex_convex1}
      \[
      \begin{aligned}
        \text{$f$ konvex} \iff &
        \forall t, u, s \in I\colon \Bigl(t < u < s\\ & \implies
        \frac{f(u) - f(t)}{u - t} \leq \frac{f(s) - f(t)}{s - t} \leq
        \frac{f(s) - f(u)}{s - u}\Bigr).
      \end{aligned}
      \]
    \item
      \label{it:ex_convex2}
      Ist $f$ differenzierbar, so gilt zudem:
      \[
      \begin{aligned}
        \text{$f$ konvex} & \iff
        \text{$f'$ ist monoton wachsend} \\
        & \iff
        \forall t, s \in I\colon f(s) \ge f(t) + f'(t) \cdot (s - t).
      \end{aligned}
      \]
      (Mit anderen Worten: Der Graph von $f$ liegt oberhalb jeder
      seiner Tangenten.)
    \item
      Ist $f$ sogar zweimal differenzierbar, gilt:
      \[
      \begin{aligned}
        \text{$f$ konvex} & \iff
        f'' \ge 0.
      \end{aligned}
      \]
      (Damit ist der Graph von $f$ relativ zur kanonischen
      Durchlaufrichtung nirgends nach rechts gekrümmt.)
    \end{enumerate}
  \item
    Ist $f$ konvex und $a \in I^o \coloneqq \left]\inf I, \sup I\right[$,
    so gilt:
    \begin{enumerate}
    \item
      Die Funktion $(f - f(a))/(x - a)$ ist auf $I \cap
      \left]-\infty, a\right[$ monoton wachsend und nach oben
      beschränkt.  Folglich existiert die \emph{linksseitige
        Ableitung}
      \[
      f'(a-) \coloneqq \lim_{\substack{t \to a \\ t < a}} \frac{f(t)
        - f(a)}{t - a}.
      \]
      Entsprechende beweise man die Existenz der
      \emph{rechtsseitigen Ableitung}
      \[
      f'(a+) \coloneqq \lim_{\substack{t \to a \\ t > a}} \frac{f(t)
        - f(a)}{t - a}.
      \]

      (Tip: Man benutze~\ref{it:conv_char}\ref{it:ex_convex1}.)
    \item
      Daher (?) ist $f$ auf $I^o$ stetig.
    \item
      Es ist $f'(a-) \leq f'(a+)$, und in Verallgemeinerung der
      Abschätzung aus~\ref{it:conv_char}\ref{it:ex_convex2} gilt ohne jegliche
      Differenzierbarkeitsvoraussetzung
      \[
      \forall c \in \left[f'(a-), f'(a+)\right]\, \forall t \in
      I\colon f(t) \ge f(a) + c \cdot (t - a).
      \]
    \end{enumerate}
  \end{enumerate}
\end{exercise}

\begin{exercise}[Die Funktion ${\abs x}^\alpha\colon \set R \to \set
  R$, $\alpha \in \set R_+$]
  \leavevmode
  \begin{enumerate}
  \item
    \label{it:jensen}
    Definiert man für $\alpha \in \set R_+$ den Funktionswert
    $x^\alpha(0) \coloneqq 0$, so hat man die
    in~Abschnitt~\ref{sec:exp_any_base} definierte
    Funktion $x^\alpha\colon \set R_+ \to \set R$ zu einer
    \emph{stetigen}, streng monoton wachsenden Funktion
    $x^\alpha\colon \left[0, \infty\right[ \to \set R$ fortgesetzt.
    Damit ist natürlich (?) auch
    \[
    {\abs x}^\alpha\colon \set R \to \set R, t \mapsto x^{\alpha}(\abs
    t)
    \]
    eine stetige Funktion.
  \item
    Für $\alpha > 1$ ist ${\abs x}^\alpha$ auf ganz $\set R$ stetig
    differenzierbar mit der Ableitung $({\abs x}^\alpha)'(0) = 0$.
  \item
    Für $\alpha \ge 1$ ist ${\abs x}^\alpha$ eine konvexe Funktion.
  \end{enumerate}
\end{exercise}

\begin{exercise}[Die Jensensche Ungleichung]
  % TODO: Tips!
  Seien $I \in \powerset{\set R}$ ein Intervall und $f\colon I \to
  \set R$ eine konvexe Funktion.  Sei $I^o \coloneqq \left]\inf I,
    \sup I\right[$.
  \begin{enumerate}
  \item
    Sind $t_1$, \dots, $t_n \in I$ und $\lambda_1$, \dots, $\lambda_n
    \in [0, 1]$ und gilt $\sum\limits_{k = 1}^n \lambda_k = 1$, so ist
    \[
    t^* \coloneqq \sum_{k = 1}^n \lambda_k t_k \in
    I\quad\text{und}\quad
    f(t^*) \leq \sum_{k = 1}^n \lambda_k f(t_k).
    \]
  \item
    Sind $\phi$, $g\colon [a, b] \to \set R$ Regelfunktionen, $m$, $M
    \in I^o$ und gelten
    \[
    m \leq \phi \leq M, \quad g \ge 0\quad\text{und}\quad
    G \coloneqq \int_a^b g \, dx > 0,
    \]
    so gilt in Verallgemeinerung des ersten Aufgabenteils
    \[
    t^* \coloneqq G^{-1} \cdot \int_a^b \phi g \, dx \in I,\quad
    f \circ \phi \in R([a, b], \set R),\quad\text{und}\quad
    f(t^*) \leq G^{-1} \cdot \int_a^b (f \circ \phi) \cdot g \, dx.
    \]

    (Die letzte Ungleichung heißt \emph{Jensensche Ungleichung}.)
  \item
    Seien $E$ ein $\set K$-Banachraum und $g \in R([a, b], \set R)$
    eine Regelfunktion mit $g \ge 0$ und $G \coloneqq \int_a^b g \, dx >
    0$.  Hiermit definieren wir für jedes $p \in \set R_+$ die
    Funktion
    \[
    N_p\colon R([a, b], E) \to \set R,
    f \mapsto \Bigl(G^{-1} \cdot \int_a^b {\anorm f}^p \cdot g
    \, dx\Bigr)^{1/p}.
    \]
    Die Zahl $N_p(f)$ ist \emph{ein} normiertes Maß dafür, wie stark
    sich die Funktion $f$ von der Nullfunktion unterscheidet, und zwar
    bezüglich der \emph{Dichtefunktion} $g$.  Die Normierung ist so
    getroffen, daß $N_p(1) = 1$.  Die Funktion $N_p$ taucht im
    Zusammenhang mit der sogenannten $p$-Norm, einem
    funktionalanalytischem Begriff, auf.  Man zeige nun:
    \[
    \forall \alpha, \beta \in \set R_+\colon (\alpha \leq \beta
    \implies N_\alpha \leq N_\beta).
    \]

    (Tip: ${\abs x}^{\beta/\alpha}$ ist konvex.)
  \end{enumerate}
\end{exercise}

\section{Die Taylorformel}
\label{sec:taylor}

\begin{theorem*}
  Seien $I \in \powerset{\set R}$ ein Intervall, $a \in I$, $n \in
  \set N_0$ und $f\colon I \to E$ eine Funktion in einen Banachraum
  $E$.
  \begin{enumerate}
  \item
    \label{it:taylor}
    Ist $n \ge 1$, $f$ eine $(n - 1)$-mal differenzierbare Funktion,
    welche in $a$ sogar $n$-mal differenzierbar ist, so existiert
    genau eine \emph{Polynomfunktion} $P = \sum\limits_{k = 0}^n a_k (x -
    a)^k$ mit Koeffizienten $a_k \in E$, so daß
    \[
    \forall k \in \{0, \dotsc, n\}\colon f^{(k)}(a) = P^{(k)}(a)
    \]
    gilt, nämlich
    \[
    P \coloneqq \sum_{k = 0}^n \frac 1 {k!} f^{(k)}(a) \cdot (x -
    a)^k.
    \]

    Diese Polynomfunktion heißt das \emph{Taylorpolynom} $n$-ter
    Ordnung von $f$ an der Stelle $a$.  Weiterhin existiert eine in
    $a$ stetige Funktion $R\colon I \to E$ mit $R(a) = 0$, so daß die
    \emph{Taylorformel}
    \[
    f = \sum_{k = 0}^n \frac 1 {k!} f^{(k)} (a) \cdot (x - a)^k +
    (x - a)^n \cdot R
    \]
    gilt.  Aufgrund der Gestalt des \emph{Restgliedes} $(x - a)^n
    \cdot R$ sagen wir, daß das Taylorpolynom $n$-ter Ordnung die
    Funktion $f$ in der Nähe von $a$ mindestens von $n$-ter Ordnung
    approximiert; vgl.~den Spezialfall $n = 1$ in
    Abschnitt~\ref{sec:diff_meaning}.
  \item
    Ist $f$ sogar eine $n$-mal \emph{stetig} differenzierbare und auf
    $I^o \coloneqq \left]\inf I, \sup I\right[$ eine $(n + 1)$-mal
    differenzierbare \emph{reellwertige} Funktion, also $E = \set R$,
    so existiert zu jedem $t \in I$ ein $\theta \in \left]0,
      1\right[$, so daß für die Funktion $R$ aus \ref{it:taylor} gilt:
    \[
    R(t) = \frac 1 {(n + 1)!} (t - a) \cdot f^{(n + 1)} \bigl(a + \theta
    \cdot (t - a)\bigr).
    \]
    In dieser Version heißt das Restglied $(x - a)^n R$
    \emph{Lagrangesches Restglied}.

    Im Falle $n = 0$ handelt es sich bei dieser Version der
    Taylorformel um eine modifizierte Version des Mittelwertsatzes;
    vgl.\ Abschnitt~\ref{sec:mean_value}).
  \item
    Ist $f\colon I \to E$ eine $(n + 1)$-mal stetig differenzierbare
    Funktion, so gilt für die
    Funktion der Aussage~\ref{it:taylor}, daß
    \[
    R(t) = \frac 1{n!} \cdot (t - a) \cdot \int_0^1 f^{(n + 1)} \bigl(a +
    s(t - a)\bigr) \cdot (1 - s)^n \, ds.
    \]
    In dieser Version heißt das Restglied $(x - a)^n R$
    \emph{Integralrestglied}; es gilt
    \[
    (t - a)^n \cdot R(t) = \frac{1}{n!} \int_a^t f^{(n + 1)} (s) \cdot
    (t - s)^n \, ds.
    \]
  \end{enumerate}
\end{theorem*}

Als Anwendung des Satzes über die differenzierbare Abhängigkeit
eines Integrals von einem Parameter erhalten wir:
\begin{proposition}[Differenzierbarkeit des Restgliedes in der
    Taylorformel]
  Ist die Funktion
  $f\colon I \to E$ eine $(n + m + 1)$-mal stetig differenzierbare
  Funktion, so ist die Funktion
  $R$ in dem Restglied $(x - a)^n \cdot R$ der Taylorformel mindestens
  $m$-mal stetig differenzierbar.
\end{proposition}

\begin{proposition}[Mehrfaches Abspalten von Linearfaktoren]
  Ist $f$ in der Nähe von $a$ eine $n$-mal \emph{stetig}
  differenzierbare Funktion und hat $f$ in $a$ eine \emph{Nullstelle
    mindestens $n$-ter Ordnung}, d.\,h.~gilt
  \[
  f(a) = f'(a) = \dotsb = f^{(n - 1)}(a) = 0,
  \]
  so gilt
  \[
  f = (x - a)^n \cdot h\quad\text{mit}\quad
  h\colon t \mapsto \frac 1 {(n - 1)!} \cdot \int_0^1 f^{(n)}\bigl(a + s (t
  - a)\bigr) \cdot (1 - s)^{n - 1} \, ds.
  \]
\end{proposition}

\begin{exercise}
  Sei $f\colon \left]-r, r\right[ \to E$ mit $r \in \set R_+$ eine
  differenzierbare, gerade Funktion, die in $0$ ein zweites Mal
  differenzierbar ist.  Dann ist die Funktion $g\colon \left[0,
    r^2\right[ \to E, t \mapsto f(\sqrt t)$ differenzierbar mit der
  Ableitung $g'(0) = f''(0)/2$.  Die Ableitung $g'$ ist in $0$ stetig,
  und es gilt (natürlich) $f = g(x^2)$.
\end{exercise}

\begin{exercise}
  Es sei $f\colon I \to \set R$ eine $(n + m)$-mal stetig
  differenzierbare Funktion mit $n$, $m \in \set N_0$ und $n \ge 2$,
  die an einer Stelle $a \in I$ eine $n$-fache Nullstelle besitzt
  (s.\,o.) und deren $n$-te Ableitung an der Stelle $a$ positiv ist.
  Auch wenn die $n$-te Wurzel in $0$ nicht differenzierbar ist, so
  existiert doch in einer Umgebung $U_\epsilon(a) \cap I$ eine
  mindestens $(m + 1)$-mal differenzierbare Funktion $g$ mit
  $f|U_\epsilon(a) \cap I = g^n$.
\end{exercise}

\section{Hinreichende Bedingungen für die Existenz lokaler Extrema}
\label{sec:taylor_discussion}

\begin{theorem*}
  Seien $I \in \powerset{\set R}$ ein Intervall, $I^o \coloneqq
  \left]\inf I, \sup I\right[$, $a \in I^o$, $n \in \set N_1$ und
  $f\colon I \to \set R$ eine $(n - 1)$-mal differenzierbare Funktion,
  die in $a$ sogar $n$-mal differenzierbar ist.  Es gelte
  \[
  f^{(1)}(a) = f^{(2)}(a) = \dotsb = f^{(n - 1)}(a) =
  0\quad\text{und}\quad
  f^{(n)}(a) \neq 0.
  \]
  Dann gilt:
  \begin{enumerate}
  \item
    Ist $n$ gerade und $f^{(n)}(a) > 0$, so besitzt $f$ in $a$ ein
    \emph{strenges lokales Minimum}, d.\,h.~es existiert ein $\epsilon
    \in \set R_+$ mit $U_\epsilon(a) \supseteq I$, so daß
    \[
    \forall t \in \dot U_\epsilon(a)\colon f(t) > f(a).
    \]
  \item
    Ist $n$ gerade und $f^{(n)}(a) < 0$, so besitzt $f$ in $a$ ein
    \emph{strenges lokales Maximum}, d.\,h.~es existiert ein $\epsilon
    \in \set R_+$ mit $U_\epsilon(a) \supseteq I$, so daß
    \[
    \forall t \in \dot U_\epsilon(a)\colon f(t) < f(a).
    \]
  \item
    Ist $n$ ungerade, so besitzt $f$ in $a$ \emph{kein} lokales
    Extremum.
  \end{enumerate}
\end{theorem*}

\begin{exercise*}[Wendepunkte]
  Es seien $I \in \powerset{\set R}$ ein Intervall, $I^o
  \coloneqq \left]\inf I, \sup I\right[$, $a \in I^o$ und $f\colon I
  \to \set R$ eine stetige Funktion.  Wir sagen dann, daß $(a, f(a))$
  ein \emph{Wendepunkt} von $f$ ist, wenn es ein $\epsilon \in \set
  R_+$ mit $U_\epsilon(a) \subseteq I$ gibt, so daß entweder
  \begin{alignat*}{5}
    & \text{$f|\left]a - \epsilon, a\right[$ konvex}\quad&
    & \text{und} & \quad
    & \text{$f|\left]a, a + \epsilon\right[$ konkav}
    \\
    \intertext{oder}
    & \text{$f|\left]a - \epsilon, a\right[$ konkav}\quad&
    & \text{und} & \quad
    & \text{$f|\left]a, a + \epsilon\right[$ konvex}
  \end{alignat*}
  ist.

  Man zeige: Ist $f$ auf $I$ eine $2n$-mal differenzierbare Funktion,
  die in $a$ sogar $(2n + 1)$-mal differenzierbar ist, und gilt
  \[
  f^{(2)}(a) = f^{(3)}(a) = \dotsb = f^{(2n)}(a) =
  0\quad\text{und}\quad
  f^{(2n + 1)}(a) \neq 0,
  \]
  so ist $(a, f(a))$ ein Wendepunkt von $f$.

  (Tip: Taylorformel für $f''$.)
\end{exercise*}

\section{Die Taylorreihe}
\label{sec:taylor_series}

Ist $f = \sum\limits_{n = 0}^\infty a_n \cdot (x - a)^n$ eine Potenzreihe mit
Konvergenzradius $\rho > 0$, so gilt
\[
\forall n \in \set N_0\colon f^{(n)}(a) = n! \cdot a_n,
\]
d.\,h.~für jedes $m \in \set N_0$ ist die Partialsumme
\[
\sum_{n = 0}^m a_n \cdot (x - a)^n
\]
das Taylorpolynom $n$-ter Ordnung von $f$.  Das veranlaßt uns, für
jede beliebig oft differenzierbare Funktion $f\colon U_\rho(a) \to E$
mit $\rho > 0$ die formale \emph{Taylorreihe}
\[
\sum_{n = 0}^\infty \frac 1 {n!} \cdot f^{(n)}(a) \cdot (x - a)^n
\]
von $f$ in $a$ zu bilden und zu untersuchen, ob diese gegen $f$
konvergiert.  Diese Taylorreihe ist die einzig möglich
Potenzreihendarstellung von $f$ mit Entwicklungspunkt $a$.

\begin{definition*}
  Ist $f\colon D \to E$ eine beliebig oft differenzierbare Funktion
  und konvergiert für jedes $a \in D$ die Taylorreihe
  \[
  \sum_{n = 0}^\infty \frac 1 {n!} \cdot f^{(n)}(a) \cdot (x - a)^n
  \]
  in einer gewissen Umgebung von $a$ gegen $f$, so heißt $f$ eine
  \emph{reell analytische Funktion}.
\end{definition*}

\begin{theorem*}
  Es seien $I$ ein Intervall, $a \in I$ und $f\colon I \to E$ eine
  beliebig oft differenzierbare Funktion.  Weiter mögen $C$, $q \in
  \set R_+$ existierten, so daß
  \[
  \forall t \in I\,\forall n \in \set N_0\colon \anorm{f^{(n)}(t)}
  \leq C \cdot q^n.
  \]
  Dann konvergiert die Taylorreihe
  \[
  \sum_{n = 0}^\infty \frac 1 {n!} \cdot f^{(n)}(a) \cdot (x - a)^n
  \]
  auf $I$ gegen $f$.
\end{theorem*}

\begin{warning*}
  Es gibt beliebig oft differenzierbare Funktionen, die nicht durch
  ihre Taylorreihe dargestellt werden können: Die Funktionen
  \[
  f\colon \set R \to \set R, t \mapsto \begin{cases}
    \exp(-\frac 1 t) & \text{für $t \in \set R_+,$} \\
    0 & \text{sonst}
  \end{cases}
  \]
  und
  \[
  g = \sum_{n = 1}^\infty 2^{-n} \cdot \cos(n^2 \cdot x)
  \]
  sind beliebig oft differenzierbar (für $f$ vgl.~die folgende
  Aufgabe).  Die Taylorreihe von $f$ in $0$ stellt die Nullfunktion
  dar.  Daher gibt es keine Umgebung $U_\epsilon(0)$, in der $f$ durch
  ihre Taylorreihe dargestellt wird.  Die Taylorreihe von $g$ in $0$
  besitzt den Konvergenzradius $\rho = 0$, ist also zur Beschreibung
  von $g$ ebenfalls ungeeignet.
\end{warning*}

\begin{exercise*}
  In dieser Aufgabe soll gezeigt werden, daß die Funktion
  \[
  f\colon \set R \to \set R, t \mapsto \begin{cases}
    \exp(-\frac 1 t) & \text{für $t \in \set R_+,$} \\
    0 & \text{sonst}
  \end{cases}
  \]
  beliebig oft differenzierbar ist.
  % GRAPH

  Dazu führe man die folgenden Schritte durch:
  \begin{enumerate}
  \item
    Für $n \in \set N_0$ sei
    \[
    f_n\colon \set R \to \set R, t \mapsto \begin{cases}
      t^{-n} \cdot \exp(-\frac 1 t) & \text{für $t \in \set R_+,$} \\
      0 & \text{sonst}
  \end{cases}
    \]
    Offenbar ist $f_0 = f$.  Nun zeige man der Reihe nach für alle $n
    \in \set N_0$:
    \begin{enumerate}
    \item
      $f_n$ ist in einer Umgebung von $0$ beschränkt.
    \item
      $f_n$ ist in $0$ stetig.
    \item
      $f_n$ ist in $0$ differenzierbar und $f'_n(0) = 0$.
    \end{enumerate}
  \item
    Für alle $n \in \set N_1$ ist die Funktion $f$ eine $n$-mal
    differenzierbare Funktion, und es existiert eine Polynomfunktion
    $P_n$ vom Grade $n - 1$ mit
    \[
    f^{(n)} = P_n \cdot f_{2n}.
    \]
    Daher (?) stellt die Taylorreihe von $f$ zum Entwicklungspunkt $0$
    die Funktion $f$ in keiner noch so kleinen Umgebung von $0$ dar.
  \end{enumerate}
\end{exercise*}

\section{Kurven in einem Banachraum}
\label{sec:curves}

\begin{namedthm}{Festsetzung}
  Für den Rest des Kapitels bezeichne $I \in \powerset{\set R}$ ein nicht
  entartetes Intervall und $E$ einen $\set K$-Banachraum.
\end{namedthm}

\begin{definition*}
  Eine stetige Funktion $\alpha\colon I \to E$ wird auch ein
  \emph{Weg} in $E$ und ihr Bild $\alpha(I)$ seine \emph{Spur}
  genannt.

  Ist $\alpha$ stetig differenzierbar, sprechen wir von
  einem \emph{$C^1$-Weg} oder einer \emph{Kurve}.  Die Ableitung
  $\alpha'(t)$ nennen wir den \emph{Geschwindigkeitsvektor},
  $\anorm{\alpha'(t)}$ die \emph{Geschwindigkeit} und --- im Falle
  zweimaliger Differenzierbarkeit --- $\alpha''(t)$ den
  \emph{Beschleunigungsvektor} von $\alpha$ zu einem \emph{Zeitpunkt}
  $t \in I$.

  Ist $\alpha$ eine Kurve und sind $a$, $b \in I$ mit $a < b$, so
  nennen wir
  \[
  L(\alpha|[a, b]) \coloneqq \int_a^b \anorm{\alpha'(t)} \, dt
  \]
  die \emph{Länge} des Kurvenstückes $\alpha|[a, b]$ (vgl.~auch
  unten stehenden Kommentar).

  Gilt $L(\alpha|[a, b]) = b - a$ für alle $a$, $b \in I$ mit $a < b$,
  so sagen wir, daß $\alpha$ \emph{nach der Bogenlänge parametrisiert}
  ist.  Ist $\alpha'(t) \neq 0$, so nennen wir $\alpha$ in $t$
  \emph{regulär}; ist $\alpha$ in allen $t \in I$ regulär, so nennen
  wir $\alpha$ eine \emph{reguläre Kurve}.
\end{definition*}

Es ist zu betonen, daß ein Weg mehr ist als seine Spur (also eine
Punktmenge), nämlich eine Abbildung von einem Intervall.  Deuten wir
den Parameter $t$ als Zeitparameter, so ist diese Abbildung als die
Beschreibung eines \emph{dynamischen Vorganges} anzusehen.

\begin{examples*}
  \label{ex:curve_examples}
  \leavevmode
  \begin{enumerate}
  \item
    Sind $p$, $v \in E$ und ist $v \neq 0$, so ist bekanntlich die
    Kurve
    \[
    \alpha \colon \set R \to E, t \mapsto p + t \cdot v
    \]
    eine Parametrisierung einer \emph{Geraden} durch den Punkt $p$ in
    Richtung des Vektors $v$.  Ist $q \in E$ ein weiterer Punkt, so
    ist
    \[
    \alpha\colon [0, 1] \to E, t \mapsto p + t \cdot (q - p)
    \]
    eine Parametrisierung der \emph{Strecke} $[p, q]$; vgl.~auch
    Abschnitt~\ref{sec:normed_vector_spaces_as_metric_spaces}.
  \item
    Ist $f\colon I \to \set R$ eine Funktion, so ist der Weg
    \[
    \alpha\colon I \to \set R^2, t \mapsto (t, f(t))
    \]
    die kanonische Parametrisierung des \emph{Graphen} von $f$.  Es
    ist $\alpha$ genau dann eine Kurve, wenn $f$ stetig
    differenzierbar ist.  Sind im letzteren Fall $a$, $b \in I$ mit $a
    < b$, so gilt bezüglich der euklidischen Norm des $\set R^2$
    offenbar
    \[
    L(\alpha|[a, b]) = \int_a^b \sqrt{1 + (f')^2} \, dx.
    \]
  \item
    \label{ex:polar_coord_curve}
    Sind $r$, $\theta\colon I \to \set R$ zwei stetige Funktionen und
    ist $p \in \set C$, so beschreibt
    \[
    \alpha\colon I \to \set C, t \mapsto p + r(t) \cdot e^{i \cdot
      \theta(t)}
    \]
    einen Weg in \emph{Polarkoordinatendarstellung} bezüglich des
    Zentrums $p$; vgl.~auch Abschnitt~\ref{sec:polar_coords}.  In den meisten
    Fällen wird $r \ge 0$ vorausgesetzt.  Sind $r$ und $\theta$ stetig
    differenzierbar, so ist $\alpha$ eine Kurve; in diesem Falle gilt
    offenbar
    \[
    \alpha' = (r' + i r \theta') \cdot
    e^{i\theta},\quad\text{also}\quad
    \anorm{\alpha'} = \sqrt{(r')^2 + (r \theta')^2}.
    \]
    Die Ableitung $\theta'$ heißt die \emph{Winkelgeschwindigkeit} von
    $\alpha$.
  \item
    Seien $p \in \set C$, $r \in \set R_+$ und $\omega \in \set R^*$.
    Dann beschreibt
    \[
    \gamma\colon \set R \to \set R^2, t \mapsto p + r \cdot e^{i \omega t}
    = p + r \cdot (\cos(\omega t), \sin(\omega t))
    \]
    eine gleichförmige Kreisbewegung um den Punkt $p$ mit Radius $r$
    und Winkelgeschwindigkeit $\omega$.  Für $r = \omega = 1$ ist
    $\gamma$ bezüglich der euklidischen Norm von $\set R^2$ nach der
    Bogenlänge parametrisiert; ist außerdem $p = 0$, so liegt gerade
    die in Abschnitt~\ref{sec:cossin} beschriebene kanonische
    Parametrisierung des Einheitskreises vor.  Daß dieser nach der
    Bogenlänge parametrisiert ist, zeigt auch, daß die
    Reihendarstellungen von $\cos$ und $\sin$ diese Funktionen in
    Abhängigkeit vom \emph{Bogenmaß} darstellen.
  \item
    Sind $v$ und $w$ zwei linear unabhängige Vektoren des Banachraumes
    $E$, so parametrisiert
    \begin{align*}
      \alpha\colon \set R \to E, & t \mapsto \cos(t) \cdot v + \sin(t)
                                   \cdot w \\
      \intertext{eine \emph{Ellipse} und}
      \beta\colon \set R \to E, & t \mapsto \cosh(t) \cdot v +
                                  \sinh(t) \cdot w
    \end{align*}
    einen \emph{Hyperbelast}.

    Meist werden diese Darstellungen für $E = \set R^2$ angegeben,
    wobei $v = (a, 0)$ und $w = (0, b)$ mit Konstanten $a$, $b \in
    \set R_+$ gewählt werden.
  \end{enumerate}
\end{examples*}

\begin{exercise}
  Für jede Kurve $\alpha\colon [a, b] \to E$ gilt
  \[
  L(\alpha) \ge \anorm{\alpha(b) - \alpha(a)} = d(\alpha(a),
  \alpha(b)).
  \]
  Daher (?) ist die Strecke zwischen $\alpha(a)$ und $\alpha(b)$ eine
  kürzeste Kurve zwischen den beiden Punkten.
\end{exercise}

\begin{corollary*}
  Ist $\alpha\colon [a, b] \to E$ eine Kurve und $Z \coloneqq (t_k)_{k
    = 0, \dotsc, n}$ eine Zerlegung des Intervalles $[a, b]$ (vgl.\
  Abschnitt~\ref{sec:step_functions}), so gilt
  \[
  L(\alpha, Z) \coloneqq \sum_{k = 1}^n \anorm{\alpha(t_k) -
    \alpha(t_{k - 1})} \leq L(\alpha).
  \]
\end{corollary*}

\begin{comment*}
  Aufgrund der vorangegangenen Aufgabe ist $L(\alpha, Z)$ die
  \emph{Länge des Stre\-cken\-zuges} mit den \emph{Eckpunkten}
  $\alpha(t_0)$, $\alpha(t_1)$, \dots, $\alpha(t_n)$.  Tatsächlich
  läßt sich zeigen, daß
  \[
  L(\alpha) = \sup\{L(\alpha, Z) \mid \text{$Z$ ist Zerlegung von $[a,
    b]$}\}.
  \]
  % BEWEIS AUFNEHMEN?

  Das Supremum auf der rechten Seite läßt sich auch für nur stetige
  Wege $\alpha\colon [a, b] \to E$ bilden.  Daher können wir in diesem
  Falle diese Formel zur Definition der Weglänge $L(\alpha)$ erheben.
  Ist $L(\alpha) < \infty$, so sagen wir, daß der Weg $\alpha$
  \emph{rektifizierbar} ist.  Insbesondere sehen wir, daß Kurven, die
  über folgenkompakten Intervallen definiert sind, rektifizierbar sind.
\end{comment*}

\begin{exercise}[Invarianz der Weglänge unter
  Parametertransformation]
  Sei $\alpha\colon [a, b] \to E$ ein Weg und $\phi\colon [c, d] \to
  [a, b]$ eine \emph{stetige Parametertransformation}, das ist eine
  stetige, monoton wachsende Funktion $\phi$ mit $\phi(c) = a$ und
  $\phi(d) = b$.  Dann ist $L(\alpha) = L(\alpha \circ \phi)$;
  insbesondere gilt, daß $\alpha$ genau dann rektifizierbar ist, wenn
  $\alpha \circ \phi$ rektifizierbar ist.
\end{exercise}

\section{Integration und Differentiation bezüglich der Bogenlänge}

\begin{namedthm}{Festsetzung}
  In diesem Abschnitt seien $\alpha\colon I \to E$ eine Kurve in einem
  Banachraum und $s\colon I \to \set R$ irgendeine Stammfunktion von $\anorm{\alpha'}$.
\end{namedthm}

Die Funktion $s$ ist monoton wachsend; und für alle $a$, $b \in I$ mit $a < b$ gilt für die Länge
$L(\alpha|[a, b]) = s(b) - s(a)$; daher heißt $s$ auch \emph{Bogenlängenparameter} von
$\alpha$.

\begin{proposition}
  Die Kurve $\alpha$ ist genau dann nach der Bogenlänge parametrisiert, wenn $\anorm{\alpha'}
  \equiv 1$.
\end{proposition}

\begin{theorem}
  Ist $\alpha$ regulär, so ist $s$ eine streng monoton wachsende
  Funktion.  Ihre Umkehrfunktion $\phi \coloneqq \check s\colon J \to
  \set R$ (mit $J \coloneqq s(I)$) ist eine stetig differenzierbare,
  streng monoton wachsende Funktion mit $\phi(J) = I$, und die Kurve
  $\gamma \coloneqq \alpha \circ \phi$ ist nach der Bogenlänge
  parametrisiert.
\end{theorem}

Im folgenden sei $f\colon I \to E'$ eine
Funktion in einen weiteren Banachraum $E'$, die mit
der Kurve $\alpha$ verbunden gedacht wird, etwa durch die Vorstellung,
daß zu jedem Zeitpunkt $t \in I$ von einem sich längs $\alpha$
bewegenden Beobachter der Funktionswert $f(t)$ gemessen wird.

\begin{definition}
  Seien $a$, $b \in I$ mit $a < b$.  Ist $f\colon I \to E'$ eine
  Regelfunktion in einen weiteren Banachraum, so heißt
  \[
  \int_a^b f \, ds \coloneqq \int_a^b f(t) \anorm{\alpha'(t)} \, dt
  \]
  das \emph{Integral von $f$ längs $\alpha$ bezüglich der Bogenlänge}.
\end{definition}

Diese Definition wird durch folgende Aufgabe motiviert:

\begin{exercise}
  Seien $a$, $b \in I$ mit $a < b$.
  \begin{enumerate}
  \item
    Ist $f$ eine Treppenfunktion mit angepaßter Zerlegung
    $(t_k)_{k = 0, \dotsc, n}$ von $[a, b]$ (vgl.~Abschnitt~\ref{sec:step_functions}), so ist
    \[
    \int_a^b f \, ds = \sum_{k = 1}^n f(\xi_k) \cdot L(\alpha|[t_{k
      - 1}, t_k]) = \sum_{k = 1}^n f(\xi_k) \cdot (s(t_k) - s(t_{k -
      1}))
    \]
    mit beliebigen $\xi_k \in \left]t_{k - 1}, t_k\right[$.
  \item \textbf{Approximation von $\int\limits_a^b f \, ds$
      durch verallgemeinerte Riemannsche Summen.}  Ist $f$ eine
    stetige Funktion, so existiert zu jedem $\epsilon \in \set R_+$
    ein $\delta \in \set R_+$, so daß für jede Zerlegung
    $(t_k)_{k = 0, \dotsc, n}$ von $[a, b]$ mit $t_k - t_{k - 1} \leq \delta$
    und jede Folge $(\xi_k)_{k = 1, \dotsc, n}$ von "`Zwischenpunkten"' mit
    $\xi_k \in \left]t_{k - 1}, t_k\right[$ gilt, daß
    \[
    \anorm{\int_a^b f \, ds - \sum_{k = 1}^n f(\xi_k) \cdot (s(t_k) -
      s(t_{k - 1}))} < \epsilon.
    \]
 \end{enumerate}
\end{exercise}

\begin{theorem}[Invarianz des Integrals bezüglich der Bogenlänge gegenüber Umparametrisierungen]
  Sei $J$ ein weiteres Intervall, und sei $\phi\colon J \to I$ eine
  stetig differenzierbare Funktion mit $\phi' > 0$.  Sind dann $a$, $b \in J$ mit $a < b$, so
  gilt
  \[
  \int_a^b (f \circ \phi) \, ds = \int_{\phi(a)}^{\phi(b)} f \, ds,
  \]
  wobei auf der linken Seite $s$ der Bogenlängenparameter der umparametrisierten Kurve
  $\alpha \circ \phi$ steht.
\end{theorem}

\begin{definition}
  Ist $\alpha$ eine reguläre Kurve, so heißt der
  "`Differentialoperator"'
  \[
  \frac{d}{ds}\coloneqq \frac 1 {\anorm{\alpha'}} \cdot \frac d
  {dt}\colon f \mapsto \frac 1 {\anorm{\alpha'}} \cdot f'
  \]
  die \emph{Ableitung nach der Bogenlänge}.
\end{definition}

Diese Definition wird durch folgende Aussage motiviert:

\begin{proposition}
  Ist $\alpha\colon I \to E$ eine reguläre Kurve und $f$
  eine differenzierbare Funktion in einen weiteren Banachraum $E'$, so
  gilt
  \[
  \forall t \in I\colon \frac {df}{ds}(t) = \lim_{h \to 0} \frac{f(t +
    h) - f(t)}{s(t + h) - s(t)}.
  \]
\end{proposition}

Die Ableitung nach der Bogenlänge mißt also die infinitesimale
Änderung von $f$ relativ zur Bogenlänge.

Außerdem können wir folgendes Analogon zum Hauptsatz der Differential-
und Integralrechnung formulieren:

\begin{proposition}
  Sei $a \in I$.  Ist $f\colon I \to E'$ eine stetige Funktion in
  einen weiteren Banachraum $E'$, so ist
  \[
  F \coloneqq \int_a^x f \, ds\colon I \to E'
  \]
  eine \emph{Stammfunktion bezüglich der Bogenlänge} von $f$, das heißt
  \[
  \frac{dF}{ds} = f.
  \]
\end{proposition}

\begin{definition}[Geometrische Größen einer Kurve]
  Ist $\alpha$ eine reguläre Kurve, so heißt
  \[
  T_\alpha \coloneqq
  \frac{d\alpha}{ds} = \frac 1 {\anorm{\alpha'}} \cdot \alpha'
  \]
  das \emph{normierte Tangentenvektorfeld} von $\alpha$, weil
  $\anorm{T_\alpha} \equiv 1$.  Ist überdies $\alpha$ zweimal
  differenzierbar, so wird
  \[
  \frac{dT_\alpha}{ds}
  \]
  das \emph{Krümmungsvektorfeld} und
  \[
  \anorm{\frac{dT_\alpha}{ds}}
  \]
  die \emph{Absolutkrümmung} von $\alpha$ genannt.

  Wird für eine zweimal stetig differenzierbare Kurve $\alpha\colon I
  \to \set R^2 \cong \set C$ das normierte Tangentenvektorfeld
  $T_\alpha$ durch eine stetig differenzierbare Funktion $\theta\colon
  I \to \set R$ vermöge $T_\alpha \equiv e^{i\theta}$ beschrieben
  (vgl.~folgende Aufgabe~\ref{ex:curve_theta}), so heißt
  \[
  \kappa \coloneqq \frac{d\theta}{d s}
  \]
  die \emph{(orientierte) Krümmung} von $\alpha$.  Ihr Absolutbetrag
  ist gerade die Absolutkrümmung.

  Existiert in diesem Falle zu einem Parameter $a \in I$ ein $\epsilon
  \in \set R_+$ mit $U_\epsilon(a) \subseteq I$, so daß
  \begin{alignat*}{3}
    \kappa|\left]a - \epsilon, a\right] & \leq 0  \quad
    & & \text{und} \quad
    & \kappa|\left[a, a + \epsilon\right[ & \geq 0
    \intertext{oder}
    \kappa|\left]a - \epsilon, a\right] & \geq 0  \quad
    & & \text{und} \quad
    & \kappa|\left[a, a + \epsilon\right[ & \leq 0,
  \end{alignat*}
  so heißt $\alpha(a)$ ein \emph{Wendepunkt} von $\alpha$ und $a$ eine \emph{Wendestelle}.
\end{definition}

\begin{example*}
  Im Falle der gleichförmigen Kreisbewegung
  \[
  \gamma\colon \set R \to \set R^2 \cong \set C, t \mapsto p + r \cdot
  e^{i\omega t}
  \]
  mit $\omega \in \set R_+$ ist
  \[
  \frac{d}{ds} = \frac{1}{r \omega} \cdot \frac{d}{dt},\quad
  T_\gamma = i \cdot e^{i \omega x} = e^{i \cdot (\omega \cdot x +
    \frac \pi 2)},\quad\text{also}\quad
  \theta = \omega \cdot x + \frac \pi 2;
  \]
  und daher ist die Krümmung $\kappa = \frac 1 r$.  Im Falle $\omega \in
  \set R_-$ erhalten wir $\kappa = -\frac 1 r$.
\end{example*}

\begin{exercise}
  Sei $f\colon I \to \set R$ eine zweimal stetig differenzierbare Funktion,
  des weiteren sei $\alpha\coloneqq (x, f)\colon I \to \set R^2$ die kanonische
  Parametrisierung des Graphen von $f$.  Man zeige, daß
  \[
  T_\alpha = e^{i \theta} \quad\text{mit}\quad
  \theta = \arctan \circ f'
  \]
  ist, und daß die Krümmung von $\alpha$ durch
  \[
  \kappa = f''/\sqrt{1 + (f')^2}^3
  \]
  berechnet wird.  Man bestätige in diesem Falle, daß obige Definition
  von Wendepunkten mit der Definition aus dem Abschnitt~\ref{sec:taylor_discussion}
  übereinstimmt.
\end{exercise}

\begin{exercise}
  \label{ex:curve_theta}
  \leavevmode
  \begin{enumerate}
  \item
    Seien $\gamma = (\gamma_1, \gamma_2)\colon I \to \set R^2$ eine
    Kurve mit $\gamma(I) \subseteq \set S^1$, $a \in I$, und $\theta_0
    \in \set R$, so daß $\gamma(a) = e^{i \theta_0}$, etwa $\theta_0 =
    \arg(\gamma(a))$, vgl.~Abschnitt~\ref{sec:polar_coords}.
    Dann gilt $\gamma = e^{i \theta}$ mit der stetig differenzierbaren
    Funktion
    \[
    \theta \coloneqq \theta_0 + \int_a^x (\gamma_1 \gamma_2' -
    \gamma_2 \gamma_1') \, dx.
    \]

    (Tip: Mit dem kanonischen Skalarprodukt des $\set R^2$ berechne
    man die Ableitung $\langle\gamma, e^{i \theta}\rangle'$ und wende
    an geeigneter Stelle die Cauchy--Schwarzsche Un\emph{gleichung}
    an.
  \item
    Man zeige, daß jede Kurve $\alpha\colon I \to \set C$ mit $0
    \notin \alpha(I)$ eine Polarkoordinatendarstellung besitzt;
    vgl.~Beispiel~\ref{ex:polar_coord_curve} aus Abschnitt~\ref{sec:curves}.
  \end{enumerate}

\end{exercise}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
