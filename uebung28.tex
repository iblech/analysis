\documentclass{uebung}

\newcommand{\semester}{Sommersemester 2017}

\title{\bfseries 13.~Übung zur Analysis II}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}
\date{17.~Juli
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
  24.~Juli 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=192]

\item \writtenex \textbf{Die Riemannsche Zahlenkugel.}
    Ähnlich wie in Abschnitt~10.1 die Zahlengerade $\set R$ zu
  $\widehat{\set R}$ erweitert wurde, wird hier die komplexe Ebene
  $\set C$ durch Hinzunahme \emph{eines} Elementes $\infty$ zu
  $\widehat{\set C} \coloneqq \set C \cup \{\infty\}$ erweitert.
  Weiterhin definieren wir für alle $\epsilon \in \set R_+$, daß
  \[
    U_\epsilon(\infty) \coloneqq \{z \in \set C\mid \abs z >
    1/\epsilon\} \cup \{\infty\}.
  \]

  In dem wir mutatis mutandis die Definition aus Abschnitt~6.1 für
  offene Mengen benutzen, erhalten wir auf $\widehat{\set C}$ eine
  topologische Struktur $\Top{\widehat {\set C}}$.  Der dadurch definierte
  topologische Raum heißt die \emph{Riemannsche Zahlenkugel},
  vgl.~\ref{it:riemann_sphere}.
  \begin{enumerate}
  \item
    Die von $\Top{\widehat{\set C}}$ auf $\set C$ induzierte
    Teilraumtopologie stimmt mit der kanonischen topologischen
    Struktur von $\set C$ überein.
  \item
    \label{it:sphere_inversion}
    Die Abbildung
    \[
      j\colon \widehat{\set C} \to \widehat{\set C},
      z \mapsto \begin{cases}
        \frac 1 z & \text{für $z \in \set C^*,$} \\
        \infty & \text{für $z = 0$ und} \\
        0 & \text{für $z = \infty$}
      \end{cases}
    \]
    ist ein Homöomorphismus.
  \item
    \label{it:riemann_sphere}
    Die \emph{stereographische Projektion} $f\colon \set S^2 \to
    \widehat{\set C}$ von der zwei-dimensionalen \emph{Sphäre}
    \[
      \set S^2 \coloneqq \{p \in \set R^3\mid x(p)^2 + y(p)^2 + z(p)^2
      = 1\}
    \]
    auf $\widehat{\set C}$, die den "`Nordpol"' $N \coloneqq (0, 0, 1)$
    auf $\infty$ abbildet und die im übrigen durch
    \[
      f|(\mathbf S^2 \setminus \{N\}) \equiv \frac 1 {1 - z} \cdot (x + i
      \cdot y)|(\mathbf S^2 \setminus \{N\})
    \]
    beschrieben ist, ist ein Homöomorphismus.  Man fertige eine Skizze
    an, aus der ersichtlich ist, daß diese Abbildung die Punkte der
    $\set S^2$ vom Projektionszentrum $N$ auf die Äquatorialebene
    projiziert.

    (Tip: Untersuchungen in der Nähe von $z = \infty \in \widehat{\set
      C}$ kann man mit Hilfe der Abbildung $j$
    aus~\ref{it:sphere_inversion} auf Untersuchungen in der Nähe von
    $z = 0$ zurückführen.)
  \item
    Es sei $A=\displaystyle\begin{pmatrix} a & b \\ c & d\end{pmatrix}$
    eine Matrix mit komplexwertigen Einträgen mit $\det(A) = a d
    - b c \neq 0$.  Dann existiert genau ein Homöomorphismus
    $f_A\colon \widehat{\set C} \to \widehat{\set C}$, der die
    \emph{gebrochen-lineare Abbildung} (oder auch
    \emph{Möbius-Transformation})
    \[
      z \mapsto \frac{a z + b}{c z + d}
    \]
    fortsetzt.

    (Tip: Man beachte den Tip zu~\ref{it:riemann_sphere} und mache
    eine Fallunterscheidung, je nachdem, ob $c = 0$ oder $c \neq 0$.
    In letzterem Falle kann man $f_A$ als Komposition $f_B \circ j
    \circ f_C$ schreiben, wobei $f_B$ und $f_C$ vom Typ $(c, d) = (0,
    1)$ sind.)
  \end{enumerate}

\item
  \oralex
  Seien $E$ ein topologischer Raum und $M$, $N \subseteq E$
  Teilmengen.  Beweise, daß 
  $\interior{(M \cap N)} = \interior M \cap \interior N$.  Gilt auch
  $\interior{(M \cup N)} = \interior M \cup \interior N$?
  
\item Seien $E$ ein topologischer Raum und $M$, $N \subseteq E$ Teilmengen.  Zeige:
  \begin{enumerate}
  \item \oralex
    $\closure{M \cup N} = \closure M \cup \closure N$.
  \item \writtenex
    Der Rand $\partial M$ ist eine abgeschlossene Teilmenge von $E$;
    für ihn gilt weiterhin
    \[
      \partial M = \{p \in E \mid \forall U \in \Open p\colon M \cap U
      \neq \emptyset \land (E \setminus M) \cap U \neq \emptyset\}.
    \]
  \item \writtenex
    Sind $M$ und $N$ jeweils offen und dicht in $E$, so ist auch $M \cap N$
    eine offene und dichte Teilmenge von $E$.
  \end{enumerate}

\item \writtenex
  Es seien $E$ ein normierter $\set K$-Vektorraum, $v \in E$ und $r \in \set
  R_+$.  Dann gilt:
  \[
    \closure{U_r(v)} = B_r(v) \coloneqq \{u \in E \mid \anorm{u - v}
    \leq r\}
  \]
  und
  \[
    \interior{B_r(v)} = U_r(v).
  \]
  (Tip: Auf Strahlen arbeiten!)
  
  Gelten diese Aussagen auch in beliebigen metrischen Räumen?
  
\item \oralex
  Es seien $A$ und $B$ zwei abgeschlossene Teilmengen eines
  topologischen Raumes $E$, und $f\colon A \to  E'$ und $g\colon B \to
  E'$ zwei stetige Abbildungen in einen topologischen Raum $E'$, und
  es gelte $f|(A \cap B) = g|(A \cap B)$.  Dann existiert genau eine
  Abbildung $h\colon A \cup B \to E'$ mit $h|A = f$ und $h|B = g$, und
  diese ist stetig.

  (Diese Aussage über das Aneinanderheften stetiger Abbildungen
  verallgemeinert offenbar die entsprechende Aussage aus der
  Aufgabe~2 von Abschnitt~3.7.)

\item \writtenex
  Zeige, daß die Abbildung
  \[
    \frac x {1 - x^2}|\interval[open]{-1}{1}\colon \interval[open]{-1}{1} \to \set R
  \]
  ein Homöomorphismus ist.
  
\end{enumerate}  

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
