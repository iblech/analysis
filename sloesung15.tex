\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2016/17}

\title{{\bfseries 15.~Übung zur Analysis I}\\
  Lösungen zu den schriftlichen Aufgaben} \author{Caren\ Schinko, M.\ Sc.} 
  \date{13.~Februar 2017}

\newcommand{\zzbig}{\textbf{Zu zeigen:} }
\newcommand{\zz}{\textit{Zu zeigen:} }

\begin{document}

\maketitle

\begin{enumerate}

\item[\bfseries 93.] \zzbig Ist $f \colon D \to E$ in $a$ differenzierbar und
  sind $(a_n)_{n \ge 0}$ und $(b_n)_{n \ge 0}$ Folgen in $D$ mit
  $\displaystyle \lim_{n \to \infty} a_n = a = \lim_{n \to \infty} b_n$ und
  $\forall n \in \set N_0\colon (a_n < b_n \land a_n \leq a \leq b_n)$,
  so gilt
  \[
  \lim_{n \to \infty} \frac{f(b_n) - f(a_n)}{b_n - a_n} = f'(a).
  \]
  \begin{proof}
    \zz \[
    \forall \epsilon \in \set R_+ \, \exists n_0 \in \set N_0 \, \forall n \ge
    n_0 \colon \left|\frac{f(b_n)-f(a_n)}{b_n-a_n}-f'(a)\right| < \epsilon.
    \]
    
    Da $f$ in $a$ differenzierbar, existiert nach Theorem 6.5 eine in $a$
    stetige Funktion $R \colon D \to E$ mit $R(a)=0$, sodass gilt:
    \begin{equation} \label{eq:approx}
    \forall t \in D\colon f(t) = f(a) + f'(a) \cdot (t-a) + R(t) \cdot (t-a).
    \end{equation}
    Sei $\epsilon \in \set R_+$ beliebig. Da $R$ in $a$ stetig ist, gilt:
    \[
    \exists \delta \in \set R_+ \, \forall t \in U_\delta(a)\colon
    \left| R(t) \right| = \left| R(t) - R(a) \right| < \epsilon. 
    \]
    Da $\displaystyle \lim_{n \to \infty} a_n = a = \lim_{n \to \infty} b_n$,
    gilt:
    \[
    \exists n_0 \in \set N_0 \, \forall n \ge n_0 \colon
    \left| a_n-a \right|, \left| b_n-a \right| < \delta.
    \]
    Damit gilt:
    \begin{equation} \label{eq:absch}
    \forall n \ge n_0\colon \left|R(a_n)\right|, \left|R(b_n)\right| < \epsilon.
    \end{equation}
    Da $b_n-a_n>0$ und $a-a_n,b_n-a\ge 0$, gilt insgesamt für alle $n \ge n_0$ :
    \begin{align*}
    \left|\frac{f(b_n)-f(a_n)}{b_n-a_n}-f'(a)\right|
    &\underset{(\ref{eq:approx})}{=}
    \frac{\left| R(b_n)(b_n-a)-R(a_n)(a_n-a) \right|}{b_n-a_n}\\
    &\underset{\Delta}{\le} \frac{\left|R(b_n)\right| (b_n-a)
    + \left|R(a_n)\right| (a-a_n)}{b_n-a_n}\\
    &\underset{(\ref{eq:absch})}{<}
    \frac{\epsilon\cdot(b_n-a) + \epsilon\cdot(a-a_n)}{b_n-a_n}
    = \epsilon.\qedhere
    \end{align*}
  \end{proof}

\item[\bfseries 95.] Der Zeitaufwand der Strecke von Punkt $A$ zu Punkt $B$ ist
  in Abhängigkeit von der Strecke $s$ mithilfe des Satzes von Pythagoras gegeben
  durch
  \[
  f\colon [0,l] \to \set R_{\ge 0}, \quad
  s \mapsto \frac{1}{v_1} \cdot \sqrt{s^2 + a^2}
  + \frac{1}{v_2} \cdot \sqrt{(l-s)^2 + b^2}.
  \]
  \begin{center}
    \begin{tikzpicture}
      \coordinate [label=above:$A$] (A) at (-4,2);
      \coordinate [label=below:$B$] (B) at (2,-2);
      
      \draw (A) -- (0,0) coordinate (O) -- (B);
      \draw (0,2) coordinate (T) -- (0,1) node [label=right:$a$] {} -- (0,-1) node [label=left:$b$] {} -- (0,-2) coordinate (S);
      \draw (A) -- (-2,2) node [label=below:$s$] {};
      \draw (A) -- (-1,2) node [label=above:$l$] {};
      \draw (-1,2) -- (2,2) coordinate (B');
      \draw[dashed] (B) -- (B');
      \draw[dashed] (S) -- (B);

      \draw pic ["$\phi_1$",draw,angle radius=1.5cm] {angle=T--O--A};
      \draw pic ["$\phi_2$",draw,angle radius=1.5cm,angle eccentricity=0.75] {angle=S--O--B};
    \end{tikzpicture}
  \end{center}
  
  Aus Abschnitt~6.11 ist bekannt, dass wir für ein streng absolutes Extremum
  $s_0$ von $f$ die Nullstellen $f'(s_0)=0$ bestimmen müssen. Bestimme
  nun also mithilfe der Rechenregeln für Ableitungen die Ableitung von $f$, sei
  dazu $s \in [0,l]$:
  \begin{align*}
  f'(s) &= \frac{1}{v_1} \cdot \frac{1}{2} \cdot \left(s^2 + a^2\right)^{-1/2}
  \cdot 2s + \frac{1}{v_2} \cdot \frac{1}{2} \cdot \left((l-s)^2
  + b^2\right)^{-1/2} \cdot (2(l-s)) \cdot (-1)\\
  &= \frac{s}{v_1} \cdot \left(s^2 + a^2\right)^{-1/2} - \frac{l-s}{v_2}
  \cdot \left((l-s)^2 + b^2\right)^{-1/2}.
  \end{align*}
  Da gilt
  \[
  \sin(\phi_1) = s \cdot \left(s^2 + a^2\right)^{-1/2} \quad\text{und}\quad
  \sin(\phi_2) = (l-s) \cdot \left((l-s)^2 + b^2\right)^{-1/2},
  \]
  folgt
  \[
  f'(s)=0 \Longleftrightarrow v_1\cdot\sin(\phi_2) = v_2\cdot\sin(\phi_1).
  \]
  Damit hat $f'$ genau eine Nullstelle in $]0,l[$, vorausgesetzt, dass $A$ und
  $B$ nicht auf der Geraden $g$ liegen. Außerdem gilt $f'(0)<0$ und $f'(l)>0$,
  damit existiert, da $f'$ stetig ist (siehe Abbildungsvorschrift oben),
  $\epsilon \in \set R_+$ mit $f'(0+\epsilon)<0$ und $f'(l-\epsilon)>0$.
  Insgesamt hat somit nach Theorem~6.11 $f$ am Punkt $s_0\in]0,l[$ mit
  $f'(s_0)=0$ ein streng absolutes Minimum.
  
  Stichwort für die Physikstudenten-Frage: Brechungsgesetz.

\item[\bfseries 97.]
  \begin{enumerate}
  \item \zzbig $\forall a \in I \colon f(a) + f'(a) \cdot (g(a) - a)=0$.
    \begin{proof}
      Sei $a \in I$ beliebig. Es gilt:
      \[
      f(a) + f'(a) \cdot (g(a) - a) = f(a) + f'(a) \cdot (a -
      \frac{f(a)}{f'(a)} - a) = 0. \qedhere
      \]
    \end{proof}
  \item \zzbig $\forall a \in I \colon f(a)=0 \Longleftrightarrow g(a)=a.$
    \begin{proof}
      Sei $a \in I$ beliebig. Es gilt:
      \[
      f(a) = ((x-g) \cdot f')(a) = 0
      \Longleftrightarrow g(a)=(x-\frac{f}{f'})(a)=a. \qedhere
      \]
    \end{proof}
  \item Da $f'\neq 0$, ist auch $1/f'$ in $a$ stetig, damit gilt mit
    Aufgabe~90(a) und den Rechenregeln für die Ableitung:
    \[
    g'(a)=x'(a)-f'(a)\cdot\frac{1}{f'(a)}=1-1=0.
    \]
    Da somit $\left|g'(a)\right|<1$, ist nach Aufgabe 91(b) $a$ ein anziehender
    Fixpunkt (nach (b) und da $f(a)=0$ ist $a$ ein Fixpunkt von $g$).
  \item Beim Heronsche Verfahren zur Ermittlung von Quadratwurzeln wird eine
    Banachfolgen bezüglich der Funktion $g = (x + a/x)/2|\set R_+$
    betrachtet. Wir vermuten, dass die Funktion $f$, auf die das Newtonsche
    Nullstellenverfahren angewendet wird, gegeben ist durch
    $f=(x^2-a)|\set R_+$. Für dieses $f$ gilt:
    \[
    x - \frac{f}{f'} = x - \frac{x^2-a}{2x}
    = \frac{1}{2}\left(x+\frac{a}{x}\right) = g,
    \]
    damit ist die Vermutung bestätigt.
  \item Sei $n\in\set N_1$. Wir überprüfen die rekursive Definition:
    \[
    t_{n+1}=t_n-\frac{f(t_n)}{f'(t_n)}
    = t_n-\frac{f(t_n)(t_n-t_{n-1})}{f(t_n)-f(t_{n-1})}
    = \frac{t_{n-1} \cdot f(t_n) - t_n \cdot f(t_{n-1})}{f(t_n) - f(t_{n-1})}.
    \]
    Die Sekante durch die Punkte
    $(t_{n - 1}, f(t_{n - 1}))$ und $(t_n, f(t_n))$ wird beschrieben durch
    folgende Funktion:
    \[
    t \mapsto m \cdot t + s
    \]
    für passendes $m \in \set R\setminus\{0\}$ und $s \in \set R$. Durch
    Einsetzten der Punkte können $m$ und $s$ bestimmt werden:
    \[
    m=\frac{f(t_n)-f(t_{n-1})}{t_n-t_{n-1}} \quad\text{und}\quad
    s=f(t_{n-1})-\frac{f(t_n)-f(t_{n-1})}{t_n-t_{n-1}} \cdot t_{n-1}.
    \]
    Damit ergibt sich für die gesuchte Funktion:
    \[
    t \mapsto \frac{f(t_n)-f(t_{n-1})}{t_n-t_{n-1}} \cdot (t-t_{n-1})
    + f(t_{n-1}).
    \]
    Berechnung des Schnittpunkts mit der $x$-Achse:
    \begin{align*}
    &\frac{f(t_n)-f(t_{n-1})}{t_n-t_{n-1}} \cdot (t-t_{n-1})
    + f(t_{n-1}) = 0\\
    \Longleftrightarrow\, &t = \left(\frac{f(t_n)-f(t_{n-1})}{t_n-t_{n-1}}
    \cdot t_{n-1} - f(t_{n-1})\right) \cdot
    \frac{t_n-t_{n-1}}{f(t_n)-f(t_{n-1})}\\
    &\phantom{t} = \frac{t_{n - 1} \cdot f(t_n) - t_n \cdot f(t_{n -
      1})}{f(t_n) - f(t_{n - 1})}.
    \end{align*}
    Damit ist $t_{n+1}$ offensichtlich der Schnittpunkt von Sekante und
    $x$"~Achse.
    
    Skizze für das Newtonsche Nullstellenverfahren:
    \begin{center}
      \includegraphics{newton.png}
    \end{center}
    \newpage

    Skizze für das Sekantenverfahren:
    \begin{center}
      \includegraphics{sekanten.png}
    \end{center}

  \item Sekantenverfahren für Funktion $f=x^3+\frac{11}{2}x^2-8x-44$ mit
    Startwerten $t_0=-10$ und $t_1=-5$:
    \begin{align*}
      t_2 &= \frac{t_0 \cdot f(t_1) - t_1 \cdot f(t_0)}{f(t_1) - f(t_0)}
      \approx -5,10059,\\
      t_3 &\approx -5,65557, \quad t_4 \approx -5,46606, \quad
      t_5 \approx -5,49751,\\
      t_6 &\approx -5,50004, \quad t_7 \approx -5,5
    \end{align*}
    Bei der Berechnung von $t_8$ stellt man fest, dass $f(-5,5)=0$, also
    ist $-5,5$ eine Nullstelle von $f$.
  \end{enumerate}
  
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
