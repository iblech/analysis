\documentclass{uebung}

\newcommand{\semester}{Sommersemester 2017}

\title{\bfseries 12.~Übung zur Analysis II}
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}
\date{10.~Juli
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
  17.~Juli 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=185]

\item Jede $\set K$-wertige $(n \times n)$-Matrix $A$
  beschreibt bekanntliche eine lineare Abbildung $A \in \Lin(\set K^n, \set K^n)$ durch
  $\displaystyle \begin{pmatrix} p_1 \\ \vdots \\ p_n\end{pmatrix} \mapsto A \cdot \begin{pmatrix}
    p_1 \\ \vdots \\ p_n\end{pmatrix}$.
  \begin{enumerate}
  \item \oralex
    \label{it:exp}
    Bestimme $\gamma_A$ jeweils für Matrizen der Form:
    \begin{enumerate}
    \item
      $\displaystyle A = \begin{pmatrix} \lambda_1 & 0 & \dots & 0 \\
        0 & \ddots & \ddots & \vdots \\
        \vdots & \ddots & \ddots & 0 \\
        0 & \dots & 0 & \lambda_n
      \end{pmatrix},$
    \item
      $\displaystyle A = \begin{pmatrix} \lambda & 1 & 0 & \dots & 0 \\
        0 & \ddots & \ddots & \ddots & \vdots \\
        \vdots & \ddots & \ddots & \ddots & 0 \\
        \vdots & & \ddots & \ddots & 1 \\
        0 & \dots & \dots & 0 & \lambda
      \end{pmatrix},$
    \item
      $\displaystyle A = \begin{pmatrix}
        0 & \lambda \\
        - \lambda & 0
      \end{pmatrix}.$
    \end{enumerate}
  \item \oralex Seien $V$ ein abgeschlossener Untervektorraum des
    Banachraumes $E$ und $B \in \Lin(E, E)$ eine stetige lineare
    Abbildung, unter der $V$ \emph{invariant} ist,
    d.\,h.~$B(V) \subseteq V$.  Sei $B^V \in L(V, V)$ die von $B$ auf
    $V$ induzierte stetige lineare Abbildung.  Zeige, daß dann $V$
    auch invariant unter $\exp(B)$ ist und daß gilt
    \[
      \exp(B) | V = (V \hookrightarrow E) \circ \exp(B^V).
    \]
    Im Falle $E = \set K^n$ gilt also (?)
    \[
      \exp(A) = \begin{pmatrix}
        \exp(A_1) & 0 & \dots & 0 \\
        0 & \ddots & \ddots & \vdots \\
        \vdots & \ddots & \ddots & 0 \\
        0 & \dots & 0 & \exp(A_m)
      \end{pmatrix},
    \]
    wenn
    \[
      A = \begin{pmatrix}
        A_1 & 0 & \dots & 0 \\
        0 & \ddots & \ddots & \vdots \\
        \vdots & \ddots & \ddots & 0 \\
        0 & \dots & 0 & A_m
      \end{pmatrix},
    \]
    eine Block-Diagonalmatrix ist.

  \item \writtenex
    Löse die folgenden Anfangswertaufgaben:
    \begin{enumerate}
    \item
      $\displaystyle y' = \begin{pmatrix} 1 & 2 \\ 1 & 0\end{pmatrix} \cdot y$,
      $\displaystyle y(0) = \begin{pmatrix} 1 \\ -1\end{pmatrix};$
    \item
      $\displaystyle y' = \begin{pmatrix} 3 & -1 \\ 1 & 1\end{pmatrix} \cdot y$,
      $\displaystyle y(0) = \begin{pmatrix} 1 \\ 11\end{pmatrix};$
    \item
      $\displaystyle y' = \begin{pmatrix} 0 & 1 & 0 \\ -1 & 0 & -1 \\ 0 & 1 & 0\end{pmatrix}
      \cdot y$,
      $\displaystyle y(0) = \begin{pmatrix} \sqrt 2 \\ 0 \\ \sqrt 2\end{pmatrix}.$
    \end{enumerate}
    (Tip: Führe geschickte Transformationen an der Matrix durch, und beachte, daß $C \cdot \exp(A) \cdot C^{-1} = \exp(C \cdot A \cdot C^{-1})$ gilt, sowie Teil~\ref{it:exp}.)
  \end{enumerate}

\item Aufgrund seiner Allgemeinen Relativitätstheorie behauptete
  Albert Einstein die Ablenkung von Licht durch große Massen.  Durch
  Beobachtungen während der Sonnenfinsternis am 29.5.1919 wurde der
  exakte Winkel $\Delta$ der Lichtablenkung bestätigt, was wesentlich
  zur Anerkennung der Allgemeinen Relativitätstheorie beitrug.
  \begin{center}
    \begin{tikzpicture}[scale=1.5,decoration={markings,mark=at position 0.5 with {\arrow{latex}}}]
      \coordinate [label=above:$M$] (M) at (0, 0);
      \coordinate (A) at (-3, 0);
      \coordinate (B) at (3, 0);
      \coordinate (C) at (0, -1);
      \coordinate (D) at (0, -1);

      \fill [black] (M) circle (2pt);

      \draw [postaction={decorate}] (A) .. controls (C) and (D)
      .. (B);

      \draw [dashed] (A) -- (C) -- ([turn]0:1cm);
      \draw [dashed] (B) -- (D) -- ([turn]0:1cm) coordinate (E);

      \pic [draw, ->, "$\Delta$", angle eccentricity=1.5] {angle=A--C--E};

    \end{tikzpicture}
  \end{center}
  Einstein ging davon aus, daß in der Nähe einer großen Masse anstatt
  der euklidischen Geometrie (oder korrekter: der Minkowskischen
  Geometrie) die sogenannte Schwarzschild-Geometrie zugrundezulegen
  ist.  Danach bewegt sich ein nahe an der Masse $M$ vorbeifliegendes
  ruhemasseloses Teilchen, etwa ein Photon, in einer Ebene, und zwar
  gemäß der Differentialgleichung
  \[
    y'' + y = 3 M y^2,
  \]
  wobei $y$ die Funktion $\phi \mapsto 1/r(\phi)$ und $(r(\phi), \phi)$ die
  Bahnbeschreibung mit Hilfe von Polarkoordinaten ist.
  \begin{center}
    \begin{tikzpicture}[scale=1.5,decoration={markings,mark=at position 0.5 with {\arrow{latex}}}]
      \coordinate [label=above:$M$] (M) at (0, 0);
      \coordinate (A) at (-3, 0);
      \coordinate (B) at (3, 0);
      \coordinate (C) at (0, -1);
      \coordinate (D) at (0, -1);

      \fill [black] (M) circle (2pt);

      \draw [name path=curve,->] (A) .. controls (C) and (D) .. (B);

      \path [name path=X] (M) -- (C);
      \path [name path=Y] (M) -- ++(-45:1);
      \path [name intersections={of=curve and X, by=E}];
      \path [name intersections={of=curve and Y, by=F}];
      
      \draw (M) -- node [left] {$r(0)$} (E);
      \draw (M) -- node [right] {$r(\phi)$} (F);

      \pic [draw, ->, "$\phi$", angle eccentricity=1.5] {angle=E--M--F};
    \end{tikzpicture}
  \end{center}  
  Wir legen die Richtung $\phi = 0$ so fest, daß $r(0)$ der minimale
  Abstand des Photons vom Massenschwerpunkt ist, also $y'(0) = 0$.  Im
  Falle der Beobachtungen vom 29.5.1919 ist $r(0)$ der Sonnenradius
  und $M$ die Sonnenmasse in geeigneten natürlichen Einheiten.
  Wenn wir $r(0) = 1$ (also $y(0) = 1$) normieren, so ist
  $3 M = 6{,} 5 \cdot 10^{-6}$ zu setzen.  Beachte: Im Falle $M = 0$
  wäre $y \equiv \cos$, was einer geradlinigen Bewegung entspricht
  (?).
  \begin{enumerate}
  \item
    \writtenex
    Bestimme $y$ approximativ nach Einsteins Methode: Da $M$ sehr
    klein ist, ist $y$ ungefähr $\cos$.  Daher bestimmte er eine
    approximative Lösung $y_E$, indem er die modifizierte
    Anfangswertaufgabe
    \[
      y'' + y = 3 M \cos^2(x), \quad y(0) = 1, y'(0) = 0
    \]
    exakt löste.
  \item
    \writtenex
    Bestimme $y(\pi/2)$ numerisch mit Hilfe des
    Runge--Kutta-Verfahrens (siehe Abschnitt 11.5); 50 Einzelschritte
    sollten dabei genügen.
  \item
    \oralex
    Da die Ablenkung $\Delta$ des Lichtes durch die Sonnenmasse
    äußerst gering ist und nur in Sonnennähe stattfindet, machen wir
    nur einen vernachlässigbaren Fehler, wenn wir $\Delta$ gemäß der
    folgenden Skizze bestimmen:
    \begin{center}
      \begin{tikzpicture}[scale=1.5,decoration={markings,mark=at position 0.5 with {\arrow{latex}}}]
        \coordinate [label=above:$M$] (M) at (0, 0);
        \coordinate (A) at (-3, 0);
        \coordinate (B) at (3, 0);
        \coordinate (C) at (0, -1);
        \coordinate (D) at (0, -1);

        \fill [black] (M) circle (2pt);

        \draw [name path=curve,opacity=0.5,>-] (A) .. controls (C) and (D) .. (B);

        \path [name path=X] (M) -- (C);
        \path [name path=Y] (M) -- ++(0:3);
        \path [name intersections={of=curve and X, by=E}];
        \path [name intersections={of=curve and Y, by=F}];
        
        \draw (M) -- (E);
        \draw (M) -- node [above] {$r(\pi/2)$} (F);

        \draw (E) -- (F);
        \draw (E) -- ++(3, 0) coordinate (G);
        
        \pic [draw, ->, "$\Delta/2$", angle radius=2cm, angle eccentricity=1.5] {angle=G--E--F};
      \end{tikzpicture}
    \end{center}
    Wie groß ist $\Delta$ ungefähr (Angabe in Winkelsekunden; $1'' =
    1^\circ/3600$), wenn die Werte von oben zugrundegelegt werden?

    (Der 1919 von Crommelin und Davidson gemessene Wert für die Ablenkung
    $\Delta$ war $1{,}98''\pm 0,18''$.  Durch Messungen an Radiosternen wurde
    1976 von Formalant und Sramek ein verbesserter Wert für $\Delta$
    von $1{,}777'' \pm 0{,}03''$ bestimmt.)
  \end{enumerate}
  
\item \oralex \textbf{Stetigkeit multilinearer Abbildungen.}
  Seien $E_1$, \dots, $E_n$ endlich-di\-men\-sio\-nale $\set K$-Vektorräume und
  $F$ irgendein normierter $\set K$-Vektorraum.  Zeige:
  Jede $n$-lineare Abbildung
  \[
  \phi\colon E_1 \times \dotsb \times E_n \to F
  \]
  ist bezüglich jeder Wahl von Normen auf den $E_k$ stetig.

  (Tip: Für jedes $k = 1$, \dots, $n$ wähle man eine Basis $(b_{k1},
  \dotsc, b_{km_k})$ von $E_k$ und bezeichne mit $(\lambda_{k1}, \dotsc,
  \lambda_{km_k})$ die dazu duale Basis von $E^*_k \coloneqq \Hom_{\set
    K}(E_k, \set K)$, d.\,h.~$\lambda_{ki}\colon E_k \to \set K$ ist die
  Linearform, die durch
  \[
  \forall j \in \{1, \dotsc, m_k\}\colon \lambda_{ki}(b_{kj}) =
  \delta_{ij}
  \]
  charakterisiert ist.  Dann gilt (?)
  \begin{multline*}
  \forall v = (v_1, \dotsc, v_n) \in E_1 \times \dotsb \times
  E_n\colon \\
  \phi(v) = \sum \lambda_{1i_1}(v_1) \dotsm \lambda_{ni_n}(v_n) \cdot
  \phi(b_{1i_1}, \dotsc, b_{ni_n}),
  \end{multline*}
  wobei in der Summe der Index $i_k$
  jeweils die Zahlen $1$, \dots, $m_k$ durchläuft.)

\item \writtenex \textbf{Bewegung in einem Zentralkraftfeld.}
  Es seien $E$ ein $\set R$-Banachraum, $B$ eine offene Teilmenge von
  $E$, und $g\colon B \to \set R$ eine lokal Lipschitz-stetige
  Funktion, d.\,h.:
  \[
    \forall v \in B\, \exists \epsilon \in \set R_+ \,
    \exists L \in \set R_+\, \forall v_1, v_2 \in B \cap U_\epsilon(v)
    \colon
    \abs{g(v_1) - g(v_2)} \leq L \cdot \anorm{v_1 - v_2}.
  \]
  Physiker mögen dann die Funktion
  \[
    f\colon B \to E, v \mapsto g(v) \cdot v
  \]
  als ein "`Zentralkraftfeld"' mit Zentrum $0$ interpretieren.  Es seien
  nun $y\colon I \to E$ eine Lösung der Differentialgleichung
  $y'' = f(y)$, $\xi \in I$, und $U$ der Untervektorraum von $E$, der
  von den Vektoren $y(\xi)$ und $y'(\xi)$ aufgespannt wird.  Zeige:
  \begin{enumerate}
  \item
    Es ist $y(I) \subseteq U$; sind $y(\xi)$ und $y'(\xi)$ also linear
    unabhängig, so beschreibt $y$ eine \emph{ebene Bewegung}.
    (Warum kommt diese Aufgabe erst jetzt?)
  \item
    Im Falle $\dim U = 2$ sei $\omega\colon U \times U \to \set R$
    eine \emph{Volumenform} von $U$, d.\,h.~eine alternierende
    Bilinearform (also $\omega(u, v) = - \omega(v, u)$ für alle $u$, $v \in U$
    und damit $\omega(u, u) = 0$) mit $\omega \neq 0$.  Dann
    ist die Funktion $I \to \set R, t \mapsto \omega(y(t), y'(t))$
    konstant.  Für Physiker ist hierin der
    "`Drehimpuls-Erhaltungssatz"' enthalten, der nach Integration in
    das \emph{zweite Keplersche Gesetz} übergeht.
  \end{enumerate}
  
\item \oralex \textbf{Topologische Teilräume.}
  Sei $E$ ein topologischer Raum und $M$ eine nicht leere Teilmenge
  von $E$.  Dann ist $\mathfrak T_M \coloneqq \{G \cap M \mid G \in
  \Top E\}$ eine topologische Struktur von $M$, die sog.~von $E$ oder
  von $\Top E$ auf $M$ induzierte \emph{Teilraumtopologie}.  $(M,
  \mathfrak T_M)$ heißt auch \emph{topologischer Teilraum} von $E$.
  
  Ist $d$ eine Metrik auf $E$ und $d_M\coloneqq d|(M \times M)$ (vgl.\
  Abschnitt~3.7), so ist $\Top{M, d_M}$ die von $\Top{E, d}$
  auf $M$ induzierte Teilraumtopologie.

\item \writtenex \textbf{Produkte topologischer Räume.}
    \begin{enumerate}
  \item
    \label{it:top_product}
    Sei $(E_i)_{i \in I}$ eine Familie topologischer Räume und
    \[
      E \coloneqq \prod_{i \in I} E_i \coloneqq\{p = (p_i)_{i \in I} \mid
      \forall i \in I\colon p_i \in E_i\}
    \]
    ihr Produkt.  Mit $\pr_i\colon E \to E_i, (p_i)_{i \in I} \mapsto
    p_i$, $i \in I$, bezeichnen wir die kanonischen Projektionsabbildungen.

    Dann nennen wir eine Teilmenge
    $G \subseteq E$ von $E$ \emph{offen}, wenn
    \begin{multline*}
      \forall p \in G\, \exists i_1, \dotsc, i_n \in I
      \exists U_1 \in \Open{p_{i_1}, E_{i_1}}, \dotsc,
      U_n \in \Open{p_{i_n}, E_{i_n}}\colon \\
      \pr_{i_1}^{-1}(U_1) \cap \dotsb \cap \pr_{i_n}^{-1}(U_n) \subseteq G.
    \end{multline*}
    Das System $\Top E$ der so definierten offenen Mengen von $E$ ist
    eine topologische Struktur von $E$.  Ist $G \in \Top{E_i}$, so ist
    $\pr_i^{-1}(G) \in \Top E$.
  \item
    Sind $(E_1, d_1)$, \dots, $(E_n, d_n)$ metrische Räume, so stimmt
    die topologische Struktur $\Top{E, d}$ des in
    Abschnitt~3.3 definierten metrischen
    Produktraumes $(E, d)$ mit der in~\ref{it:top_product} definierten topologischen
    Struktur $\Top E$ des Produktes der topologischen Räume $(E_i,
    \Top{E_i, d_i})_{i \in I}$ überein.
  \end{enumerate}  

\item \writtenex \textbf{Eine exotische topologische Struktur auf
    $\set R$.}
  Sei
  \[\mathfrak T \coloneqq \{\emptyset, \set R\} \cup
  \{\interval[open] a \infty \mid a \in \set R\}.
  \]
  Zeige:
  \begin{enumerate}
  \item
    Es ist $\mathfrak T$ eine topologische Struktur auf $\set R$.
  \item
    Eine Folge reeller Zahlen $(a_n)_{n \ge 0}$ konvergiert genau dann
    in $(\set R, \mathfrak T)$, wenn sie nach unten beschränkt ist.
    Gib im Falle der Konvergenz alle Grenzwerte an.
  \item
    Es ist $(\set R, \mathfrak T)$ kein Hausdorff-Raum.
  \end{enumerate}
  
\end{enumerate}  

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
