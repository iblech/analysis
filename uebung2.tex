\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2016/17}

\title{\bfseries 2.~Übung zur Analysis I} 
\author{Prof.\ Dr.\ Marc\ Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}  
\date{24.~Oktober 2016\thanks{Die bearbeiteten Übungsblätter sind bis
    9:55 Uhr am 31.~Oktober 2016 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=7]

\item \writtenex
  Sind $A$ und $B$ zwei Aussagen, so ist
  \[
    A \implies B \quad\text{äquivalent zu}\quad \lnot B \implies \lnot A.
  \]

  (Tip: Stelle für beide Aussagen die Wahrheitstafeln auf; vergleiche
  dazu den Beweis des Theorems in Abschnitt 0.3 der Vorlesung.)

\item \oralex Sei $m \in \set Z$. Sei $A_m$, $A_{m + 1}$, $A_{m + 2}$,
  \dots\ eine Folge von Aussagen. Zeige folgende Modifikationen der
  vollständigen Induktion:
  \begin{enumerate}
  \item
    \begin{description}[wide=0pt]
    \item[Voraussetzung:]
      Es gelte
      \begin{enumerate}
      \item $A_m$ ist wahr,
      \item Ist $n \in \set Z$, $n \ge m$ und ist $A_n$ wahr, so ist
        auch $A_{n + 1}$ wahr.
      \end{enumerate}
    \item[Behauptung:]
      $A_n$ ist wahr für $n \in \set Z$ mit $n \ge m$.
    \end{description}
  \item
    \begin{description}[wide=0pt]
    \item[Voraussetzung:] Es gelte: Ist $n \in \set Z$, $n \ge m$ und
      gilt $A_k$ für $m \leq k < n$, so gilt $A_n$.
    \item[Behauptung:]
      $A_n$ ist wahr für $n \in \set Z$ mit $n \ge m$.
    \end{description}
  \end{enumerate}

\item \writtenex 
  \textbf{Berechnung der Bogenlänge des Einheitskreises
    über einer Sehne (Teil 1).}  Es sei $s$ eine Sehne der Länge $\ell$ des
  Einheitskreises.  Wir wollen die Länge des Bogens $B$ über der Sehne
  $s$ nach einem von Archimedes von Syrakus vorgeschlagenem Verfahren
  bestimmen.  Dabei wird $B$ durch Sehnenzüge $\Sigma_n$ (mit $n = 1$,
  $2$, $4$, $8$, $16$, \dots) gleichlanger Sehnen angenähert.  Die
  Länge einer Sehne von $\Sigma_n$ werde mit $b_n$ bezeichnet. Dabei
  ergibt sich $b_{2n}$ aus $b_n$ gemäß der folgenden Abbildung:
  \begin{center}
    \begin{tikzpicture}
      \coordinate (P) at (170: 4cm);
      \coordinate (Q) at (10: 4cm);
      \coordinate (R) at (90: 4cm);
      \coordinate (PR) at (130: 4cm);
      \coordinate (RQ) at (50: 4cm);
      \coordinate (PPR) at (150: 4cm);

      \node [fill=black, inner sep=1pt] at (0, 0) {};

      \draw (0, 0) circle [radius=4cm];
      \draw [name path=b1] (P) -- node [above, pos=0.67] {$b_1 = \ell$} (Q);
      \draw [name path=b2] (P) -- node [above, sloped] {$b_2$} (R) -- (Q);
      \draw (P) -- node [above, sloped, fill=white] {$b_4$} (PR) -- (R) -- (RQ) -- (Q);
      
      \draw [dashed] (0, 0) -- 
      node [right] {$h_1$} (intersection of (0, 0)--R and P--Q) {}; 
      \draw [dashed] (0, 0) -- 
      node [above, sloped, pos=0.67] {$h_2$} (intersection of (0, 0)--PR and P--R) {}; 
      \draw [dashed] (0, 0) -- 
      node [above, sloped, pos=0.67] {$h_4$} (intersection of (0, 0)--PPR and P--PR) {}; 
    \end{tikzpicture}
  \end{center} 
  Eine Annäherung an die Länge des Bogens $B$
  liefert dann die Folge $(B_{2^k})_{k \in \set N_0}$ mit
  $B_{2^k} = 2^k \cdot b_{2^k}$.
  \begin{enumerate}
  \item Entwickle ein Verfahren zur rekursiven Berechnung von
    $b_{2^k}$ und damit von $B_{2^k}$.
  \item Teste Dein Verfahren mit einem (Taschen-)Rechner mit dem
    Eingabewert $\ell = 2$.
  \item Schreibe für das Verfahren ein (Scheme-)Programm.  

    (Tip: Für betraglich sehr kleines $\epsilon$ tritt numerisch in
    einem Ausdruck der Form $1 - \sqrt{1 - \epsilon}$
    \emph{Auslöschung} ein, also Verlust an Genauigkeit durch
    Subtraktion fast gleich großer Gleitkommazahlen.  In diesem Falle
    gelingt die Vermeidung der Auslöschung durch eine einfache
    Termumformung:
    $1 - \sqrt{1 - \epsilon} = \frac{(1 - \sqrt{1 - \epsilon}) \cdot
      (1 + \sqrt{1 - \epsilon})}{1 + \sqrt{1 - \epsilon}} =
    \frac\epsilon{1 + \sqrt{1 - \epsilon}}$.)
  \end{enumerate}

\item \writtenex
  \textbf{Binomialkoeffizienten.}
  Für $a \in \set R$ definieren wir die Folge der \emph{Binomial\-koeffizienten}
  $\binom a n$ für $n \in \set N_0$ rekursiv durch
  \[
  \binom a 0 \coloneqq 1 \quad \text{und} \quad
  \binom a {n + 1} \coloneqq \frac{a - n}{n + 1} \cdot \binom a n.
  \]
  Seien im folgenden $a \in \set R$ und $m$, $n \in \set N_0$. Zeige:
  \begin{enumerate}
  \item
    \label{it:binomial}
    $\displaystyle \binom a n + \binom a {n + 1} = \binom {a + 1}{n +
      1}$.
    \paragraph{Bemerkung.} In dieser Gleichung ist für die speziellen
    Werte $a \in \set N_0$ das Bildungsgesetz des \emph{Pascalschen
      Dreiecks} enthalten. Dieses ist eine Funktionstafel für die
    Werte der Binomialkoeffizienten $\binom a n$ für $a$,
    $n \in \set N_0$ mit $n \leq a$:
    \begin{equation*}
      \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)}, scale=0.8]
        \foreach \n / \k in {0/0, 1/0, 1/1, 2/0, 2/1, 2/2, 3/0, 3/1, 3/2, 3/3, 4/0, 4/1, 4/2, 4/3, 4/4}
        \path ($({1.4*(\k-1/2*\n)}, -\n)$) node {$\binom \n \k$};
      \end{tikzpicture}
      =
      \begin{tikzpicture}[baseline={([yshift=-.5ex]current bounding box.center)},scale=0.8]
        \foreach \n / \k / \x in {0/0/1, 1/0/1, 1/1/1, 2/0/1, 2/1/2, 2/2/1, 3/0/1, 3/1/3, 3/2/3, 3/3/1, 4/0/1, 4/1/4, 4/2/6, 4/3/4, 4/4/1}
        \path ($({1.4*(\k-1/2*\n)}, -\n)$) node [label=center:\x] {\phantom{$\binom \n \k$}};
      \end{tikzpicture}
    \end{equation*}
  \item
    Für $n \leq m$ gilt
    $\displaystyle \binom m n = \frac{m!}{n! (m - n)!} = \binom m {m - n}$.
  \item
    Für $n > m$ gilt $\displaystyle \binom m n = 0$.
  \item
    $\displaystyle \binom m n \in \set N_0$. 

    (Tip: Benutze \ref{it:binomial}.)
  \end{enumerate}

\end{enumerate}

\end{document}
