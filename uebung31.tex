\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2017/18}

\title{\bfseries 1.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}
\date{16.~Oktober
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
    23.~Oktober 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=215]

\item \writtenex Seien $E$ und $F$ zwei Banachräume.  Zeige: Ist
  $f\colon E \to F$ stetig und auf $E \setminus \{0\}$
  differenzierbar, so ist $f$ genau dann positiv homogen vom Grad $r$,
  wenn $f$ für $p \neq 0$ die folgende \emph{Eulersche
    Differentialgleichung}
  \[
    D_p f(p) = r \cdot f(p)
  \]
  erfüllt.  Beachte, daß es sich hierbei um eine \emph{partielle}
  Differentialgleichung handelt.

  (Tip: $y(t) \coloneqq f(t \cdot p)$.)

\item \oralex \textbf{Winkeltreue holomorpher Funktionen.}
  Es sei $f\colon G \to \set C$ eine Funktion, die auf einer offenen
  Teilmenge $G$ von $\set C$ definiert ist. Zeige:
  \begin{enumerate}
  \item
    Die Funktion $f$ ist genau dann holomorph,  wenn $f$ als Abbildung
    von $G \subseteq \set R^2$ in den $\set R^2$ im Sinne des
    Kapitels~13 differenzierbar ist und wenn in jedem Punkt $z \in G$ das
    Differential $D_z f$ eine $\set C$-lineare Abbildung ist.  Im
    Falle der Holomorphie gilt
    \[
      \forall z \in G\, \forall w \in \set C\colon D_z f(w) = f'(z)
      \cdot w.
    \]
    (Tip: Abschnitt~6.5.)
  \item
    Ist $a \in \set C \setminus \{0\}$, so stellt die $\set C$-lineare
    Abbildung $A\colon \set C \to \set C, z \mapsto a \cdot z$ eine
    \emph{Drehstreckung} dar, d.\,h.~es gibt ein $r \in \set R_+$ und
    einen Winkel $\phi \in \set R$, so daß $\forall z \in \set C\colon A z = r
    R_\phi(z)$;
    vgl.\ Abschnitt~7.14.
  \item
    Wir sagen, daß sich zwei in $0$ differenzierbare Wege $\alpha$,
    $\beta\colon \interval 0 1 \to G$ zum Zeitpunkt $0$ unter einem
    Winkel $\phi$ schneiden, wenn $z_0 \coloneqq \alpha(0) =
    \beta(0)$, $\alpha'(0) \neq 0$, $\beta'(0) \neq 0$ und
    $\measuredangle(\alpha'(0), \beta'(0)) = \phi$ gelten; vgl.\
    Abschnitt~12.10.  In dieser Situation zeige: Ist $f$
    holomorph und $f'(z_0) \neq 0$, so schneiden sich auch die
    Bildwege $f \circ \alpha$ und $f \circ \beta$ zum Zeitpunkt $0$
    unter dem Winkel $\phi$.  Diese Eigenschaft wird als die
    \emph{Winkeltreue} holomorpher Abbildungen bezeichnet.
  \end{enumerate}

\item \oralex Sei $G$ eine offene Teilmenge des Banachraumes $E$.  Es
  seien $b_1$, \dots, $b_m$ irgendwelche Vektoren des Banachraumes
  $F$, $f_1$, \dots, $f_m\colon G \to \set R$ Funktionen und
  $f \coloneqq \sum\limits_k f_k \cdot b_k$. Zeige:
  \begin{enumerate}
  \item
    Sind $f_1$, \dots, $f_m$ in $a \in G$ differenzierbar, so ist auch
    $f$ in $a$ differenzierbar, und zwar gilt:
    \[
      \forall v \in E\colon D_a f(v) = \sum_k D_a f_k(v) \cdot b_k.
    \]
  \item
    Ist $(b_1, \dotsc, b_m)$ sogar eine Basis von $F$ und ist $f$ in
    $a$ differenzierbar, so sind auch alle Funktionen $f_k$ in $a$
    differenzierbar.
  \end{enumerate}

\item \writtenex \textbf{Aus Differenzierbarkeit folgt nicht stetige
    partielle Differenzierbarkeit.}

  Zeige, daß die Abbildung
  \[
    f\colon \set R^2 \to \set R, (s, t) \mapsto \begin{cases}
      (s^2 + t^2) \cdot \sin \Biggl(\frac 1 {\sqrt{s^2 + t^2}}\Biggr) &
      \text{falls $(s, t) \neq 0,$} \\
      0 & \text{falls $(s, t) = (0, 0)$}
    \end{cases}
  \]
  differenzierbar, die partiellen Ableitungen von $f$ aber unstetig in $0$ sind.

\item \oralex Seien $E$ ein Banachraum und
  $\sum\limits_{n = 0}^\infty a_n x^n$ eine Potenzreihe mit reellen
  Koeffizienten $a_n$ und Konvergenzradius $\rho > 0$.  Zeige, daß die
  Abbildung
  (vgl.\ Abschnitt 11.8)
  \[
    f\colon U_\rho^{\Lin(E, E)}(0) \to \Lin(E, E), X \mapsto \sum_{n =
      0}^\infty a_n X^n
  \]
  in $0 \in \Lin(E, E)$ differenzierbar ist, und berechne $D_0 f$.

  (Tip: Es ist nicht schwer, die "`lineare Approximation"' von $f$ an
  der Stelle $X = 0$ zu raten.)

\item
  Wir benutzen die Notationen aus Abschnitt 13.6.
  \begin{enumerate}
  \item \oralex
    Zeige, daß jedes Polynom $P\colon E \to F$ differenzierbar ist
    und daß im Falle eines homogenen Polynoms $n$-ten Grades der Form $P
    = \phi \circ \Delta_n$ gilt:
    \[
      \forall p \in E\,
      \forall v \in E\colon
      D_p P(v) = \sum_{k = 1}^n \phi (p, \dotsc, p, \underbrace{v}_k,
      p, \dotsc, p).
    \]
    Wird $\phi$ als symmetrisch vorausgesetzt, so vereinfacht sich die
    Formel zu
    \[
      \forall p \in E\, \forall v \in E\colon
      D_p P(v) = n \cdot \phi(p, \dotsc, p, v).
    \]
  \item \oralex
    Zeige, dass die \emph{quadratischen Formen}, d\,h.~die homogenen Polynome
    $P\colon \set R^n \to \set R$ vom Grad $2$ gerade die
    Funktionen sind, die sich mittels einer symmetrischen $(n \times
    n)$-Matrix $A = (a_{ik})$ durch
    \[
      P(v) = v^\top A v = \sum_{i, k = 1}^n a_{ik} v_i
      v_k,\quad\text{also}\quad
      P = \sum_{i, k = 1}^n a_{i k} x_i x_k
    \]
    beschreiben lassen, wobei in dem Ausdruck $v^\top A v$ der Vektor
    $v$ als ein Spaltenvektor und $v^\top$ als sein Transponiertes
    betrachtet wird.
  \item \writtenex
    Wie lautet das Differential $D_A P$ des Polynoms
    \[
      P\colon \Lin(E, E) \to \Lin(E, E), A \mapsto A^3 = A \circ A
      \circ A?
    \]
  \end{enumerate}

\item Wir benutzen wieder die Notationen aus Abschnitt 13.6.  Die
  Formeln (1)--(3) sind die dortigen. Zeige:
  \begin{enumerate}
  \item \oralex
    Für jedes $r \in \{0, \dotsc, n\}$ ist die Koeffizientenfunktion
    $s_r\colon \End(E) \to \set R$ des charakteristischen Polynoms ein
    homogenes Polynom vom Grad $r$, also insbesondere differenzierbar.

    (Tip: (3).)
  \item \oralex
    Definieren wir die \emph{Adjunkte}
    \[
      \hat A \coloneqq \sum_{r = 0}^{n - 1} (-1)^r \cdot s_{n - 1 -
        r}(A) \cdot A^r \in \End(E)
    \]
    für alle $A \in \End(E)$, so gilt
    \[
      A \circ \hat A = \det(A) \cdot 1_E.
    \]

    (Tip: Satz von Cayley--Hamilton.)

    In der Matrizenrechnung entspricht $\hat A$ der
    \emph{Kofaktormatrix}.
  \item \oralex
    Es gilt
    \[
      \forall A \in \End(E)\,
      \forall X \in \End(E)\colon
      d_A \det(X) = \tr (\hat A \circ X) = \tr (X \circ \hat A),
    \]
    also insbesondere (?)
    \[
      d_{1_E} \det = \tr.
    \]

    (Tip: Benutze (2), setze zunächst $A \in
    \GL(E)$ voraus und beachte dabei (1).)
  \item \writtenex
    \label{it:det_diff}
    Es seien $A \colon J \to \End(E)$ eine stetige Funktion und
    $Y\colon J \to \End(E)$ eine Lösung der Endomorphismen-wertigen
    Differentialgleichung
    \[
      Y' = A(x) \circ Y
    \]
    (vgl.\ Abschnitt 11.11).  Zeige: Dann ist die
    Funktion
    $y \coloneqq \det \circ Y\colon J \to \set R$ eine Lösung der
    linearen Differentialgleichung
    \[
      y' = \tr(A(x)) y.
    \]
    Beweise mit diesem Ergebnis erneut (Natürlich gilt dieser Beweis dann nur
    für $\dim E < \infty$.):  Existiert ein
    $\xi \in J$, so daß $Y(\xi) \in \GL(E)$ ist, so ist $Y(J) \subseteq
    \GL(E)$.
  \item \writtenex
    Mit Hilfe von~\ref{it:det_diff} beweise
    \[
      \forall A \in \End(E)\colon \det(\exp (A)) = \exp(\tr (A)).
    \]
  \end{enumerate}

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
