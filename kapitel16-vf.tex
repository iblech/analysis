\chapter{Vektorfelder, Pfaffsche Formen und Kurvenintegrale}

\section{Vektorfelder und dynamische Systeme}

\begin{definition*}
  Sei $r \in \set N_0 \cup \{\infty\}$.
  \begin{enumerate}
  \item
    Ein \emph{$\Diff r$-Vektorfeld} auf einer offenen Teilmenge $G$
    eines Banachraumes $E$ ist eine $r$-mal stetig differenzierbare
    Funktion
    \[
      X\colon G \to E, p \mapsto X_p \coloneqq X(p).
    \]
    Wir stellen uns $X_p$ jeweils als einen in $p$ angehefteten
    Richtungsvektor vor.
  \item
    Unter einer \emph{Integralkurve} eines derartigen Vektorfeldes $X$
    verstehen wir eine auf einem offenen Intervall $J$
    definierte Kurve $\alpha\colon J \to G$, welche Lösung der
    autonomen Differentialgleichung (vgl.\ die Aufgabe aus Abschnitt~\ref{sec:ode})
    \[
      \alpha' = X \circ \alpha
    \]
    ist, für die also
    \[
      \forall t \in J\colon \alpha'(t) = X_{\alpha(t)}
    \]
    gilt.
  \end{enumerate}
\end{definition*}

\begin{example*}
  Hat ein $\Diff r$-Vektorfeld $X$ in $p$ eine Nullstelle (d.\,h.~$X_p
  = 0 \in E$), so ist $\alpha\colon \set R \to G, t \mapsto p$ die
  maximale Integralkurve (s.\,u.) von $X$ mit $\alpha(0) = p$.  In
  diesem Falle nennen wir $p$ auch eine \emph{Gleichgewichtslage} oder
  eine \emph{Singularität} des durch $X$ beschriebenen dynamischen
  Systems (s.\,u.).
\end{example*}

\begin{theorem*}
  Es sei $X$ ein $\Diff r$-Vektorfeld auf $G \subseteq E$ mit $r \ge 1$.
  \begin{enumerate}
  \item
    Zu jedem $p \in G$ existiert genau eine Integralkurve
    \[
      \alpha_p \colon J_p \to G
    \]
    von $X$ mit $0 \in J_p$ und $\alpha(0) = 0$, die in folgendem
    Sinne endeutig und maximal ist:  Ist $\alpha\colon J \to G$ eine
    weitere Integralkurve von $X$ mit $0 \in J$ und $\alpha(0) = p$,
    so ist $J \subseteq J_p$ und $\alpha = \alpha_p|J$.
  \item
    \textbf{Invariant von Integralkurven gegenüber Zeitverschiebung.}
    Ist $\alpha\colon J \to G$ eine Integralkurve von $X$ und $s \in
    \set R$, so ist auch $\beta\colon -s + J \to G, t \mapsto \alpha(t
    + s)$ eine Integralkurve von $X$.
  \item
    Es ist
    \[
      D \coloneqq \bigcup_{p \in G} (J_p \times \{p\})
    \]
    eine offene Teilmenge von $\set R \times G$, welche die Teilmenge
    $\{0\} \times G$ enthält, % XXX: Sprechweise ändern, falls
    % Top. vorgezogen wird
    und
    \[
      \Phi\colon D \to G, (t, p) \mapsto \alpha_p(t)
    \]
    %%% FIXME: Vorgriff auf Kapitel 13
    eine $\Diff r$-Abbildung (vgl.~\ref{chap:banach_diff}), der
    sogenannte
    \emph{maximale Fluß} von $X$.  Er erfaßt die Abhängigkeit der
    Integralkurven $\alpha_p$ von dem Startpunkt $p$.
  \item
    Ist das Vektorfeld $X$ \emph{vollständig}, d.\,h.~$D = \set R
    \times G$, so ist für jedes $t \in \set R$ die Abbildung
    \[
      \Phi_t\colon G \to G, p \mapsto \Phi(t, p) = \alpha_p(t)
    \]
    ein $\Diff r$-Diffeomorphismus (vgl.\
    Kapitel~\ref{chap:banach_diff}), und die Abbildung $t \mapsto
    \Phi_t$ ist ein Gruppenhomomorphismus von $\set R$ in die Gruppe
    aller $\Diff r$-Diffeomorphismen $G \to G$, also
    \[
      \forall t, s \in \set R\colon \Phi_{t + s} = \Phi_t \circ
      \Phi_s;
    \]
    insbesondere ist
    \[
      \Phi_0 = \id_G
    \]
    und
    \[
      \forall t \in \set R\colon \Phi_t^{-1} = \Phi_{-t}.
    \]
    Die Familie $(\Phi_t)_{t \in \set R}$ heißt daher auch die von $X$
    erzeugte \emph{Einparametergruppe} oder das von $X$ induzierte
    \emph{dynamische System}.    
  \end{enumerate}
\end{theorem*}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    Es sei $X$ ein konstantes Vektorfeld auf ganz $E$, etwa $X \equiv
    v$ für ein $v \in E$.  Dann ist $\alpha_p\colon \set R \to E, t
    \mapsto p + t \cdot v$ die maximale Integralkurve von $X$, die zur
    Zeit $t = 0$ durch $p$ läuft.  Daher ist $X$ ein vollständiges
    Vektorfeld mit dem maximalen Fluß
    \[
      \Phi\colon \set R \times E \to E, (t, p) \mapsto p + t \cdot v.
    \]
  \item
    Es sei $X\colon E \to E$ eine stetige lineare Abbildung, die wir
    als ein Vektorfeld betrachten.  Es ist beliebig oft
    differenzierbar und vollständig.  Sein maximaler Fluß ist
    \[
      \Phi\colon \set R \times E \to E, (t, p) \mapsto \exp(t \cdot X)
      p,
    \]
    also ist seine Einparametergruppe $(\Phi_t)_{t \in \set R}$ durch
    $\Phi_t = \exp(t \cdot X) \in \GL(E)$ gegeben.
  \end{enumerate}
\end{examples*}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
