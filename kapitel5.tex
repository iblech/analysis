\chapter{Normierte Vektorräume und unendliche Reihen}
\label{chap:normed_vector_spaces}

\section{Der Körper der komplexen Zahlen}

\begin{definition*}
  Versehen wir den reellen Vektorraum $\set R^2$, in dem wir Vektoren
  bekanntlich addieren können, mit der durch
  \[
  (a, b) \cdot (a', b') \coloneqq (a \cdot a' - b \cdot b', a \cdot b' + a' \cdot b)
  \]
  für alle $(a, b), (a', b') \in \set R^2$ definierten Multiplikation, so wird
  er zu einem Körper, dem Körper $\set C$ der \emph{komplexen Zahlen}. Mit anderen Worten gelten
  in $\set C$ die Axiome
  \ref{it:real_addition_assoc}--\ref{it:distributivity} aus Kapitel
  \ref{chap:reals}, wenn wir in ihnen stets $\set R$ durch $\set C$
  und $\set R^*$ durch $\set C^* \coloneqq \set C \setminus \{0\}$
  ersetzen. Desgleichen gelten mutatis mutandis die Sätze aus den
  Abschnitten~\ref{sec:addition}--\ref{sec:distributivity}.

  Die komplexe Zahl $(0, 1)$ wird mit $i$ bezeichnet und
  \emph{imaginäre Einheit} genannt. Es gilt $i^2 = (-1, 0)$. Mit Hilfe
  der imaginären Einheit läßt sich jede komplexe Zahl $z$ in
  der Form
  \[
  (a, b) = (a, 0) + i \cdot (b, 0)
  \]
  schreiben.  Es heißt $a$ der \emph{Realteil} von $z$ und $b$ der
  \emph{Imaginärteil}.  (Eine komplexe Zahl, deren Realteil
  verschwindet, heißt \emph{rein imaginär}.)  Damit bezeichnen wir die beiden Koordinatenfunktionen
  $x$, $y\colon \set R^2 \to \set R$ auch mit $\Re$ und $\Im$.

  Die Abbildung $\set R \to \set C, a \mapsto (a, 0)$ ist ein
  injektiver Körperhomomorphismus.  Daher identifizieren wir die
  reelle Zahl $a$ mit der komplexen Zahl $(a, 0)$; damit betrachten
  wir $\set R$ als eine Teilmenge, genauer als einen Unterkörper von
  $\set C$.  Insbesondere schreiben wir
  \[
  (a, b) = a + i \cdot b.
  \]

  Die Abbildung
  $\set C \to \set C, z \mapsto \overline z \coloneqq \Re(z) - i \cdot
  \Im(z)$
  heißt die \emph{Konjugation} in $\set C$. 

  Der \emph{Betrag} einer komplexen Zahl $z = a + i \cdot  b$ wird durch
  \[
  \abs z \coloneqq \norm 2 z = \sqrt{a^2 + b^2}
  \]
  definiert.  Für reelle Zahlen stimmt dieser Betrag mit dem unter
  \ref{sec:absolute_value} eingeführten überein.
\end{definition*}

\begin{proposition*}
  \leavevmode
  \begin{enumerate}
  \item
    Die Konjugation $\set C \to \set C, z \mapsto \overline z$ ist ein
    Körperhomomorphismus, d.\,h.~es gelten die folgenden Regeln:
    \begin{align*}
      \overline 0 & = 0, 
      & \overline 1 & = 1,
      & \overline {z + w} & = \overline z + \overline w,
      & & \text{und}
      & \overline { z \cdot w} & = \overline z \cdot \overline w
    \end{align*}
    für alle $z$, $w \in \set C$, woraus insbesondere
    $\overline{1/z} = 1/\overline z$ für alle $z \neq 0$ folgt.
    Außerdem gilt $\overline{\overline z} = z$; damit ist die
    Konjugation sogar ein Isomorphismus von Körpern, denn sie ist ihre eigene Umkehrung.
  \item
    Für Real- und Imaginärteil einer komplexen Zahl $z$ gelten die Formeln
    \begin{align*}
      \Re(z) & = (z + \overline z)/2 
      & & \text{und}
      & \Im(z) & = (z - \overline z)/2i.
    \end{align*}
  \item
    Für den Betrag einer komplexen Zahl $z$ gilt
    \[
    \abs z = \sqrt{z \cdot \overline z}.
    \]
  \end{enumerate}
\end{proposition*}

\begin{rules*}
  Sind $z$, $w \in \set C$, so gilt:
  \begin{enumerate}
  \item
    $\abs z \ge 0$,
  \item
    $z \neq 0 \implies \abs z > 0$,
  \item
    $\abs{\overline z} = \abs z$,
  \item
    $\abs{\Re(z)} \leq \abs{z}$ und $\abs{\Im(z)} \leq \abs z$,
  \item
    $\abs{z \cdot w} = \abs z \cdot \abs w$,
  \item
    \label{it:complex_triangle_inequality}
    $\abs{z + w} \leq \abs z + \abs w$.
  \end{enumerate}
\end{rules*}
Obige Regel~\ref{it:complex_triangle_inequality} heißt auch die
(komplexe) \emph{Dreiecksungleichung}.

\begin{namedthm}{Festsetzung}
  In Zukunft bezeichne $\set K$ stets einen der beiden Körper $\set R$
  oder $\set C$.  Ein Vektorraum über dem Körper $\set K$ wird kurz
  ein $\set K$-Vektorraum genannt.  Jeder $\set C$-Vektorraum kann
  auch als $\set R$-Vektorraum betrachtet werden, indem die
  Multiplikation mit Skalaren auf die reellen Zahlen beschränkt wird.
\end{namedthm}

\section{Normierte Vektorräume}
\label{sec:normed_vector_spaces}

\begin{definition}
  Sei $E$ ein $\set K$-Vektorraum. Dann heißt eine Funktion
  $\anorm \cdot\colon E \to \set R$ eine \emph{Norm} auf $E$, wenn sie
  die folgenden Axiome erfüllt:
  \begin{axiomlist}[label=(N\arabic*), start=0]
  \item $\forall v \in E\colon \anorm v \ge 0$,
  \item $\forall v \in E\colon (v = 0 \iff \anorm v = 0)$,
  \item $\forall v \in E\, \forall a \in \set K\colon \anorm{a \cdot v} = \abs a \cdot \anorm v$,
  \item
    \label{it:norm_triangle_inequality}
    $\forall v, w \in E\colon \anorm{v + w} \leq \anorm v + \anorm w$.
  \end{axiomlist}
  Das Axiom~\ref{it:norm_triangle_inequality} heißt auch \emph{Dreiecksungleichung}.

  Ist $\anorm \cdot$ eine Norm auf $E$, so nennen wir
  $(E, \anorm \cdot)$ einen \emph{normierten} $\set K$-Vektor\-raum.  Wie
  im Falle von metrischen Räumen werden wir auch sagen, daß $E$ ein
  normierter $\set K$-Vektorraum mit Norm $\anorm \cdot$ sei oder noch
  kürzer, daß $E$ ein normierter $\set K$-Vektorraum ist, dessen Norm
  wir dann in der Regel mit $\anorm \cdot$ bezeichnen.
\end{definition}

\begin{rule*}
  In normierten $\set K$-Vektorräumen gilt:  $\forall v, w \in E\colon
  \abs{\anorm v - \anorm w} \leq \anorm{v - w}$.
\end{rule*}

\begin{example}
  \label{ex:k_as_normed_vector_space}
  Der Betrag auf dem Körper $\set K$ ist aufgrund der angegebenen
  Regeln eine Norm.  Somit können wir $\set K$ als einen normierten
  Vektorraum über sich selbst auf\/fassen.
\end{example}

\begin{example}
  \label{ex:product_vector_space}
  Sind $(E_1, \norm 1 \cdot)$, \dots, $(E_n, \norm n \cdot)$ normierte
  $\set K$-Vektorräume, so erhalten wir auf dem
  \emph{Produktvektorraum} $E \coloneqq \prod\limits_{k = 1}^n E_k$
  eine Norm durch
  \[
  \anorm v \coloneqq \max\{ \norm k {\pr_k(v)} \mid k = 1, \ldots, n\}.
  \]
  Wenn nichts anderes gesagt wird, betrachten wir den
  Produktvektorraum $E$ stets in dieser Weise als normierten
  $\set K$-Vektorraum.
\end{example}
Es sei beachtet, daß auf $E = \prod\limits_{k = 1}^n E_k$ die Addition
und die Multiplikation mit Skalaren "`komponentenweise"' definiert sind.
Das bedeutet gerade, daß die kanonischen Projektionen
$\pr_k\colon E \to E_k$ lineare Abbildungen sind, die bekanntlich
folgendermaßen definiert sind:

\begin{definition}
  Eine Abbildung $A\colon E \to E'$ zwischen zwei $\set K$-Vektorräumen heißt \emph{linear}, wenn
  gilt
  \[
  \forall u, v \in E\, \forall a \in \set K\colon
  (A(u + v) = A u + A v \land A(a \cdot v) = a \cdot A v).
  \]
  Ist $A$ linear, so folgt aus der Definition insbesondere, daß dann auch
  $A(0) = 0$ und $A(-u) = - A u$.
\end{definition}

\begin{example}
  \label{ex:kn_as_normed_vector_space}
  Indem wir in Beispiel \ref{ex:product_vector_space} die Setzung
  $E_1 = \cdots = E_n = \set K$ vornehmen, erhalten wir insbesondere
  den $\set K^n$ als normierten $\set K$-Vektorraum.  In diesem Falle
  bezeichnen wir die Norm von Beispiel \ref{ex:product_vector_space} meist mit
  $\norm \infty \cdot$.
\end{example}

\begin{example}
  \label{ex:sup_norm}
  Sei $M$ eine beliebige nicht leere Menge, und sei $E$ ein
  $\set K$-Vektorraum.  Dann heißt eine Funktion $f\colon M \to E$
  beschränkt, wenn die reellwertige Funktion
  $\anorm f\colon p \mapsto \anorm{f(p)}$ beschränkt ist.  Die Menge
  $B(M, E)$ der beschränkten Funktionen ist eine $\set K$-Vektorraum, wenn
  die Addition und die Multiplikation mit Zahlen aus $\set K$ punktweise
  definiert werden, d.\,h.
  \[
  \forall p \in M\, \forall a \in \set K\colon 
  \bigl((f + g)(p) = f(p) + g(p)
  \land
  (a \cdot f)(p) = a \cdot f(p)
  \bigr).
  \]
  Dieser wird durch die Definition
  \[
  \norm \infty f \coloneqq \sup\{\anorm {f(p)} \mid p \in M\}
  \]
  zu einem normierten $\set K$-Vektorraum.  Für jedes $p \in M$ ist die Abbildung
  \[
  \hat p\colon B(M, E) \to E, f \mapsto f(p)
  \]
  linear.
\end{example}

\section{Normierte Vektorräume als metrische Räume}
\label{sec:normed_vector_spaces_as_metric_spaces}

\begin{proposition}
  Sei $(E, \anorm \cdot)$ ein normierter $\set K$-Vektorraum.  Dann wird auf $E$ eine Metrik durch
  \[
  d(v, w) \coloneqq \anorm{w - v}
  \]
  für alle $v$, $w \in E$
  definiert.  Für die diesbezüglichen $\epsilon$-Umgebungen gilt:
  \begin{align*}
    U_\epsilon(v) = v + U_\epsilon(0) & \coloneqq \{v + u \mid u \in U_\epsilon(0)\}\\
    \intertext{und}\\
    U_\epsilon(0) = \epsilon \cdot U_1(0) & \coloneqq \{\epsilon \cdot v \mid v \in U_1(0)\}.
  \end{align*}
\end{proposition}
Normierte $\set K$-Vektorräume werden stets in dieser Weise als
metrische Räume betrachtet.  Damit stehen uns in Zukunft alle
Begriffe, die wir für metrische Räume eingeführt haben, auch für
normierte $\set K$-Vektorräume zur Verfügung.

\begin{definition}
  Ein \emph{$\set K$-Banachraum} ist ein normierter
  $\set K$-Vektorraum, der (bezüglich seiner kanonischen Metrik) ein
  vollständiger metrischer Raum ist.
\end{definition}

Für die folgende Aufgabe benötigen wir noch eine Definition:
\begin{definition}
  Eine Teilmenge $M$ eines $\set K$-Vektorraumes $E$ heißt
  \emph{konvex}, wenn für je zwei Vektoren $v$, $w \in M$ auch deren
  \emph{Verbindungsstrecke}
  \[
  [v, w] \coloneqq \{v + t \cdot (w - v) = (1 - t) \cdot v + t \cdot w\mid t \in [0, 1]\}
  \]
  ganz in $M$ liegt.
\end{definition}

\begin{exercise}
  In jedem normierten $\set K$-Vektorraum sind die $\epsilon$-Umgebungen konvex.
\end{exercise}

\begin{example}
  Die Metrik des normierten Raumes $\set R$ aus Beispiel
  \ref{ex:k_as_normed_vector_space} in Abschnitt
  \ref{sec:normed_vector_spaces} ist genau die in Kapitel
  \ref{chap:metric_spaces} eingeführte; daher ist $\set R$ das
  einfachste Beispiel eines Banachraumes.  Die Metrik des normierten
  Raumes $\set C$ ist die euklidische Metrik; sie stimmt nicht mit der
  in Abschnitt \ref{sec:metric_product} eingeführten Metrik des
  $\set R^2$ überein; man vergleiche dazu Abschnitt
  \ref{sec:euclidean_metric}.  Dieser besagt aber, daß die Begriffe
  "`Stetigkeit"', "`gleichmäßige Stetigkeit"', "`Konvergenz"',
  "`Abgeschlossenheit bezüglich Limesbildung"', "`Folgenkompaktheit"',
  "`Cauchyfolge"', "`Vollständigkeit"' für die beiden
  eingeführten Metriken von $\set C \cong \set R^2$ übereinstimmen.
  Insbesondere ist $\set C$ ein Banachraum.  Weiterhin gilt: Eine
  Folge $(z_n)_{n \ge m}$ von $\set C$ konvergiert genau dann, wenn
  die beiden Folgen $(\Re (z_n))_{n \ge m}$ und
  $(\Im (z_n))_{n \ge m}$ konvergieren.
\end{example}

\begin{example}
  Die Metrik des normierten Produktvektorraumes
  $E = \prod\limits_{k = 1}^n E_k$ aus Beispiel
  \ref{ex:product_vector_space} in Abschnitt
  \ref{sec:normed_vector_spaces} stimmt mit der Metrik überein, die wir
  nach Abschnitt \ref{sec:metric_product} bei der Bildung des
  metrischen Produktraumes erhalten, wenn wir von den
  \emph{metrischen} Räumen $E_1$, \dots, $E_n$ ausgehen.  Insbesondere
  konvergiert daher eine Folge $(v_i)_{i \ge m}$ des normierten
  Produktvektorraumes $E$ genau dann, wenn die $n$ Komponentenfolgen
  $(\pr_k(v_i))_{i \ge m}$ jeweils in $E_k$ konvergieren.
\end{example}

\begin{example}
  Auf Beispiel \ref{ex:kn_as_normed_vector_space} aus Abschnitt
  \ref{sec:normed_vector_spaces} können wir natürlich die Ergebnisse
  aus den vorangegangenen Beispielen anwenden, weswegen insbesondere
  $\set R^n$ und $\set C^n$ für alle $n \in \set N_1$ Banachräume
  sind.
\end{example}

\begin{example}
  In Beispiel \ref{ex:sup_norm} aus Abschnitt
  \ref{sec:normed_vector_spaces} ist für alle $p \in M$ die Abbildung
  $\hat p\colon B(M, E) \to E, f \mapsto f(p)$ gleichmäßig stetig.
\end{example}

Für die erste Aussage der folgenden Proposition benötigen wir noch eine Definition:
\begin{definition}
  Sind $(E, d)$ und $(E', d')$ zwei metrische Räume, so heißt eine
  Bijektion $f\colon E \to E'$ eine \emph{Isometrie}, wenn gilt:
  \[
  \forall p, q \in E\colon d'(f(p), f(q)) = d(p, q).
  \]
\end{definition}

\begin{proposition}
  Sei $(E, \anorm \cdot)$ ein normierter $\set K$-Vektorraum.
  \begin{enumerate}
  \item Die Konjugation $\set C \to \set C, z \mapsto \overline z$ ist
    eine lineare Isometrie und somit eine gleichmäßig stetige
    Abbildung.
  \item Die Abbildungen $\anorm \cdot\colon E \to \set R$ und
    $E \times E \to E, (v, w) \mapsto v + w$ sind gleichmäßig stetig.
    (Dies gilt natürlich insbesondere für $E = \set K$.)
  \item
    Die Abbildung $\set K^* \to \set K, a \mapsto 1/a$ ist stetig.
  \end{enumerate}
\end{proposition}

\begin{exercise}
  \label{ex:linear_cont}
  Seien $E$ und $F$ zwei normierte $\set K$-Vektorräume und
  $A\colon E \to F$ eine lineare Abbildung.  Dann sind die folgenden
  drei Aussagen zueinander äquivalent:
  \begin{enumerate}
  \item $A$ ist in $0 \in E$ stetig.
  \item $A$ ist gleichmäßig stetig.
  \item Es existiert eine Konstante $M \in [0, \infty[$, so daß
    \label{it:operator_norm}
    \begin{equation}
      \label{eq:operator_norm}
      \forall v \in E\colon \anorm{A(v)} \leq M \cdot \anorm v.
    \end{equation}
  \end{enumerate}
  Gilt \ref{it:operator_norm}, so ist
  $M \coloneqq \sup\{\anorm{A(v)} \mid \anorm v \leq 1\}$ die kleinste
  Konstante, für welche die Abschätzung (\ref{eq:operator_norm}) gültig
  ist.  Wir werden die Konstante $M$ als \emph{Operatornorm} von $A$
  kennenlernen.
\end{exercise}

\begin{exercise}
  \label{ex:closure_of_subspace}
  Ist $E$ ein normierter Vektorraum und $U$ ein Untervektorraum von
  $E$, so ist die abgeschlossene Hülle $\overline U$ ebenfalls ein
  Untervektorraum von $E$.
\end{exercise}

\section{Produkte in normierten Vektorräumen}
\label{sec:bilinear_maps}

\begin{namedthm}{Festsetzung}
  Sind $M$, $N$ und $L$ irgendwelche Mengen und ist
  $f\colon M \times N \to L$ eine Abbildung, so definieren wir für
  jeden Punkt $p \in M$ die Abbildung
  \[
  f_p \coloneqq f(p, \cdot)\colon N \to L, q \mapsto f(p, q)
  \]
  und für jeden Punkt $q \in N$ die Abbildung
  \[
  f^q \coloneqq f(\cdot, q)\colon M \to L, p \mapsto f(p, q).
  \]
\end{namedthm}

Mit diesen Bezeichnungen können wir leicht formulieren, was wir unter
einer \emph{bilinearen} Abbildung verstehen wollen:

\begin{definition*}
  Sind $E$, $E'$ und $F$ drei $\set K$-Vektorräume, so heißt eine Abbildung
  \[
  B\colon E \times E' \to F
  \]
  \emph{bilinear}, wenn für jedes $u \in E$ und jedes $v \in E'$ die
  Abbildungen $B_u\colon E' \to F$ und $B^v\colon E \to F$ linear
  sind.
\end{definition*}

\begin{theorem*}
  Seien $E$, $E'$ und $F$ normierte $\set K$-Vektorräume und
  $B\colon E \times E' \to F$ eine bilineare Abbildung.  Dann sind die
  folgenden drei Aussagen zueinander äquivalent:
  \begin{enumerate}
  \item
    $B$ ist in $(0, 0) \in E \times E'$ stetig.
  \item
    $B$ ist auf ganz $E \times E'$ stetig.
  \item
    Es existiert eine Konstante $M \in \left[0, \infty\right[$, so daß
    \begin{equation}
      \label{eq:bilinear_norm}
      \forall (u, v) \in E \times E'\colon \anorm{B(u, v)} \leq M \cdot \anorm u \cdot \anorm v.
    \end{equation}
  \end{enumerate}
\end{theorem*}

\begin{examples*}
  Sei $E$ ein $\set K$-Vektorraum.
  Die folgenden Abbildungen sind bilineare Abbildungen, welche die
  Bedingung \eqref{eq:bilinear_norm} mit der Konstanten $M = 1$
  erfüllen:
  \begin{enumerate}
  \item
    das \emph{normale Produkt} $\set K \times \set K \to \set K, (a, b) \mapsto a \cdot b$,
  \item
    die \emph{skalare Multiplikation} $\set K \times E \to E, (a, v) \mapsto a \cdot v$,
  \item
    \label{it:scalar_product}
    das \emph{Skalarprodukt} $\set R^n \times \set R^n \to \set R, (u, v) \mapsto
    \langle u, v\rangle \coloneqq \sum\limits_{k = 1}^n u_k \cdot v_k$ und
  \item
    \label{it:cross_product}
    das \emph{Kreuzprodukt} $\set R^3 \times \set R^3 \to \set R^3,
    (u, v) \mapsto u \times v \coloneqq (u_2 v_3 - u_3 v_2, u_3 v_1 - u_1 v_3, u_1 v_2 - u_2 v_1)$.
  \end{enumerate}
  Dabei sind in den Beispielen \ref{it:scalar_product} und
  \ref{it:cross_product} die Vektorräume $\set R^n$ und $\set R^3$ mit
  der euklidischen Norm zu versehen, damit die Konstante $M$
  tatsächlich als $1$ gewählt werden kann.  Daß dies im Falle
  \ref{it:scalar_product} so ist, ist gerade die Aussage der
  \emph{Cauchy--Schwarzschen Ungleichung}.
\end{examples*}
Später werden wir weitere wichtige bilineare Abbildungen kennenlernen.

\begin{proposition}
  Seien $E$, $E'$ und $F$ normierte $\set K$-Vektorräume, sei
  $B\colon E \times E' \to F$ eine stetige bilineare Abbildung, seien $M$
  ein metrischer Raum, $p_0 \in M$, $a \in \set K$, $f$,
  $g\colon M \to E$ und $h\colon M \to E'$ in $p_0$ stetige
  Funktionen.  Dann sind auch die Funktionen
  \begin{align*}
    & f + g,
    && a \cdot f,
    && \anorm f
    && \text{und}
    && B(f, h) \coloneqq B \circ (f, h)
  \end{align*}
  in $p_0$ stetig. Insbesondere ist daher die Menge $C(M, E)$ der
  stetigen Funktionen $M \to E$ ein $\set K$-Vektorraum.  Dies gilt
  natürlich auch für $E = \set K$.  Im Falle von $E = \set C$ ist auch
  $\overline f$ in $p_0$ stetig, und im Falle $E = \set K$ und
  $f(p_0) \neq 0$ ist auch $1/f$ in $p_0$ stetig.
\end{proposition}

\begin{corollary*}
  Jede (komplexe) Polynomfunktion 
  \[
  P\colon \set C \to \set C, z \mapsto \sum_{k = 0}^n a_k \cdot z^k,
  \]
  wobei $n \in \set N_0$ und $a_0$, \dots, $a_n \in \set C$ ist
  stetig, und daher sind auch alle (komplexen) rationalen Fuktionen,
  d.\,h.~alle Funktionen, die sich als Quotient $P/Q$ zweier komplexer
  Polynomfunktionen $P$ und $Q$ scheiben lassen, überall dort stetig,
  wo sie definiert sind.  Infolge dessen gilt mutatis mutandis für
  komplexe Polynomfunktionen der Satz vom Koeffizientenvergleich aus
  Abschnitt \ref{sec:polynomials}; es ist deren Grad eindeutig
  definiert; es gilt der Satz über den euklidischen
  Divisionsalgorithmus, weswegen wir in Korollar \ref{cor:linear_factor} aus
  Abschnitt \ref{sec:polynomials} bei Vorliegen einer Nullstelle
  $\alpha \in \set C$ einer Polynomfunktion $P$ den Linearfaktor
  $z - \alpha$ von $P$ abspalten können; daher gilt auch Korollar
  \ref{cor:number_of_zeroes} aus Abschnitt \ref{sec:polynomials}.  Hier ist jedoch
  folgender wichtiger Satz hinzuzufügen:
\end{corollary*}

\begin{namedthm}{Fundamentalsatz der Algebra}
  Jede komplexe Polynomfunktion $P$ vom Grad $n \ge 1$ besitzt
  mindestens eine Nullstelle.  Daher besitzt sie eine eindeutige
  Produktdarstellung der Form
  \[
  P(z) = c \cdot \prod_{k = 1}^n (z - \alpha_k)
  \]
  mit $c \in \set C^*$ und $\alpha_1$, \dots, $\alpha_n \in \set C$.
\end{namedthm}

\begin{proposition}
  Seien $E$, $E'$ und $F$ normierte $\set K$-Vektorräume, sei
  $B\colon E \times E' \to F$ eine stetige bilineare Abbildung,
  seien $a \in \set K$, $(u_n)_{n \ge m}$ und $(v_n)_{n \ge m}$ konvergente
  Folgen in $E$ und $(w_n)_{n \ge m}$ eine konvergente Folge in $E'$.
  Es gelte:
  \begin{align*}
    \lim_{n \to \infty} u_n & = u, 
    & \lim_{n \to \infty} v_n & = v
    & & \text{und}
    & \lim_{n \to \infty} w_n & = w.
  \end{align*}
  Dann gilt auch
  \begin{align*}
    \lim_{n \to \infty} (u_n + v_n) & = u + v, \\
    \lim_{n \to \infty} (a \cdot v_n) & = a \cdot v, \\
    \lim_{n \to \infty} B(v_n, w_n) & = B(v, w), \\
    \lim_{n \to \infty} \anorm{v_n} & = \anorm v, \\
    \lim_{n \to \infty} \overline{v_n} & = \overline v
                                      && \text{falls $E = \set C,$} \\
    \intertext{und}
    \lim_{n \to \infty} (1/v_n) & = 1/v
                                      && \text{falls $E = \set K$ und $v \neq 0.$}
  \end{align*}
  Weiterhin gilt: Ist $(v_n)_{n \ge m}$ eine \emph{Nullfolge} in $E$,
  d.\,h.~$\lim\limits_{n \to \infty} v_n = 0$ und $(w_n)_{n \ge m}$ eine beschränkte Folge in $E'$,
  so ist auch $(B(v_n, w_n))_{n \ge m}$ eine Nullfolge in $F$.
\end{proposition}

\section{Konvergenz von Folgen von Abbildungen}
\label{sec:function_convergence}

In diesem Abschnitt seien $(E, d)$ ein metrischer Raum, $M$ eine
beliebige Menge, $(f_n)_{n \ge m}$ eine Folge von Abbildungen
$f_n\colon M \to E$ und $f\colon M \to E$ eine weitere Abbildung.

\begin{definition*}
  \leavevmode
  \begin{enumerate}
  \item Die Folge $(f_n)_{n \ge m}$ konvergiert \emph{punktweise}
    gegen $f$, wenn für jeden Punkt $p \in M$ die Punktfolge
    $(f_n(p))_{n \ge m}$ von $E$ gegen $f(p)$ konvergiert, wenn also
    gilt:
    \[
    \forall p \in M\,\forall \epsilon \in \set R_+\, \exists n_0 \ge m\,
    \forall n \ge n_0\colon d(f_n(p), f(p)) < \epsilon.
    \]
  \item
    Die Folge $(f_n)_{n \ge m}$ konvergiert \emph{gleichmäßig} gegen $f$, wenn gilt:
    \[
    \forall \epsilon \in \set R_+\, \exists n_0 \ge m\, \forall n \ge n_0\,
    \forall p \in M\colon d(f_n(p), f(p)) < \epsilon.
    \]
  \end{enumerate}
\end{definition*}
Offensichtlich (?) impliziert gleichmäßige Konvergenz punktweise Konvergenz.

\begin{namedthm}{1.~Vererbungssatz}
  Ist $M$ ein metrischer Raum, ist $p_0 \in
  M$, sind alle Abbildungen $f_n$ in
  $p_0$ stetig und konvergiert die Folge $(f_n)_{n \ge
    m}$ gleichmäßig gegen $f$, so ist auch $f$ in $p_0$ stetig.
\end{namedthm}

\begin{exercise*}
  Seien $M$ eine Menge, $E$ und $E'$ metrische Räume,
  $(f_n)_{n \ge m}$ eine Folge von Abbildungen $f_n\colon M \to E$,
  $f\colon M \to E$ eine weitere Abbildung und $g\colon E \to E'$ eine
  stetige Abbildung. Dann gilt:
  \begin{enumerate}
  \item
    Konvergiert die Folge $(f_n)_{n \ge m}$ punktweise gegen $f$, so konvergiert auch die Folge
    $(g \circ f_n)_{n \ge m}$ punktweise gegen $g \circ f$.
  \item Konvergiert die Folge $(f_n)_{n \ge m}$ gleichmäßig gegen $f$
    und ist $g$ sogar gleichmäßig stetig, so konvergiert auch die
    Folge $(g \circ f_n)_{n \ge m}$ gleichmäßig gegen $g \circ f$.
  \item Ist $E$ das Produkt metrischer Räume $E_1$, \dots, $E_N$, so
    konvergiert die Folge $(f_n)_{n \ge m}$ genau dann punktweise
    (bzw.~gleichmäßig) gegen $f$, wenn für jedes $k = 1$, \dots, $N$
    die Folge $(\pr_k \circ f_n)_{n \ge m}$ punktweise
    (bzw.~gleichmäßig) gegen $\pr_k \circ f$ konvergiert.
  \end{enumerate}
\end{exercise*}

\section{Der normierte Raum $B(M, E)$}
\label{sec:banach_bounded}

In diesem Abschnitt seien $M$ eine nicht leere Menge und $E$ ein
normierter $\set K$"~Vek\-tor\-raum.  Wir bezeichnen wie in Beispiel
\ref{ex:sup_norm} aus Abschnitt \ref{sec:normed_vector_spaces} mit
$B(M, E)$ den normierten $\set K$-Vektorraum der beschränkten
Funktionen $f\colon M \to E$.

\begin{theorem}
  Eine Folge $(f_n)_{n \ge m}$ von $B(M, E)$ konvergiert genau dann
  bezüglich der Supremumsnorm $\norm \infty \cdot$ gegen ein
  $f \in B(M, E)$, wenn die Folge gleichmäßig gegen $f$ konvergiert.
\end{theorem}

\begin{theorem}
  \label{thm:bounded_functions_banach}
  Ist $E$ ein Banachraum, so ist auch $B(M, E)$ ein Banachraum.
\end{theorem}

\begin{corollary*}
  Für jede nicht leere Menge $M$ und jedes $n \in \set N_1$ sind
  $B(M, \set R^n)$ und $B(M, \set C^n)$ Banachräume über $\set R$
  bzw.~$\set C$.  Hat $M$ unendlich viele Punkte, so sind diese
  Banachräume unendlich-dimensional.
\end{corollary*}

\section{Der Vektorraum $C(K, E)$}

\begin{theorem*}
  Sei $K$ ein folgenkompakter metrischer Raum, und sei $E$ ein
  normierter $\set K$-Vektorraum.  Dann ist die Menge $C(K, E)$ der
  stetigen Funktionen $f\colon K \to E$ ein abgeschlossener
  Untervektorraum von $B(K, E)$.  Ist $E$ ein Banachraum, so ist daher
  auch $C(K, E)$ ein $\set K$-Banachraum.  Insbesondere sind
  $C(K, \set R^n)$ und $C(K, \set C^n)$ für jedes $n \in \set N_1$
  Banachräume über $\set R$ bzw.~$\set C$.
\end{theorem*}

\begin{comment*}
  Mit einer genaueren Untersuchung von Funktionenräumen --- wie den
  Räumen $B(M, E)$ und $C(K, E)$ --- beschäftigt sich die
  \emph{Funktionalanalysis}.  Die dort gewonnenen Erkenntnisse sind
  z.\,B.~für die Behandlung partieller Differentialgleichungen und für
  den Aufbau der Quantenphysik von grundlegender Bedeutung.
\end{comment*}

\section{Der Begriff der unendlichen Reihe}
\label{sec:series}

In diesem Abschnitt sei $(v_n)_{n \ge m}$ eine Folge in einem normierten $\set K$-Vektorraum $E$.
\begin{definition*}
  \leavevmode
  \begin{enumerate}
  \item
    Für jedes $n \ge m$ definieren wir
    $s_n \coloneqq \sum\limits_{k = m}^n v_k$.  Die Folge
    $(s_n)_{n \ge m}$ heißt die \emph{unendliche Reihe} zur Folge
    $(v_n)_{n \ge m}$; sie wird mit $\sum\limits_{k = m}^\infty v_k$
    bezeichnet.  Der Vektor $s_n$ heißt die $n$-te \emph{Partialsumme}
    dieser unendlichen Reihe.
  \item Anstatt zu sagen, daß die Folge $(s_n)_{n \ge m}$ der
    Partialsummen gegen einen Vektor $v \in E$ konvergiert, sagen
    wir, daß die Reihe $\sum\limits_{k = m}^\infty v_k$ gegen $v$
    konvergiert; anstatt $\lim\limits_{n \to \infty} s_n = v$
    schreiben wir dann $\sum\limits_{k = m}^\infty v_k = v$.  Ist
    $E = \set R$, so sind wie bei der Konvergenz reeller Zahlenfolgen
    auch für Reihen die Grenzwerte $\infty$ und $-\infty$ zugelassen.
    Divergiert die Folge $(s_n)_{n \ge m}$, so sprechen wir natürlich
    auch von der Divergenz der Reihe $\sum\limits_{k = m}^\infty v_k$.
  \end{enumerate}
\end{definition*}

\begin{proposition*}[Ein notwendiges Konvergenzkriterium]
  Falls die Reihe $\sum\limits_{k = m}^\infty v_k$ im
  normierten $\set K$-Vektorraum $E$ konvergiert, so ist $(v_n)_{n \ge m}$ eine
  Nullfolge.
\end{proposition*}
Diese Aussage kann nicht umgekehrt werden, wie z.\,B.~die harmonische
Reihe weiter unten zeigt.

\begin{example}
  Ist $(a_n)_{n \ge m}$ eine Folge nicht negativer reeller Zahlen, so
  konvergiert die Reihe $\sum\limits_{k = m}^\infty a_k$ auf jeden
  Fall in $\widehat{\set R}$, wobei natürlich der Grenzwert $\infty$
  sein kann.
\end{example}

\begin{example}[Die geometrische Reihe]
  Ist $z \in \set C$, so konvergiert die \emph{geometrische} Reihe
  \[
  \sum_{k = 0}^\infty z^k
  \]
  für $\abs z < 1$, und zwar gegen $1/(1 - z)$, und divergiert in
  $\set C$ für $\abs z \ge 1$; hingegen konvergiert die Reihe für
  reelle $z \ge 1$ in $\widehat{\set R}$, nämlich gegen $\infty$.
\end{example}

\begin{example}[Die Dirichletsche Reihe zur Riemannschen Zeta-Funktion]
  \label{ex:zeta}
  Für jedes $s \in \set N_1$ konvergiert die Reihe $\sum\limits_{n = 1}^\infty \frac 1 {n^s}$
  in $\widehat{\set R}$ gegen ein $\zeta(s) \in \widehat{\set R}$.  Es ist
  \begin{align*}
    \zeta(1) & = \infty &
    &\text{und} &
    & \forall s \in \set N_2\colon \bigl(\zeta(s) \in \left]1, 2\right] \land
      \zeta(s + 1) \leq \zeta (s) - (1/2)^{s + 1}\bigr).
  \end{align*}
  Für gerade $s$ sind die Werte $\zeta(s)$ bekannte rationale Vielfache von $\pi^s$, so ist
  zum Beispiel
  \begin{align*}
    \zeta(2) & = \pi^2/6, 
    & \zeta(4) & = \pi^4/90,
    & \zeta(6) & = \pi^6/945
    &&\text{und}
    & \zeta(12) & = 691 \pi^{12}/638\,512\,875.
  \end{align*}
  Über die Werte zu ungeradem
  $s$ ist viel weniger bekannt.  So hat \textsc{R.~Apéry} in den
  Jahren 1978/79 bewiesen, daß
  $\zeta(3)$ irrational ist.  Außerdem ist seitdem bewiesen worden,
  daß unendlich viele Werte der Form $\zeta(s)$ mit
  $s$ ungerade irrational sein müssen.

  Für $s = 1$ heißt die fragliche Reihe die \emph{harmonische} Reihe.
\end{example}
  
\begin{exercise*}[Cauchysches Verdichtungslemma]
  Ist $(a_n)_{n \ge 1}$ eine monoton fallende Folge nicht negativer
  reeller Zahlen, so konvergiert die Reihe $\sum\limits_{k = 1}^\infty a_k$
  genau dann in $\set R$, wenn die "`verdichtete"' Reihe
  $\sum\limits_{k = 1}^\infty 2^k \cdot a_{2^k}$ in $\set R$ konvergiert.
\end{exercise*}

\section{Alternierende Reihen}

\begin{namedthm}{Das Leibnizsche Konvergenzkriterium}
  Ist $(a_n)_{n \ge m}$ eine monoton fallende Nullfolge nicht negativer reeller Zahlen, so konvergiert die \emph{alternierende} Reihe
  \[
  \sum_{k = m}^\infty (-1)^k \cdot a_k
  \]
  gegen eine reelle Zahl.
\end{namedthm}

\begin{example*}[Die alternierende harmonische Reihe]
  Die Reihe $\sum\limits_{n = 1}^\infty (-1)^n \cdot \frac 1 n$
  konvergiert in $\set R$, und zwar gegen $-\ln(2)$.
\end{example*}

\section{Rechnen mit Reihen}

\begin{proposition*}
  Seien $E$ und $F$ normierte $\set K$-Vektorräume, und seien
  $A\colon E \to F$ eine stetige lineare Abbildung, $a \in \set K$ und
  $\sum\limits_{k = m}^\infty v_k$ und
  $\sum\limits_{k = m}^\infty w_k$ zwei konvergente Reihen in $E$, und
  zwar gelte
  \begin{align*}
    \sum_{k = m}^\infty v_k & = v
    & \text{und} & 
    & \sum_{k = m}^\infty w_k & = w.
  \end{align*}
  Dann gilt auch
  \begin{align*}
    \sum_{k = m}^\infty (v_k + w_k) & = v + w,
    & \sum_{k = m}^\infty a \cdot v_k & = a \cdot v,
    & \text{und} &
    & \sum_{k = m}^\infty A(v_k) = A(v).
  \end{align*}
  Ist $E = \set R$ und gilt $v_k \leq w_k$ für alle $k \ge m$, so gilt
  auch $v \leq w$.  Die letzte Aussage bleibt auch richtig, wenn
  $v \in \{-\infty, \infty\}$ oder $w \in \{-\infty, \infty\}$.
\end{proposition*}

\section{Absolute Konvergenz}

\label{sec:absolute_convergence}

Es seien $E$ ein normierter $\set K$-Vektorraum und $(v_n)_{n \ge m}$ eine Folge in $E$.

\begin{namedthm}{Das Konvergenzkriterium von Cauchy für Reihen}
  Ist $E$ ein $\set K$-Banach\-raum, so konvergiert die Reihe
  $\sum\limits_{k = m}^\infty v_k$ genau dann, wenn gilt:
  \[
  \forall \epsilon \in \set R_+ \,
  \exists n_0 \ge m \,
  \forall n \ge n_0 \,
  \forall i \in \set N_0\colon
  \anorm {\sum_{k = n}^{n + i} v_k} < \epsilon,
  \]
\end{namedthm}

\begin{definition*}
  Die unendliche Reihe $\sum\limits_{k = m}^\infty v_k$ heißt
  \emph{absolut} konvergent, wenn die reelle Reihe
  $\sum\limits_{k = m}^\infty \anorm {v_k}$ in $\set R$ konvergiert.
\end{definition*}

\begin{theorem}
  \label{thm:absolute_convergence}
  Ein normierter Vektorraum ist genau dann ein Banachraum, wenn in ihm
  jede absolut konvergente Reihe konvergiert.
\end{theorem}
In Banachräumen können wir aufgrund dieses Satzes auf die Konvergenz
unendlicher Reihen aus der Konvergenz geeigneter reeller Reihen mit
nicht negativen Gliedern schließen.  Daher hat das folgende Theorem
ein weites Anwendungsfeld:

\begin{theorem}
  Ist $(a_n)_{n \ge m}$ eine Folge nicht negativer reeller Zahlen, so konvergiert die Folge
  $\sum\limits_{k = m}^\infty a_k$ in $\set R$, wenn mindestens eine der folgenden Bedingungen erfüllt ist:
  \begin{description}
  \item[Majorantenkriterium.] Es existiert eine Folge $(b_n)_{n \ge m}$ nicht negativer
    reeller Zahlen $b_n$ und ein $n_0 \ge m$, so daß gilt:
    \[
    \bigl(\forall k \ge n_0\colon a_k \leq b_k\bigr) \land
    \sum_{k = m}^\infty b_k < \infty.
    \]
  \item[Quotientenkriterium.]
    Es existiert ein $n_0 \ge m$ und ein $q \in \left]0, 1\right[$, so daß gilt:
    \[
    \forall n \ge n_0\colon \Bigl(a_n \neq 0 \land \frac{a_{n + 1}}{a_n} \leq q\Bigr).
    \]
  \item[Wurzelkriterium.]
    Es existiert $n_0 \ge \max\{2, m\}$ und ein $q \in \left]0, 1\right[$, so daß gilt:
    \[
    \forall n \ge n_0 \colon \sqrt[n]{a_n} \leq q.
    \]
  \end{description}
\end{theorem}

\begin{comment*}
  Zum Nachweis der Bedingung des Quotienten- oder Wurzelkriteriums ist
  unter Umständen die folgende Aussage nützlich: Ist $(c_n)_{n \ge m}$
  eine reelle Zahlenfolge mit
  $c \coloneqq \limsup\limits_{n \to \infty} c_n < 1$, so existiert zu
  jeder Zahl $q \in \left]c, 1\right[$ ein $n_0 \ge m$, so daß gilt:
  \[
  \forall n \ge n_0\colon c_n \leq q.
  \]
\end{comment*}

\begin{examples*}
  \leavevmode
  \begin{enumerate}
  \item
    Ist $(v_n)_{n \ge 1}$ eine beschränkte Folge in einem Banachraum und $s \in \set N_2$, so konvergiert die Reihe $\sum\limits_{k = 1}^\infty k^{-s} \cdot v_k$.
  \item
    Für jedes $z \in \set C$ konvergiert die Reihe $\sum\limits_{n = 0}^\infty \frac 1 {n!} \cdot z^n$; ihr Grenzwert wird mit $e^z$ oder $\exp(z)$ bezeichnet.  Die Funktion
    \[
    \exp\colon \set C \to \set C, z \mapsto e^z
    \]
    heißt die \emph{Exponentialfunktion} zur Basis $e \coloneqq \exp(1)$, der sogenannten
    \emph{Eulerschen Zahl}.
  \end{enumerate}
\end{examples*}

\begin{exercise*}
  Man zeige: Die Reihe $\sum\limits_{k = m}^\infty v_k$ eines
  normierten Vektorraumes $E$ konvergiert sicherlich nicht, wenn für
  die Folge der reellen Zahlen $a_n \coloneqq \anorm{v_n}$ eine der
  folgenden Bedingungen erfüllt ist:
  \begin{enumerate}
  \item Es existiert ein $n_0 \ge m$, so daß
    $\forall n \ge n_0\colon (a_n \neq 0 \land a_{n + 1}/a_n \ge 1)$.
  \item
    Für unendlich viele $n \ge \max\{2, m\}$ gilt $\sqrt[n]{a_n} \ge 1$.
  \end{enumerate}
\end{exercise*}

\section{Der Umordnungssatz}

\begin{theorem*}
  Sei $\sigma\colon \set N_m \to \set N_m$ eine Bijektion.
  \begin{enumerate}
  \item Ist $(a_n)_{n \ge m}$ eine Folge nicht negativer reeller
    Zahlen, so konvergieren die beiden Reihen
    $\sum\limits_{k = m}^\infty a_k$ und
    $\sum\limits_{k = m}^\infty a_{\sigma(k)}$ in $\widehat{\set R}$
    gegen denselben Grenzwert.
  \item Ist $E$ ein $\set K$-Banachraum und ist
    $\sum\limits_{k = m}^\infty v_k$ eine absolut konvergente Reihe,
    so konvergiert auch die \emph{umgeordnete} Reihe
    $\sum\limits_{k = m}^\infty v_{\sigma(k)}$ absolut, und es gilt:
    \[
    \sum_{k = m}^\infty v_{\sigma(k)} = \sum_{k = m}^\infty v_k.
    \]
  \end{enumerate}
\end{theorem*}

\begin{comment*}
  Ist $(a_n)_{n \ge m}$ eine reelle Zahlenfolge, konvergiert die
  Reihe $\sum\limits_{k = m}^\infty a_k$ in $\set R$, ist diese
  Reihe aber nicht absolut konvergent, so existiert zu jedem
  $a \in \widehat{\set R}$ eine Bijektion
  $\sigma\colon \set N_m \to \set N_m$, so daß die umgeordnete Reihe
  $\sum\limits_{k = m}^\infty a_{\sigma(k)}$ gegen $a$ konvergiert.
  Das ist die Aussage des \emph{Riemannschen Umordnungssatzes} (zu
  finden etwa in \textsc{Harro Heuser}: \textit{Lehrbuch der
    Analysis, Teil 1}).  Durch derartige Umordnungen lassen sich
  auch divergente Reihen erzeugen.  Ein Beispiel für eine
  konvergente, aber nicht absolut konvergente Reihe ist offenbar die
  alternierende harmonische Reihe.
\end{comment*}

Das Problem der Umordnung von Reihen bzw.~der "`Anordnung"' von Reihen
entsteht ganz natürlich bei der Multiplikation von Reihen, die in dem
nächsten Abschnitt behandelt wird.

\section{Produkte von Reihen}
\label{sec:bilinear_series}

Seien $E$, $E'$ und $F$ drei $\set K$-Banachräume, und sei
$B\colon E \times E' \to F$ eine stetige bilineare Abbildung
(vgl.~Abschnitt \ref{sec:bilinear_maps}). Seien
$\sum\limits_{k = 0}^\infty u_k$ und $\sum\limits_{k = 0}^\infty v_k$ zwei
unendliche Reihen in $E$ bzw.~$E'$.

\begin{definition*}
  Die Reihe $\sum\limits_{n = 0}^\infty w_n$ mit
  \[
  w_n \coloneqq \sum_{k = 0}^n B(u_k, v_{n - k})
  \]
  heißt das \emph{Cauchy-Produkt} der Reihen
  $\sum\limits_{k = 0}^\infty u_k$ und
  $\sum\limits_{k = 0}^\infty v_k$ bezüglich des "`Produktes"' $B$.
\end{definition*}


Es seien jetzt $u \in E$ und $v \in E'$ und gelte
$\sum\limits_{k = 0}^\infty u_k = u$ und
$\sum\limits_{k = 0}^\infty v_k = v$.
\begin{theorem}
  Sind die beiden Reihen $\sum\limits_{k = 0}^\infty u_k$ und
  $\sum\limits_{k = 0}^\infty v_k$ absolut konvergent, so konvergiert
  für jede "`Abzählung"', d.\,h.~Bijektion,
  $\set N_0 \to \set N_0 \times \set N_0, n \mapsto (i_n, k_n)$ die
  unendliche Reihe $\sum\limits_{n = 0}^\infty B(u_{i_n}, v_{k_n})$
  gegen $B(u, v)$.
\end{theorem}

\begin{theorem}
  Ist mindestens eine der beiden Reihen
  $\sum\limits_{k = 0}^\infty u_k$ und
  $\sum\limits_{k = 0}^\infty v_k$ absolut konvergent, so konvergiert
  das Cauchy-Produkt dieser beiden Reihen gegen $B(u, v)$.
\end{theorem}

\begin{comment*}
  Die Theoreme bleiben richtig, wenn im Falle $\set K = \set C$
  für $B$ ein Hermitesches Produkt eingesetzt wird.
\end{comment*}

\section{Die Dezimaldarstellung reeller Zahlen}
\label{sec:decimal}

\begin{exercise*}
  Man zeige:
  \begin{enumerate}
  \item
    Ist $(a_n)_{n \ge 1}$ eine Folge von Zahlen $a_n \in \{0, 1, \dotsc, 9\}$, so konvergiert die
    unendliche Reihe
    \[
    \sum_{k = 1}^\infty a_k \cdot 10^{-k}
    \]
    gegen eine reelle Zahl $a \in [0, 1]$.  Die Folge
    $(a_n)_{n \ge 1}$ nennen wir eine \emph{Dezimaldarstellung} von
    $a$.
  \item 
    \label{it:decimal_expansion}
    Jede reelle Zahl $a \in [0, 1]$ besitzt eine
    Dezimaldarstellung $(a_n)_{n \ge 1}$.  Für $a \neq 1$ kann sie
    mittels der \emph{Gauß-Klammer}
    $\gauss t \coloneqq \max\{n \in \set Z \mid n \leq t\}$ für
    $t \in \set R$ rekursiv definiert werden:
    \begin{align*}
      a_1 & \coloneqq \gauss {10 \cdot a}
      & \text{und} & 
      & \forall n \in \set N_1\colon a_{n + 1} & \coloneqq
                                                 \gauss {10^{n + 1} \cdot 
                                                 \Bigl(a - \sum_{k = 1}^n a_k \cdot 10^{-k}\Bigr)}.
    \end{align*}
  \item Jedes $a \in \left[0, 1\right[$ besitzt höchstens zwei
    Dezimaldarstellungen: Ist $(b_n)_{n \ge 1}$ eine
    Dezimaldarstellung von $a$, die von der in
    \ref{it:decimal_expansion} angegebenen abweicht, so existiert
    genau ein $m \in \set N_1$, so daß
    \begin{enumerate}
    \item
      $\forall n < m\colon a_n = b_n$,
    \item
      $a_m = b_m + 1$,
    \item
      $\forall n > m\colon \bigl(a_n = 0 \land b_n = 9\bigr)$.
    \end{enumerate}
    Insbesondere ist die Dezimaldarstellung irrationaler Zahlen
    $a \in \left]0, 1\right[$ eindeutig.
  \item Besitzt $a \in [0, 1]$ eine \emph{periodische}
    Dezimaldarstellung $(a_n)_{n \ge 1}$, d.\,h.~gilt
    \[
    \exists p \in \set N_1\, \forall n \in \set N_1\colon a_{n + p} = a_n,
    \]
    so ist $a \in \set Q$.
  \end{enumerate}
\end{exercise*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
