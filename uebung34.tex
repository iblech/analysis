\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2017/18}

\title{\bfseries 4.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}
\date{6.~November
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
    13.~November 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=231]

\item Bestimme sämtliche kritische Punkte, lokale Minima, lokale
  Maxima und Sattelpunkte der folgenden Abbildungen
  $f\colon \set R^2 \to \set R$:
  \begin{enumerate}
  \item \oralex
    $\displaystyle f(x, y) \coloneqq x^3 + 6 x y + y^3$,
  \item \writtenex
    $\displaystyle f(x, y) \coloneqq \frac{\sin(x) - 2}{1 + y^2}$.
  \end{enumerate}

\item \writtenex
  Leite "`nach Rezept"' analog zu (c) im Theorem aus Abschnitt 9.3
  ein Integralrestglied für die Taylorformel im Mehrdimensionalen her.

\item \oralex
  Eine Warenhauskette plant die Errichtung eines Warenlagers, von dem
  aus ihre Warenhäuser in $n$ etwa gleich großen Städten an den Orten
  $A_1$, $A_2$, \dots, $A_n$ beliefert werden sollen.  Um die
  Transportkosten möglichst gering zu halten, soll das Lager an einer
  Stelle $L$ gebaut werden, für die die Abstandssumme
  $r(L) \coloneqq \sum\limits_{k = 1}^n \anorm{A_k - L}$ minimal ist. Zeige:
  \begin{enumerate}
  \item
    Dieses Optimierungsproblem besitzt (mindestens) eine Lösung.
  \item
    Aus der allgemeinen Theorie leite unter der Voraussetzung $L
    \notin \{A_1, \dotsc, A_n\}$ eine notwendige Bedingung für die
    Lage von $L$ her.  (Tip: $\grad_L r$.)
  \item
    Arbeite für $n \in \{2, 3, 4\}$ die Lösung explizit aus.

    (Tip: Was bedeutet es geometrisch, wenn die Summe von $n$
    Einheitsvektoren verschwindet?  Im Falle $n = 3$ unterscheide
    die Fälle, daß alle Winkel des Dreiecks $\Delta(A_1, A_2, A_3)$
    kleiner als $120^\circ$ sind, bzw.~einer der Winkel größer oder
    gleich $120^\circ$.  Im ersten Falle reicht es, wenn die Lösung
    durch verschieben eines Sterns wie im Logo einer berühmten
    schwäbischen Automarke gefunden wird.)
  \end{enumerate}

\item \oralex \textbf{Eine fundamentale Optimierungsaufgabe.}
  \label{ex:fund_opt}
  Es seien $H$ ein Hilbertraum, $V$ ein abgeschlossener Untervektorraum
  von $H$ und $b \in H$.  Es soll folgende Optimierungsaufgabe
  behandelt werden:
  \begin{verse}
    Bestimme $v_0 \in V$ so, daß $v_0$ zu $b$ einen möglichst kleinen
    Abstand hat; es ist also dasjenige $v_0 \in V$ gesucht, so daß
    die Funktion
    \[
      V \to \set R, v \mapsto \anorm{v - b}
    \]
    in $v_0$ minimal ist.
  \end{verse}

  Zur analytischen Behandlung ist es einfacher, das Quadrat der
  fraglichen Funktion, also
  \[
    f\colon V \to \set R, v \mapsto {\anorm{v - b}}^2 = \scp{v - b}{v
      - b}
  \]
  zu minimieren.
  \begin{enumerate}
  \item
    \label{it:fund_opta}
    Zeige, daß in der oben beschriebenen Situation für ein jedes
    vorgegebene $v_0 \in V$ die folgenden
    Aussagen~\ref{it:fund_opt1}--\ref{it:fund_opt4} zueinander
    paarweise äquivalent sind:
    \begin{enumerate}
    \item
      \label{it:fund_opt1}
      Es ist $v_0$ ein kritischer Punkt von $f$.
    \item
      Der "`Verbindungsvektor"' $b - v_0$ steht senkrecht auf dem
      Unterraum $V$.
    \item Es ist $v_0$ derjenige Vektor aus $V$, der im Sinne des
      Rieszschen Darstellungssatzes aus Abschnitt~12.10 die
      Linearform
      \[
      \lambda\colon V \to \set R, v \mapsto \scp v b
      \]
      beschreibt.
    \item
      \label{it:fund_opt4}
      Es existiert ein $d \in \interval[open right] 0 \infty$, so daß
      gilt:
      \[
        \forall v \in V \colon f(v) = \scp{v - v_0}{v - v_0} + d.
      \]
    \end{enumerate}
  \item
    Folgere aus~\ref{it:fund_opta}, daß die Optimierungsaufgabe
    genau eine Lösung $v_0 \in V$ besitzt.  Geometer erkennen, daß
    $v_0$ die Orthogonalprojektion von $b$ auf den Unterraum $V$ ist.
  \item
    Zeige: Gilt $n \coloneqq \dim V < \infty$ und ist $(a_1, \dotsc, a_n)$
    eine Orthonormalbasis von $V$, so gilt
    \[
      v_0 = \sum_{i = 1}^n \scp{b}{a_i} \cdot a_i.
    \]
  \end{enumerate}

\item\oralex \textbf{Noch eine Optimierungsaufgabe.}
  \label{ex:opt3}
  Es seien $H$ ein Hilbertraum und $b$, $b_1$, \dots, $b_m$
  irgendwelche Vektoren aus $H$.  Wir wollen die folgende
  Optimierungsaufgabe betrachten:
  \begin{verse}
    Bestimme ein $m$-Tupel $(\lambda_1, \dotsc, \lambda_m) \in \set
    R^m$, so daß die Linearkombination $\sum\limits_{k = 1}^m
    \lambda_k b_k$ möglichst nahe bei $b$ liegt,  daß also
    \[
      E(\lambda_1, \dotsc, \lambda_m) \coloneqq \scp{b - \sum_{k =
          1}^m \lambda_k b_k}{b - \sum_{k = 1}^m \lambda_k b_k}
    \]
    minimiert wird.
  \end{verse}
  Man zeige, daß diese Optimierungsaufgabe Lösungen besitzt, und zwar
  sind diese gerade die Lösungen $(\lambda_1, \dotsc, \lambda_m)$ des
  linearen Gleichungssystems
  \[
    \sum_{k = 1}^m a_{ik} \cdot \lambda_k = c_i,\quad i = 1, \dotsc,
    m,
  \]
  wobei $a_{ik} \coloneqq \scp{b_i}{b_k}$ und $c_i \coloneqq
  \scp{b_i}{b}$.

  Falls die Vektoren $b_1$, \dots, $b_m$ linear unabhängig sind, so
  ist die Lösung eindeutig bestimmt.

  (Tip: Aufgabe~\ref{ex:fund_opt} mit einem bestimmten $V$ anwenden.)

\item\oralex \textbf{Funktionen zu Feldern von Stützpunkten}
  \label{ex:opt4}
  Seien die Punkte $(x_1, y_1)$, \dots, $(x_n, y_n) \in \set R^2$ ein Feld von
  Stützpunkten und $g_1$, \dots, $g_m\colon \interval a b \to \set R$
  eine Liste von Funktionen; es gelte $a \leq x_j \leq b$ für $j = 1$,
  \dots, $n$.  Gesucht sind Koeffizienten $\lambda_1$, \dots,
  $\lambda_m \in \set R$, so daß der Graph der Linearkombination
  $\sum\limits_{k = 1}^m \lambda_k \cdot g_k$ möglichst gut dem
  Stützpunktfeld angepaßt ist, d.\,h.~genauer, daß das "`Gaußsche
  Fehlerquadrat"'
  \[
    E(\lambda_1, \dotsc, \lambda_m) \coloneqq \sum_{j = 1}^n
    \Bigl(y_j - \sum_{k = 1}^m \lambda_k \cdot g_k(x_j)\Bigr)^2
  \]
  minimiert wird.

  Zeige, daß diese Optimierungsaufgabe Lösungen besitzt, und zwar
  sind diese gerade die Lösungen des linearen Gleichungssystems
  \[
    \sum_{k = 1}^m a_{ik} \cdot \lambda_k = c_i,\quad i = 1, \dotsc,
    m,
  \]
  wobei $a_{ik} \coloneqq \sum\limits_{j = 1}^n g_i(x_j) \cdot
  g_k(x_j)$ und $c_i \coloneqq \sum\limits_{j = 1}^n g_i(x_j) \cdot
  y_j$.

  (Tip: Aufgabe~\ref{ex:opt3} mit $H = \set R^n$ anwenden.)

\item \writtenex \textbf{Arithmetisches Mittel und lineare Regression.}
  Es sei ein Stützpunktfeld $(x_1, y_1)$, \dots, $(x_n, y_n) \in \set
  R^2$ gegeben, in dem nicht alle $x_j$ gleich sind.
  \begin{enumerate}
  \item Das arithmetische Mittel $\overline y$ der "`Meßwerte"' $y_1$,
    \dots, $y_n$ ist die Zahl $y$, für welche die Funktion
    \[
      y \mapsto \sum_{j = 1}^n (y - y_j)^2
    \]
    ihren minimalen Wert annimmt.

    (Tip: Diese Aussage läßt sich sehr schnell aus
    Aufgabe~\ref{ex:opt4} mit $m = 1$ und $g_1 \equiv 1$ herleiten.
    Es läßt sich aber auch ein direkter Beweis geben.)
  \item
    Als die \emph{Regressionsgerade} des Stützpunktfeldes wird
    diejenige "`Gerade"' $y(x) = \alpha x + \delta$ bezeichnet, die
    optimal zu dem Stützpunktfeld paßt, d.\,h.~für welche der Ausdruck
    \[
      \sum_{j = 1}^n \bigl(y_j - y(x_j)\bigr)^2
    \]
    minimal wird.

    Zeige: Die Regressionsgerade des Feldes $(x_1, y_1)$, \dots,
    $(x_n, y_n)$ ist durch
    \[
      \alpha = \frac{\overline{xy} - \overline x \cdot \overline
        y}{\overline{x^2} - (\overline x)^2}\quad\text{und}\quad
      \delta = \overline y - \alpha \overline x
    \]
    gegeben, wobei $\overline x$, $\overline y$, $\overline{xy}$ und
    $\overline{x^2}$ die folgenden arithmetischen Mittel sind:
    \begin{xalignat}{2}
      \overline x & \coloneqq \frac 1 n \sum_{j = 1}^n x_j, &
      \overline y & \coloneqq \frac 1 n \sum_{j = 1}^n y_j, \\
      \overline{x^2} & \coloneqq \frac 1 n \sum_{j = 1}^n x_j^2, &
      \overline{xy} & \coloneqq \frac 1 n \sum_{j = 1}^n x_j y_j.
    \end{xalignat}
    (Tip: Aufgabe~\ref{ex:opt4})
  \end{enumerate}

\item
  Es seien $E$ und $F$ Banachräume, $G$ eine offene Teilmenge von $E$,
  $f\colon G \to F$ eine $\Diff r$-Abbildung mit $r \ge 1$ und $a \in
  G$. Zeige:
  \begin{enumerate}
  \item \oralex
    \label{it:loc_surj}
    Ist das Differential $D_a f\colon E \to F$ eine surjektive
    Abbildung und besitzt sein Kern ein abgeschlossenes Komplement
    (d.\,h.~existiert ein abgeschlossener Unterraum $V$ von $E$ mit $E
    = V \oplus \ker D_a f$), so existiert ein $\rho \in \set R_+$, so
    daß für alle $\epsilon \in \interval[open left] 0 \rho$ gilt:
    \[
      f(a) \in \interior{f(U_\epsilon(a))}.
    \]
    Wir nennen diese Eigenschaft \emph{lokale Surjektivität}.

    (Tip: Betrachte $f \circ \phi$ mit
    $\phi\colon V \to E, p \mapsto a + p$ in der Nähe von $0$.)
  \item
    \writtenex
    Ist das Differential
    $D_a f\colon E \to F$ eine injektive Abbildung und besitzt sein
    Bild ein abgeschlossenes Komplement (d.\,h.~existiert ein
    abgeschlossener Unterraum $W$ von $F$ mit $F = W \oplus \im D_a
    f$, so existiert eine offene Umgebung $U \in \Open{a, G}$, so daß
    $f|U$ injektiv ist.
    Wir nennen diese Eigenschaft \emph{lokale Injektivität}.

    (Tip: Betrachte das Differential von $g\colon G \times W \to
    F, (p, q) \mapsto q + f(p)$ in $(a, 0)$.)
  \end{enumerate}
  Natürlich (?) können wir im Falle $\dim E < \infty$
  bzw.~$\dim F < \infty$ ohne Einschränkung die Existenz
  abgeschlossener Komplemente annehmen.  Das Gleiche gilt für \ref{it:loc_surj},
  wenn $E$ ein Hilbertraum ist.

\item
  \begin{enumerate}
  \item \oralex \textbf{Polarkoordinaten.}
    Die Einschränkung
    \[
      f\colon \set R_+ \times \interval[open]{-\pi}{\pi} \to \set R^2,
      (r, \phi) \mapsto (r \cos(\phi), r \sin (\phi))
    \]
    der Polarkoordinaten-Abbildung (vgl.\ Abschnitt~7.19) ist
    ein $\Diff \infty$"=Diffeomorphismus auf die geschlitzte Ebene
    $\set R^2 \setminus \{p \in \set R^2 \mid y(p) = 0, x(p) \leq
    0\}$.

    (Tip: Die Funktionaldeterminante von $f$ ist $\det (J_{(r, \phi)}
    f) = r$.)
  \item \writtenex \textbf{Kugelkoordinaten.}
    Die Einschränkung $f \coloneqq F|\set R_+ \times
    \interval[open]{0}{\pi} \times \interval[open]{-\pi}{\pi}$ der
    \emph{Kugelkoordinaten-Abbildung}
    \[
      F\colon \set R^3 \to \set R^3,
      (r, \theta, \phi) \mapsto (r \sin(\theta) \cos(\phi), r
      \sin(\theta) \sin (\phi), r \cos(\theta))
    \]
    ist ein $\Diff \infty$-Diffeomorphismus auf
    \[
      \set R^3 \setminus \{p \in \set R^3 \mid y(p) = 0, x(p) \leq
      0\}.
    \]

    (Tip: Die Funktionaldeterminante von $f$ ist $\det (J_{(r, \theta,
      \phi)}f) = r^2 \sin(\theta)$.)
  \end{enumerate}

\end{enumerate}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
