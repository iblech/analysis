\chapter{Grundlegende Theoreme über differenzierbare Abbildungen}

\section{Zwei weiterführende Versionen des Banachschen Fixpunktsatzes}

\begin{theorem*}
  \label{thm:banach_special}
  Seien $(E, d)$ ein metrischer Raum, $p_0 \in E$, $r \in \set R_+$
  und $L \in \interval[open right] 0 1$.
  \begin{enumerate}
  \item
    \label{it:banach_special1}
    Es sei $f\colon U_r(p_0) \to E$ eine Abbildung mit folgenden
    Eigenschaften:
    \begin{enumerate}
    \item
      \label{it:banach1}
      $\displaystyle \forall p, q \in U_r(p_0)\colon d(f(p), f(q))
      \leq L \cdot d(p, q),$
    \item
      \label{it:banach2}
      $\displaystyle d(p_0, f(p_0)) < (1 - L) \cdot r.$
    \end{enumerate}
    Dann ist die Banachfolge $(p_n)_{n \ge 0}$ bezüglich $f$ mit
    Startpunkt $p_0$ definierbar (vgl.\
    Abschnitt~\ref{sec:banach_simple}), sie liegt in $U_r(p_0)$ und
    konvergiert gegen einen Fixpunkt $p^* \in U_r(p_0)$ von $f$; in
    $U_r(p_0)$ ist $p^*$ der einzige Fixpunkt von $f$.  Weiterhin gilt
    \[
      d(p_n, p^*) \leq L^n \cdot r.
    \]
  \item
    Seien $\Lambda$ ein topologischer Raum (der "`Parameterraum"') und
    $g\colon \Lambda \times U_r(p_0) \to E$ eine stetige Abbildung, so
    daß für alle $\lambda \in \Lambda$ die Funktionen $g_\lambda
    \coloneqq g(\lambda, \cdot)\colon U_r (p_0) \to E$ die
    Eigenschaften~\ref{it:banach1} und~\ref{it:banach2} der Funktion
    $f$ aus \ref{it:banach_special1} erfüllen, und zwar jeweils mit
    demselben $L \in \interval[open right] 0 1$.

    Dann existiert genau eine Abbildung $h\colon \Lambda \to
    U_r(p_0)$, so daß
    \[
      \forall \lambda \in \Lambda\colon g(\lambda, h(\lambda)) =
      h(\lambda);
    \]
    und diese Funktion $h$ ist stetig.
  \end{enumerate}
\end{theorem*}

\section{Über die stetige Umkehrbarkeit einer Abbildung}

\begin{theorem*}
  Es seien $G$ eine offene Teilmenge eines Banachraumes $E$ und
  $f\colon G \to E$ eine stetige Abbildung, die von $\id_E$ nur um
  eine kontrahierende Abbildung
  abweicht, d.\,h.~es existiert ein $L \in \interval[open right] 0 1$,
  so daß für alle $p$, $q \in G$ gilt:
  \begin{equation}
    \label{eq:cont_inverse}
    \anorm{\bigl(f(p) - p\bigr) - \bigl(f(q) - q\bigr)} \leq L \cdot
    \anorm{p - q}.
  \end{equation}
  Dann ist $f$ ein \emph{Homöomorphismus in $E$}, d.\,h.~$f(G)$ ist
  eine offene Teilmenge von $E$ und $f\colon G \to f(G)$ ist ein
  Homöomorphismus; vgl.\ Abschnitt~\ref{sec:top_continuous_funct}.  In
  diesem Falle gilt für die Umkehrabbildung $\check f\colon f(G) \to G$
  außerdem: Ist $a \in G$, $b \coloneqq f(a)$ und $r \in \set R_+$ mit
  $U_r(a) \subseteq G$, so ist $U_{(1 - L) r}(b) \subseteq f(G)$ und
  \[
    \forall q \in U_{(1 - L) r}(b)\colon \anorm{\check f(q) - \check
      f(b)}
    \leq (1 - L)^{-1} \cdot \anorm{q - b}.
  \]
\end{theorem*}

\begin{comment*}
  Mit Mitteln der algebraischen Topologie läßt sich zeigen, daß im
  Falle $\dim E < \infty$ das Theorem richtig bleibt, wenn nur die
  Stetigkeit und Injektivität von $f$, nicht aber die
  Bedingung~\eqref{eq:cont_inverse} vorausgesetzt werden.
\end{comment*}

\section{Der lokale Umkehrsatz}
\label{sec:local_inverse}

\begin{theorem*}
  Seien $E$ und $F$ Banachräume, $G \subseteq E$ eine offene
  Teilmenge, $r \in \set N_1 \cup \{\infty\}$, und $f\colon G \to F$
  eine $\Diff r$-Abbildung.  Ist dann für ein $a \in G$ das
  Differential $D_a f$ ein Vektorraumisomorphismus\footnote{Es sei
    beachtet, daß nach dem Satz vom inversen Operator ein stetiger
    Vektorraumisomorphismus zwischen Banachräumen automatisch ein
    Homöomorphismus ist.}, so existiert eine Umgebung
  $U \in \Open{a, G}$, so daß $f|U$ ein $\Diff r$-Diffeomorphismus in
  $F$ ist, d.\,h.~$f(U)$ ist eine offene Teilmenge von $F$ und
  $f|U\colon U \to f(U)$ ist ein $\Diff r$-Diffeomorphismus.
\end{theorem*}

Die lineare Approximation $D_a f$ gibt also bereits einen guten
Einblick in das lokale Verhalten von $f$.  Diese Aussage wird durch
folgende Aufgabe unterstrichen:

\begin{exercise}
  Es seien $E$ und $F$ Banachräume, $G$ eine offene Teilmenge von $E$,
  $f\colon G \to F$ eine $\Diff r$-Abbildung mit $r \ge 1$ und $a \in
  G$.
  \begin{enumerate}
  \item
    \label{it:loc_surj}
    Ist das Differential $D_a f\colon E \to F$ eine surjektive
    Abbildung und besitzt sein Kern ein abgeschlossenes Komplement
    (d.\,h.~existiert ein abgeschlossener Unterraum $V$ von $E$ mit $E
    = V \oplus \ker D_a f$), so existiert ein $\rho \in \set R_+$, so
    daß für alle $\epsilon \in \interval[open left] 0 \rho$ gilt:
    \[
      f(a) \in \interior{f(U_\epsilon(a))}.
    \]
    Wir nennen diese Eigenschaft \emph{lokale Surjektivität}.

    (Tip: Man betrachte $f \circ \phi$ mit
    $\phi\colon V \to E, p \mapsto a + p$ in der Nähe von $0$.)
  \item Ist das Differential eine
    $D_a f\colon E \to F$ eine injektive Abbildung und besitzt sein
    Bild ein abgeschlossenes Komplement (d.\,h.~existiert ein
    abgeschlossener Unterraum $W$ von $F$ mit $F = W \oplus \im D_a
    f$, so existiert eine offene Umgebung $U \in \Open{a, G}$, so daß
    $f|U$ injektiv ist.
    Wir nennen diese Eigenschaft \emph{lokale Injektivität}.

    (Tip: Man betrachte das Differential von $g\colon G \times W \to
    F, (p, q) \mapsto q + f(p)$ in $(a, 0)$.)
  \end{enumerate}
  Natürlich (?) können wir im Falle $\dim E < \infty$
  bzw.~$\dim F < \infty$ ohne Einschränkung die Existenz
  abgeschlossener Komplemente annehmen.  Das Gleiche gilt für \ref{it:loc_surj},
  wenn $E$ ein Hilbertraum ist.
\end{exercise}

\begin{exercise}
  \label{ex:spherical_coords}
  \leavevmode
  \begin{description}
  \item[Polarkoordinaten.]
    Die Einschränkung
    \[
      f\colon \set R_+ \times \interval[open]{-\pi}{\pi} \to \set R^2,
      (r, \phi) \mapsto (r \cos(\phi), r \sin (\phi))
    \]
    der Polarkoordinaten-Abbildung (vgl.\ Abschnitt~\ref{sec:polar_coords}) ist
    ein $\Diff \infty$-Diffeomorphismus auf die geschlitzte Ebene
    $\set R^2 \setminus \{p \in \set R^2 \mid y(p) = 0, x(p) \leq
    0\}$.

    (Tip: Die Funktionaldeterminante von $f$ ist $\det (J_{(r, \phi)}
    f) = r$.)
  \item[Kugelkoordinaten.]
    Die Einschränkung $f \coloneqq F|\set R_+ \times
    \interval[open]{0}{\pi} \times \interval[open]{-\pi}{\pi}$ der
    \emph{Kugelkoordinaten"=Abbildung}
    \[
      F\colon \set R^3 \to \set R^3,
      (r, \theta, \phi) \mapsto (r \sin(\theta) \cos(\phi), r
      \sin(\theta) \sin (\phi), r \cos(\theta))
    \]
    ist ein $\Diff \infty$-Diffeomorphismus auf
    \[
      \set R^3 \setminus \{p \in \set R^3 \mid y(p) = 0, x(p) \leq
      0\}.
    \]

    (Tip: Die Funktionaldeterminante von $f$ ist $\det (J_{(r, \theta,
      \phi)}f) = r^2 \sin(\theta)$.)
  \end{description}
\end{exercise}

\section{Über implizit definierte Funktionen}

\begin{definition*}[Partielle Differentiale]
  Es sei $E = \prod\limits_{k = 1}^n E_k$ ein Produkt
  reeller Banachräume, $F$ ein weiterer reeller Banachraum, $G
  \subseteq E$ eine offene Teilmenge und $f\colon G \to F$ eine in $a
  = (a_1, \dotsc, a_n) \in G$ differenzierbare Funktion.  Für jedes $k
  = 1$, \dots, $n$ ist die Abbildung
  \[
    g_k\colon \pr_k(G) \to F, p \mapsto f(a_1, \dotsc, a_{k - 1}, p, a_{k
      + 1}, \dotsc, a_n)
  \]
  dann in $a_k$ differenzierbar.
  Das \emph{partielle
    Differential} von $f$ in $a$ ist durch
  \[
    \frac{\partial f}{\partial p_k}(a) \coloneqq D_{a_k} g_k.
  \]
  definiert.  Wir benutzen folgende abkürzende Schreibweise:
  \[
    \frac{\partial f}{\partial p_k}(a) \cdot w \coloneqq D_{a_k} g_k(w).
  \]
\end{definition*}

\begin{proposition*}
  In der Situation der Definition gilt die Formel
  \[
    \forall v = (v_1, \dotsc, v_n) \in E\colon
    D_a f(v) = \sum_{k = 1}^n \frac{\partial f}{\partial p_k}(a) \cdot v_k.
  \]
  Insbesondere ist
  \[
    \frac{\partial f}{\partial p_k}(a) \cdot v_k = D_a f(v)\quad
    \text{mit $v \coloneqq \{(0, \dotsc, \underbrace{v_k}_k, \dotsc, 0)\}.$}
  \]
\end{proposition*}

\begin{proof}
  \begin{small}
    Beweisidee: $g_k = f \circ i_k$ mit $i_k\colon E_k \to E, p \mapsto (a_1,
    \dotsc, a_{k - 1}, p, a_{k + 1}, \dotsc, a_n)$.
  \end{small}
\end{proof}

Nach dieser vorbereitenden Definition können wir nun das folgende
wichtige Theorem formulieren und beweisen:

\begin{theorem*}
  Seien $E_1$, $E_2$ und $F$ Banachräume und
  $E \coloneqq E_1 \times E_2$ der Produktbanachraum, dessen Punkte
  mit $(p, q)$ bezeichnet seien.  Weiterhin seien $G$ eine offene
  Teilmenge von $E$, $g\colon G \to F$ eine $\Diff r$-Funktion mit
  $r \in \set N_1 \cup \{\infty\}$, $N$ ihre Nullstellenmenge, also
  \[
    N \coloneqq \{(p, q) \in G \mid g(p, q) = 0\},
  \]
  und $(a, b)$ ein Punkt von $N$, also $g(a, b) = 0$.

  Ist dann das partielle Differential $\frac{\partial g}{\partial
    q}(a, b) \in \Lin(E_2; F)$ ein Vektorraumisomorphismus, so
  existieren Umgebungen $U_1 \in \Open{a, E_1}$ und $U_2 \in \Open{b,
    E_2}$ mit $U_1 \times U_2 \subseteq G$, so daß $N \cap (U_1 \times
  U_2)$ der Graph einer $\Diff r$-Funktion $f\colon U_1 \to U_2$ ist,
  also
  \begin{equation}
    \label{eq:implicit}
    \forall (p, q) \in U_1 \times U_2\colon
    \bigl(g(p, q) = 0 \iff q = f(p)\bigr).
  \end{equation}
  Wir sagen in diesem Falle auch, daß wir durch Auflösen der Gleichung
  $g(p, q) = 0$ nach $q$ die Funktion $f$ erhalten.
  Denn wegen~\eqref{eq:implicit} ist
  die Funktion $f\colon U_1 \to U_2$ ja gerade durch die Gleichung
  $g(p, f(p)) = 0$ festgelegt.

  Weiterhin gilt in diesem Falle, daß
  \[
    \forall (v, w) \in E_1 \times E_2\colon
    \bigl(d_{(a, b)} g(v, w) = 0 \iff w = d_a f(v)\bigr).
  \]
\end{theorem*}

\begin{example*}[Der Spezialfall $E_1 = \set R^n$, $F = E_2 = \set R^m$]
  Die kanonischen Koordinatenfunktionen des $\set R^n \times \set R^m$
  seien mit $x_1$, \dots, $x_n$, $y_1$, \dots, $y_m$ bezeichnet,
  genauer: Ist $p = (p_i) \in \set R^n$ und $q = (q_k) \in \set R^m$,
  so ist $x_i(p, q) = p_i$ und $y_k(p, q) = q_k$.  In diesem Falle
  kann die Funktion $g\colon G \to \set R^m$ durch ein $m$-Tupel
  $(g_1, \dotsc, g_m)$ von $\Diff r$-Funktionen $g_k\colon G \to \set
  R$ dargestellt werden, und die Menge $N$ ist die Menge der Lösungen
  des Gleichungssystems
  \begin{equation}
    \label{eq:implicit_system}
    \begin{aligned}
      g_1(x_1, \dotsc, x_n, y_1, \dotsc, y_m) & = 0 \\
      & \vdots \\
      g_m(x_1, \dotsc, x_n, y_1, \dotsc, y_m) & = 0.
    \end{aligned}
  \end{equation}
  Die Voraussetzung an das partielle Differential von $g$ kann jetzt
  durch
  \[
    \det\Bigl(\frac{\partial g_j}{\partial y_k}\Bigr)_{j, k = 1,
      \dotsc, m} \neq 0
  \]
  ausgedrückt werden.  Die "`implizit definierte"' Funktion $f$ ist
  jetzt ein $m$-Tupel $(f_1, \dotsc, f_m)$ von $\Diff r$-Funktionen
  $f_k\colon U_1 \to \set R$.  Wir sagen auch, daß wir $f_1$, \dots,
  $f_m$ durch Auflösen des Gleichungssystems~\eqref{eq:implicit_system} nach
  $y_1$, \dots, $y_m$ erhalten haben.
\end{example*}

\begin{exercise}
  Die Situation sei so wie im Theorem über implizit definierte
  Funktionen.  Es sei weiterhin eine \emph{stetige} Funktion $h\colon
  U \to E_2$ gegeben, wobei $U \in \Open{a, E_1}$ und $h(a) = b$ ist,
  so daß gilt:
  \[
    \forall p \in U\colon \bigl((p, h(p)) \in G \land g(p, h(p)) =
    0\bigr).
  \]
  Dann existiert eine Umgebung $U' \in \Open{a, U}$, so daß $h|U'$
  eine $\Diff r$-Funktion ist; und in der Nähe von $a$ läßt sich ihr
  Differential durch
  \[
    D_p h = - \Bigl(\frac{\partial g}{\partial q}(p, h(p))\Bigr)^{-1}
    \circ \Bigl(\frac{\partial g}{\partial p}(p, h(p))\Bigr)
  \]
  ausdrücken.
\end{exercise}

\begin{exercise}
  Es sei $V$ ein endlich-dimensionaler reeller Banachraum, $E
  \coloneqq \End(V)$ der Vektorraum aller linearen Abbildungen $V \to
  V$, $A_0 \in E$ und $\lambda_0 \in \set R$ ein Eigenwert von $A_0$
  der algebraischen Vielfachheit $1$, d.\,h.~$\lambda_0$ ist eine
  einfache Nullstelle des charakteristischen Polynoms $P_{A_0}$ von
  $A_0$.  Dann existiert eine Umgebung $U \in \Open{A_0, E}$ und eine
  $\Diff \infty$-Funktion $f\colon U \to \set R$ mit $f(A_0) =
  \lambda_0$, so daß für jedes $A \in U$ die Zahl $f(A)$ ein Eigenwert
  der Vielfachheit $1$ von $A$ ist.

  (Tip: $g\colon (A, t) \mapsto P_A(t)$ ist eine $\Diff
  \infty$-Funktion; vgl.\ Abschnitt~\ref{sec:polynom_multi}).
\end{exercise}

\section{Differenzierbare Mannigfaltigkeiten}

In der mathematischen Modellbildung tauchen an vielen Stellen Räume
auf, in denen zumindest lokal Koordinaten eingeführt werden können,
mittels derer die Position von Punkten oder die Änderung von
Funktionen beschrieben werden können.  Beispiele sind die
zweidimensionale Sphäre mit geographischen Koordinaten,
Konfigurations- und Zustandsraum mechanischer Systeme mit sogenannten
verallgemeinerten Koordinaten $q_i$ und verallgemeinerten
Geschwindigkeitskoordinaten $\dot q_i$, "`einfache"' thermodynamische
Systeme mit den "`Koordinaten"' $p$ und $V$ (Druck und Volumen),
"`die"' Raumzeit der allgemeinen Relativitätstheorie, usw. Für diese
Modelle ist der Begriff der Mannigfaltigkeit grundlegend.

\begin{definition}
  Seien $M$ ein Hausdorffraum und $E$ ein reeller Banachraum.

  Eine \emph{Karte} $(U, x)$ von $M$ von Typ $E$ ist eine offene
  Teilmenge $U$ von $M$ zusammen mit einem Homöomorphismus $x\colon U
  \to E$ \emph{in} $E$ (d.\,h.~$x(U)$ ist offen in $E$).  Mit
  $\mathfrak A^E_0(M)$ bezeichnen wir die Menge aller möglichen Karten
  von $M$ vom Typ $E$.  Ist $(U, x)$ eine Karte von Typ $\set
  R^n$ und $x = (x_1, \dotsc, x_n)$, so heißen die Funktionen $x_1$,
  \dots, $x_n\colon U \to \set R$ auch die
  \emph{Koordinatenfunktionen} der Karte.

  Sind $(U, x)$, $(V, y) \in \mathfrak A^E_0(M)$, so heißt der
  Homöomorphismus
  \[
    y \circ \check x\colon x(U \cap V) \to y(U \cap V) \subseteq E
  \]
  die \emph{Koordinatentransformation} von $(U, x)$ nach $(V, y)$.

  Eine Teilmenge $\mathfrak A \subseteq \mathfrak A^E_0(M)$ heißt
  \emph{Atlas} von $M$ vom Typ $E$, wenn
  \[
    \forall p \in M\, \exists (U, x) \in \mathfrak A\colon p \in U.
  \]

  Der Hausdorffraum $M$ heißt eine \emph{topologische
    Mannigfaltigkeit} vom Typ $E$, wenn $\mathfrak A_0^E(M)$ ein Atlas
  von $M$ ist, wenn also $M$ \emph{lokal homöomorph} zu $E$ ist.
  Topologische Mannigfaltigkeiten vom Typ $\set R^n$ heißen
  \emph{$n$-dimensionale} topologische Mannigfaltigkeiten.
\end{definition}

\begin{comment}
  Eine $n$-dimensionale topologische Mannigfaltigkeiten kann nicht
  gleichzeitig $m$-dimensional mit $m \neq n$ sein.
\end{comment}

\begin{example*}
  Wir wir in Aufgabe~\ref{ex:spherical_coords} in
  Abschnitt~\ref{sec:local_inverse} gesehen haben, induziert die
  Kugelkoordinaten"=Abbildung durch Einschränkung einen
  $\Diff \infty$-Diffeomorphismus
  \[
    \set R_+ \times \interval[open] 0 \pi \times \interval[open]
    {-\pi}{\pi}
    \to \set R^3 \setminus \{p \in \set R^3 \mid y(p) = 0 \land x(p)
    \leq 0\}.
  \]
  Daher ist die Einschränkung $f \coloneqq F|\interval[open] 0 \pi
  \times \interval[open]{-\pi}{\pi}$ der Abbildung
  \[
    F\colon \set R^2 \to \set R^3,
    (\theta, \phi) \mapsto (\sin(\theta) \cos(\phi), \sin(\theta) \sin
    (\phi),
    \cos(\theta))
  \]
  ein Homöomorphismus auf die geschlitzte Einheitssphäre
  \[
    U \coloneqq \set S^2 \setminus \{p \in \set S^2 \mid y(p) = 0
    \land x(p) \leq 0\}.
  \]
  Bezeichnen wir mit $x\colon U \to \set R^2$ die Umkehrabbildung von
  $f$, so erhalten wir mit $(U, x)$ eine Karte der Einheitssphäre.
  Ihre Koordinatenfunktionen sind allgemein als \emph{sphärische}
  Koordinaten bekannt.  Mithilfe orthogonaler Transformationen $A \in
  \Orth(\set R^3)$ erhalten wir weitere Karten $(U_A, x_A)$ vermöge
  der Setzung $U_A \coloneqq A^{-1}(U)$ und $x_A \coloneqq x \circ
  A\colon U_A \to \set R^2$.  Offenbar bildet die Gesamtheit dieser
  Karten einen Atlas der Einheitssphäre; diese ist somit eine
  topologische Mannigfaltigkeit.
\end{example*}

Auf das bekannteste Beispiel einer topologischen Mannigfaltigkeit, das
mathematische Modell des Raumes der Anschauung, gehen wir im kommenden
Abschnitt~\ref{sec:affine_space} ein.

\begin{definition}
  Seien $M$ ein Hausdorffraum und $E$ ein reeller Banachraum.  Ein
  Atlas $\mathfrak A$ von $M$ vom Typ $E$ heißt ein
  \emph{$\Diff r$-Atlas}, $r \in \set N \cup \{\infty\}$, wenn die
  Koordinatentransformationen zwischen je zwei Karten von $\mathfrak A$
  jeweils $\Diff r$-Diffeomorphismen sind.
\end{definition}

\begin{comment}
  Jeder $\Diff r$-Atlas von $M$ ist Teilmenge eines maximalen $\Diff
  r$-Atlas von $M$.
\end{comment}

Am Beispiel der Differenzierbarkeit von Funktionen zeigen wir, in
welcher Weise auf $\Diff r$-Mannigfaltigkeiten Analysis betrieben
werden kann.  Eine ausführliche Einführung in die Analysis auf
Mannigfaltigkeiten wird in jeder Differentialgeometrie-Vorlesung
gegeben.

\begin{definition}
  Sei $\mathfrak A$ ein $\Diff r$-Atlas eines Hausdorffraumes $M$.
  Eine Funktion $f\colon M \to \set R$ ist dann bezüglich $\mathfrak
  A$ eine $\Diff k$-Funktion, $k \in \{0, \dotsc, r\}$, wenn für alle
  Karten $(U, x) \in \mathfrak A$ die Abbildung
  \[
    f \circ \check x\colon x(U) \to \set R
  \]
  eine $\Diff k$-Funktion ist.
\end{definition}

\section{Affine Räume als $\Diff \infty$-Mannigfaltigkeiten}

\label{sec:affine_space}

\begin{definition*}
  Sei $E$ ein Banachraum.
  Ein \emph{affiner Raum} $(M, +)$ vom Typ $E$ ist eine nicht leere Menge $M$
  zusammen mit einer Abbildung
  \[
    E \times M \to M, (v, p) \mapsto p + v,
  \]
  welche die folgenden Axiome erfüllt:
  \begin{axiomlist}[label=(A\arabic*), start=0]
  \item
    $\displaystyle \forall p \in M\colon p + 0 = p,$
  \item
    $\displaystyle \forall p \in M\,
    \forall v, w \in E\colon (p + v) + w = p + (v + w),$
  \item
    $\displaystyle \forall p, q \in M\,
    \exists! v \in E\colon p + v = q.$
  \end{axiomlist}

  Sind $p$, $q \in M$, so heißt der eindeutige Vektor $v \in E$ mit $p
  + v = q$ der \emph{Verbindungsvektor} von $p$ nach $q$ und wird mit
  $q - p$ bezeichnet.

  Auf $M$ wird eine Metrik $d$ durch
  \[
    \forall p, q \in E\colon d(p, q) = \anorm{q - p}
  \]
  definiert.  Damit wird $M$ zu einem vollständigen metrischen Raum,
  und insbesondere zu einem Hausdorffraum.

  Ist $E$ ein endlich-dimensionaler Vektorraum der Dimension $n$, so
  heißt $M$ ein \emph{$n$-dimensionaler} affiner Raum.

  Wählen wir ein $p_0 \in E$, so ist
  \[
    x\colon M \to E, p \mapsto p - p_0
  \]
  ein Homöomorphismus auf $E$ und damit eine globale Karte von $M$,
  welche $M$ zu einer Mannigfaltigkeit vom Typ $M$ macht.  Die Menge
  dieser Karten bildet einen $\Diff \infty$-Atlas, den \emph{kanonischen
  $\Diff \infty$-Atlas} eines affinen Raumes.
\end{definition*}

Das mathematische Modell des klassischen Raumes der Anschauung ist ein
dreidimensionaler affiner Raum.  Der speziellen Relativitätstheorie
liegt ein vierdimensionaler affiner Raum zugrunde.

\section{$\Diff r$-Untermannigfaltigkeiten}

Im folgenden seien $F$ ein Banachraum, $M$ ein topologischer Teilraum
von $F$ sowie $r \in \set N_1 \cup \{\infty\}$.

Da $F$ ein Hausdorffraum ist, ist auch $M$ ein Hausdorffraum.

\begin{proposition}
  \label{prop:level1}
  Es sei $M$ in der Nähe eines Punktes $a \in M$ eine
  \emph{$\Diff r$-planierbare} Menge vom Typ $E$, d.\,h.~es existiere
  eine Umgebung $\tilde U \in \Open{a, F}$ und ein
  $\Diff r$-Diffeomorphismus $f\colon \tilde U \to E \times E'$ in das
  Produkt zweier Banachräume $E$ und $E'$, so daß
  \begin{equation}
    f(M \cap \tilde U) = (E \times \{0\}) \cap f(\tilde U).
  \end{equation}
  Bezeichnet in dieser Situation $\pr_1\colon E \times E' \to E$ die
  kanonische Projektion, so ist
  \[
    U \coloneqq M \cap \tilde U
  \]
  eine offene Umgebung von $a$ in $M$ und
  \[
    x \coloneqq \pr_1 \circ f|U \colon U \to E
  \]
  ist ein Homöomorphismus in $E$, d.\,h.~$(U, x)$ ist eine Karte des
  topologischen Raumes $M$ vom Typ $E$.
\end{proposition}

\begin{proposition}
  Sei $M$ \emph{lokal $\Diff r$-planierbar} vom Typ $E$, d.\,h., $M$ sei in der
  Nähe eines jeden Punktes $a$ von $M$ planierbar vom Typ $E$.  Die
  Menge der gemäß Proposition~\ref{prop:level1} erhaltenen Karten $(U,
  x)$ bildet einen $\Diff r$-Atlas von $M$ vom Typ $E$.

  Wir sagen auch, daß $M$ eine \emph{$\Diff r$-Untermannigfaltigkeit}
  von $F$ vom Typ $E$ ist.  Ist $\dim E = n < \infty$, so heißt $M$
  auch \emph{$n$-dimensionale Untermannigfaltigkeit} von $F$.
\end{proposition}

\begin{example*}
  Es seien $E$ und $F$ Banachräume, $G$ eine offene Teilmenge von $E$
  und $\phi\colon G \to F$ eine $\Diff r$-Abbildung.  Dann ist deren
  Graph
  \[
    M \coloneqq \{(p, \phi(p)) \mid p \in G\}
  \]
  eine $\Diff r$-Untermannigfaltigkeit von $E \times F$ vom Typ $E$.
\end{example*}

\begin{proposition}[Der Tangentialraum einer $\Diff r$-Untermannigfaltigkeit]
  Ist $M$ eine $\Diff r$-Untermannigfaltigkeit von $F$, so ist für
  jedes $a \in M$ die Menge
  \[
    T_a M \coloneqq \{\alpha'(0)\},
  \]
  wobei $\alpha\colon U_\epsilon(0) \to F$ die Menge aller $\Diff
  1$-Kurven mit $\alpha(U_\epsilon(0)) \subseteq M$ und $\alpha(0) =
  a$ durchläuft, ein abgeschlossener Untervektorraum von $F$ (also
  ebenfalls ein Banachraum), der sogenannte \emph{Tangentialraum} von
  $M$ im Punkte $a$.

  Ist $M$ eine Untermannigfaltigkeit vom Typ $E$, so induziert jede
  Karte $(U, x)$ einen stetigen Vektorraumisomorphismus
  \[
    d_a x\colon T_a M \to E, \alpha'(0) \mapsto (x \circ \alpha)'(0),
  \]
  welcher nach dem Satz vom inversen Operator sogar ein
  Homöomorphismus ist.  Im Falle, daß $M$ endlich-dimensional ist,
  gilt insbesondere
  \[
    \dim T_a M = \dim M.
  \]
\end{proposition}

\begin{proposition}
  Seien $E$ und $F$ Banachräume, $G$ eine offene Teilmenge von $F$ und
  $g\colon G \to E$ eine $\Diff r$-Funktion. Sei weiter $a \in G$, so
  daß $d_a g\colon F \to E$ surjektiv ist und $\ker d_a g \subseteq F$
  ein abgeschlossenes Komplement besitzt.  Dann existiert eine Umgebung
  $\tilde U \in \Open{a, G}$, so daß für alle $p \in \tilde U$ die
  lineare Abbildung $d_p g\colon F \to E$ surjektiv ist und $\ker d_p
  g \subseteq F$ ein abgeschlossenes Komplement besitzt und ein
  stetiger Vektorraum-Isomorphismus $\ker d_p g \to \ker d_a g$ existiert.
\end{proposition}

\begin{proof}
  \begin{small}
    Sei $F'$ ein abgeschlossenes Komplement von $\ker d_a g$ in $F$.
    Dann ist
    $\Phi\coloneqq d_a g|F'\colon F \to E$ ein stetiger
    Vektorraum-Isomorphismus.  Nach dem Satz vom inversen Operator ist
    auch die Umkehrabbildung $\check\Phi\colon E \to F'$ eine stetige
    lineare Abbildung.  Daher ist
    \[
      \Gamma\colon G \to \Lin(F'; F'), p \mapsto \check \Phi \circ d_p
      g|F'
    \]
    eine wohldefinierte stetige Abbildung mit $\Gamma(a) = 1_{F'} \in
    \GL(F')$.  Da $\GL(F')$ eine offene Teilmenge von $\Lin(F'; F')$
    ist, existiert eine Umgebung $\tilde U \in \Open{a, G}$ mit
    $\Gamma(\tilde U) \subseteq \GL(F')$.  Daraus folgt unmittelbar
    für jedes $p \in \tilde U$, daß
    $d_p g|F'\colon F' \to E$ ein stetiger Vektorraum-Isomorphismus ist;
    insbesondere ist also $d_p g$ surjektiv.

    Wir zeigen jetzt, daß
    \[
      \Psi_p\colon \ker d_p g \times F' \to F, (u, v) \mapsto u + v
    \]
    ein Vektorraumisomorphismus ist, daß also $F'$ abgeschlossenes
    Komplement von $\ker d_p g$ ist.  Zur Injektivität:  Ist $(u, v)
    \in \ker d_p g \times F'$ mit $u + v = 0$, so ist auch $d_p g(v) =
    d_p g(u + v) = 0$.  Da $d_p g|F'$ injektiv ist, folgt $v = 0$,
    also auch $u = 0$.  Zur Surjektivität: Ist $w \in F$ vorgegeben,
    so existiert aufgrund der Surjektivität von $d_p g|F'$ ein $v \in
    F'$ mit $d_p g(v) = d_p g(w)$.  Trivialerweise ist dann $u
    \coloneqq w - v \in \ker d_p g$ und $u + v = w$.

    Der gesuchte stetige Vektorraum-Isomorphismus
    $\ker d_p g \to \ker d_a g$ ist dann durch die Komposition
    $p \circ \Psi_a^{-1} \circ \Psi_p \circ j$ gegeben, wobei
    $j\colon \ker d_p g \to \ker d_p \times F', u \mapsto (u, 0)$ und
    $p\colon \ker d_a g \times F' \to \ker d_a g, (u, v) \mapsto u$.
  \end{small}
\end{proof}

\begin{theorem*}[Gleichungsdefinierte Untermannigfaltigkeiten]
  Seien $E$ und $F$ Banachräume, $G$ eine offene Teilmenge von $F$ und
  $g\colon G \to E$ eine $\Diff r$-Funktion,
  $r \in \set N_1 \cup \{\infty\}$.  Die Nullstellenmenge von $g$
  bezeichnen wir mit
  \[
    N \coloneqq \{p \in G \mid g(p) = 0\}.
  \]

  Sei $a \in N$.
  Ist dann $d_a g\colon F \to E$ surjektiv und besitzt $\ker d_a
  g \subseteq F$ ein abgeschlossenes Komplement, so ist $N$ in der
  Nähe von $a$ eine $\Diff r$-planierbare Teilmenge vom Typ $\ker d_a
  g$.

  Weiter ist die Menge $M$ der Punkte $p \in N$, für die das
  Differential $\ker d_p g\colon F \to E$ surjektiv ist, der Kern
  $\ker d_p g \subseteq F$ ein abgeschlossenes Komplement besitzt und
  für die ein stetiger Vektorraumisomorphismus
  $\ker d_p g \to \ker d_a g$ (dessen Umkehrung nach dem Satz vom
  inversen Operator dann automatisch auch stetig ist) existiert,
  einerseits eine offene Teilmenge von $N$ und andererseits eine
  $\Diff r$-Untermannigfaltigkeit von $F$ vom Typ $\ker d_a g$.  Für
  jedes $p \in M$ ist dann
  \[
    T_p M = \ker d_p g.
  \]

  Ist $\dim F = n < \infty$ und $m = \dim E$ und ist
  die Menge
  \[
    M = \{p \in N \mid \text{$d_p g\colon F \to E$ surjektiv}\}
  \]
  nicht leer, so ist $m \leq n$ und $M$
  eine $(n - m)$-dimensionale Mannigfaltigkeit von $F$.
\end{theorem*}

\begin{comment}
  Im Falle, daß $F$ ein Hilbertraum ist, können wir ohne Einschränkung die
  Existenz abgeschlossener Komplemente annehmen.
\end{comment}

\begin{comment}
  Ist $E = \set R^m$ und $g = (g_1, \dotsc, g_m)$, so bedeutet die
  Surjektivität von $d_p g\colon F \to \set R^m$ gerade die
  \emph{infinitesimale Unabhängigkeit} der "`Bestimmungsgleichungen"'
  $g_1 = \dotsb = g_m = 0$.  Dadurch wird erreicht, daß jede der
  Gleichungen $g_i(p) = 0$ den Freiheitsgrad für $p$ um eins drückt,
  so daß $M$ die Kodimension $m$ bekommt.
\end{comment}

\section{Tests für Injektivität, Surjektivität und Bijektivität des
  Differentials}

\begin{lemma}
  Seien $E$ und $F$ reelle Banachräume, $G$ eine offene Teilmenge von $E$ und
  $f\colon G \to F$ eine in $a \in G$ differenzierbare Funktion.
  \begin{enumerate}
  \item
    Ist $E = \set R^n$, so ist $d_a f$ genau dann injektiv, wenn
    $\frac{\partial f}{\partial x_1}(a)$, \dots, $\frac{\partial
      f}{\partial x_n}(a)$ linear unabhängig sind.
  \item
    Ist $F = \set R^m$ und ist $f = (f_1, \dotsc, f_m)$, so ist
    $d_a f$ genau dann surjektiv, wenn $d_a f_1$, \dots, $d_a f_m \in
    \Lin(E; \set R)$
    linear unabhängig sind.  Im Falle, daß $E$ ein Hilbertraum ist,
    ist dies genau dann der Fall, wenn $\grad_a f_1$, \dots, $\grad_a
    f_m$ linear unabhängig sind.
  \item
    Sind $E$ und $F$ endlich-dimensionale Vektorräume \emph{derselben}
    Dimension, so ist $d_a f$ surjektiv genau dann, wenn $d_a f$
    injektiv ist.
  \end{enumerate}
\end{lemma}

\begin{lemma}[Die Gramsche Determinante]
  Seien $F$ ein reeller Hilbertraum, $(a_1, \dotsc, a_n)$ ein
  Orthonormalsystem und $v_1$, \dots, $v_n \in \spann\{a_1, \dotsc,
  a_n\}$.  Für die \emph{Gramsche Determinante} $\det
  \bigl(\scp{v_i}{v_k}\bigr)$ dieser Vektoren gilt dann:
  \begin{enumerate}
  \item
    Es ist $\displaystyle\det
    \bigl(\scp{v_i}{v_k}\bigr) = \bigl(\det
    \bigl(\scp{v_i}{a_k}\bigr)\bigr)^2 \ge 0$.
  \item
    Es sind $v_1$, \dots, $v_n$ genau dann linear unabhängig, wenn
    $\det \bigl(\scp{v_i}{v_k}\bigr) \neq 0$.
  \end{enumerate}
\end{lemma}

\begin{exercise*}
  Sei $H$ ein Hilbertraum mit $\dim H \ge m$.  Dann ist die Menge
  $F(m)$ der linear unabhängigen $m$-Tupel von Vektoren $(v_1, \dotsc,
  v_m) \in H^m$ eine offene Teilmenge von $H^m$.  (Lineare
  Unabhängigkeit ist also gegenüber kleinen Störungen stabil.)

  Weiterhin ist $F(m)$ in $H^m$ dicht.  Beide Einsichten
  zusammenfassend sagen wir auch, lineare Unabhängigkeit sei eine
  \emph{generische} Eigenschaft; d.\,h.~richtig verstanden: Lineare
  Abhängigkeit ist eine Ausnahmeerscheinung.
\end{exercise*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "skript"
%%% End:
