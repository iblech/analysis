\documentclass{uebung}

\newcommand{\semester}{Sommersemester 2017}

\title{\bfseries 7.~Übung zur Analysis II}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}  
\date{5.~Juni
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
    12.~Juni 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=144]

\item \textbf{Konvexe Funktionen.}
  Seien $I \in \powerset{\set R}$ ein Intervall und $f\colon I \to
  \set R$ eine Funktion.
  \begin{enumerate}
  \item \oralex
    Zeige: Für die Konvexität von $f$ gelten die folgenden
    Charakterisierungen:
    \begin{enumerate}
    \item Ohne besondere Voraussetzungen an $f$ gilt:
      \label{it:ex_convex1}
      \[
      \begin{aligned}
        \text{$f$ konvex} \iff &
        \forall t, u, s \in I\colon \Bigl(t < u < s\\ & \implies
        \frac{f(u) - f(t)}{u - t} \leq \frac{f(s) - f(t)}{s - t} \leq
        \frac{f(s) - f(u)}{s - u}\Bigr).
      \end{aligned}
      \]
    \item
      \label{it:ex_convex2}
      Ist $f$ differenzierbar, so gilt zudem:
      \[
      \begin{aligned}
        \text{$f$ konvex} & \iff
        \text{$f'$ ist monoton wachsend} \\
        & \iff
        \forall t, s \in I\colon f(s) \ge f(t) + f'(t) \cdot (s - t).
      \end{aligned}
      \]
      (Mit anderen Worten: Der Graph von $f$ liegt oberhalb jeder
      seiner Tangenten.)
    \item
      Ist $f$ sogar zweimal differenzierbar, gilt:
      \[
      \begin{aligned}
        \text{$f$ konvex} & \iff
        f'' \ge 0.
      \end{aligned}
      \]
      (Damit ist der Graph von $f$ relativ zur kanonischen
      Durchlaufrichtung nirgends nach rechts gekrümmt.)
    \end{enumerate}
  \item \writtenex
    Zeige: Ist $f$ konvex und $a \in I^o \coloneqq \left]\inf I, \sup I\right[$,
    so gilt:
    \begin{enumerate}
    \item
      Die Funktion $(f - f(a))/(x - a)$ ist auf $I \cap
      \left]-\infty, a\right[$ monoton wachsend und nach oben
      beschränkt.  Folglich existiert die \emph{linksseitige
        Ableitung}
      \[
      f'(a-) \coloneqq \lim_{\substack{t \to a \\ t < a}} \frac{f(t)
        - f(a)}{t - a}.
      \]
      Entsprechende beweise man die Existenz der
      \emph{rechtsseitigen Ableitung}
      \[
      f'(a+) \coloneqq \lim_{\substack{t \to a \\ t > a}} \frac{f(t)
        - f(a)}{t - a}.
      \]
      
      (Tip: Benutze~\ref{it:ex_convex1}.)
    \item
      Daher (?) ist $f$ auf $I^o$ stetig.
    \item
      Es ist $f'(a-) \leq f'(a+)$, und in Verallgemeinerung der
      Abschätzung aus~\ref{it:ex_convex2} gilt ohne jegliche
      Differenzierbarkeitsvoraussetzung
      \[
      \forall c \in \left[f'(a-), f'(a+)\right]\, \forall t \in
      I\colon f(t) \ge f(a) + c \cdot (t - a).
      \]
    \end{enumerate}
  \end{enumerate}

\item \oralex \textbf{Die Funktion ${\abs x}^\alpha\colon \set R \to \set
    R$, $\alpha \in \set R_+$.} Zeige:
  \begin{enumerate}
  \item
    \label{it:jensen}
    Definieren wir für $\alpha \in \set R_+$ den Funktionswert
    $x^\alpha(0) \coloneqq 0$, so haben wir die
    in Abschnitt 7.9 definierte
    Funktion $x^\alpha\colon \set R_+ \to \set R$ zu einer
    \emph{stetigen}, streng monoton wachsenden Funktion
    $x^\alpha\colon \left[0, \infty\right[ \to \set R$ fortgesetzt.
    Damit ist natürlich (?) auch
    \[
    {\abs x}^\alpha\colon \set R \to \set R, t \mapsto x^{\alpha}(\abs
    t)
    \]
    eine stetige Funktion.
  \item
    Für $\alpha > 1$ ist ${\abs x}^\alpha$ auf ganz $\set R$ stetig
    differenzierbar mit der Ableitung $({\abs x}^\alpha)'(0) = 0$.
  \item
    Für $\alpha \ge 1$ ist ${\abs x}^\alpha$ eine konvexe Funktion.
  \end{enumerate}

\item \writtenex \textbf{Die Jensensche Ungleichung.}
  Seien $I \in \powerset{\set R}$ ein Intervall und $f\colon I \to
  \set R$ eine konvexe Funktion.  Sei $I^o \coloneqq \left]\inf I,
    \sup I\right[$. Zeige:
  \begin{enumerate}
  \item
    Sind $t_1$, \dots, $t_n \in I$ und $\lambda_1$, \dots, $\lambda_n
    \in [0, 1]$ und gilt $\sum\limits_{k = 1}^n \lambda_k = 1$, so ist
    \[
    t^* \coloneqq \sum_{k = 1}^n \lambda_k t_k \in
    I\quad\text{und}\quad
    f(t^*) \leq \sum_{k = 1}^n \lambda_k f(t_k).
    \]

    (Tip: Aufgabe 142.)
  \item
    Sind $\phi$, $g\colon [a, b] \to \set R$ Regelfunktionen, $m$, $M
    \in I^o$ und gelten
    \[
    m \leq \phi \leq M, \quad g \ge 0\quad\text{und}\quad
    G \coloneqq \int_a^b g \, dx > 0,
    \]
    so gilt in Verallgemeinerung des ersten Aufgabenteils
    \begin{multline*}
    t^* \coloneqq G^{-1} \cdot \int_a^b \phi g \, dx \in I,\quad
    f \circ \phi \in R([a, b], \set R),\\ \text{und}\quad
    f(t^*) \leq G^{-1} \cdot \int_a^b (f \circ \phi) \cdot g \, dx.
    \end{multline*}

    (Die letzte Ungleichung heißt \emph{Jensensche Ungleichung}.)

    (Tip: Aufgabe 143, Aufgabe 126, Aufgabe 131.)
  \item
    Seien $E$ ein $\set K$-Banachraum und $g \in R([a, b], \set R)$
    eine Regelfunktion mit $g \ge 0$ und $G \coloneqq \int_a^b g \, dx >
    0$.  Hiermit definieren wir für jedes $p \in \set R_+$ die
    Funktion
    \[
    N_p\colon R([a, b], E) \to \set R,
    f \mapsto \Bigl(G^{-1} \cdot \int_a^b {\anorm f}^p \cdot g
    \, dx\Bigr)^{1/p}.
    \]
    Die Zahl $N_p(f)$ ist \emph{ein} normiertes Maß dafür, wie stark
    sich die Funktion $f$ von der Nullfunktion unterscheidet, und zwar
    bezüglich der \emph{Dichtefunktion} $g$.  Die Normierung ist so
    getroffen, daß $N_p(1) = 1$.  Die Funktion $N_p$ taucht im
    Zusammenhang mit der sogenannten $p$-Norm, einem
    funktionalanalytischem Begriff, auf.  Man zeige nun:
    \[
    \forall \alpha, \beta \in \set R_+\colon (\alpha \leq \beta
    \implies N_\alpha \leq N_\beta).
    \]

    (Tip: ${\abs x}^{\beta/\alpha}$ ist konvex.)
  \end{enumerate}

\item \oralex
  Sei
  \[
  g\colon \set R_+ \to \set R, t \mapsto \begin{cases}
    \frac 1 t + t - 2 & \text{für $t \leq 1$ und} \\
    0 & \text{für $t > 1$}.
  \end{cases}
  \]
  Zeige, daß $g$ stetig differenzierbar ist, und daß $g$ nicht zweimal
  in $1$ differenzierbar ist.

\item \writtenex
  Zeige: Sei $f\colon \left]-r, r\right[ \to E$ mit $r \in \set R_+$ eine
  differenzierbare, gerade Funktion, die in $0$ noch ein zweites Mal
  differenzierbar ist.  Dann ist die Funktion $g\colon \left[0,
    r^2\right[ \to E, t \mapsto f(\sqrt t)$ differenzierbar mit der
  Ableitung $g'(0) = f''(0)/2$.  Die Ableitung $g'$ ist in $0$ stetig,
  und es gilt (natürlich) $f = g(x^2)$.

\item \oralex
  Zeige: Es sei $f\colon I \to \set R$ eine $(n + m)$-mal stetig
  differenzierbare Funktion mit $n$, $m \in \set N_0$ und $n \ge 2$,
  die an einer Stelle $a \in I$ eine $n$-fache Nullstelle besitzt
  und deren $n$-te Ableitung an der Stelle $a$ positiv ist.
  Auch wenn die $n$-te Wurzel in $0$ nicht differenzierbar ist, so
  existiert doch in einer Umgebung $U_\epsilon(a) \cap I$ eine
  mindestens $(m + 1)$-mal differenzierbare Funktion $g$ mit
  $f|U_\epsilon(a) \cap I = g^n$.

\item \writtenex
  Es seien $I \in \powerset{\set R}$ ein Intervall, $I^o
  \coloneqq \left]\inf I, \sup I\right[$, $a \in I^o$ und $f\colon I
  \to \set R$ eine stetige Funktion.  Wir sagen dann, daß $(a, f(a))$
  ein \emph{Wendepunkt} von $f$ ist, wenn es ein $\epsilon \in \set
  R_+$ mit $U_\epsilon(a) \subseteq I$ gibt, so daß entweder
  \begin{alignat*}{5}
    & \text{$f|\left]a - \epsilon, a\right[$ konvex}\quad&
    & \text{und} & \quad 
    & \text{$f|\left]a, a + \epsilon\right[$ konkav}
    \\
    \intertext{oder}
    & \text{$f|\left]a - \epsilon, a\right[$ konkav}\quad&
    & \text{und} & \quad 
    & \text{$f|\left]a, a + \epsilon\right[$ konvex}
  \end{alignat*}
  ist.

  Zeige: Ist $f$ auf $I$ eine $2n$-mal differenzierbare Funktion,
  die in $a$ sogar $(2n + 1)$-mal differenzierbar ist, und gilt
  \[
  f^{(2)}(a) = f^{(3)}(a) = \dotsb = f^{(2n)}(a) =
  0\quad\text{und}\quad
  f^{(2n + 1)}(a) \neq 0,
  \]
  so ist $(a, f(a))$ ein Wendepunkt von $f$.

  (Tip: Taylorformel für $f''$.)

\item
  In dieser Aufgabe soll gezeigt werden, daß die Funktion
  \[
  f\colon \set R \to \set R, t \mapsto \begin{cases}
    \exp(-\frac 1 t ) & \text{für $t \in \set R_+,$} \\
    0 & \text{sonst}
  \end{cases}
  \]
  beliebig oft differenzierbar ist.
  Dazu führe die folgenden Schritte durch:
  \begin{enumerate}
  \item \oralex
    Für $n \in \set N_0$ sei
    \[
    f_n\colon \set R \to \set R, t \mapsto \begin{cases}
      t^{-n} \cdot \exp(-\frac 1 t ) & \text{für $t \in \set R_+,$} \\
      0 & \text{sonst}
  \end{cases}
    \]
    Offenbar ist $f_0 = f$.  Nun zeige man der Reihe nach für alle $n
    \in \set N_0$:
    \begin{enumerate}
    \item
      $f_n$ ist in einer Umgebung von $0$ beschränkt.
    \item
      $f_n$ ist in $0$ stetig.
    \item
      $f_n$ ist in $0$ differenzierbar und $f'_n(0) = 0$.
    \end{enumerate}
  \item \writtenex
    Für alle $n \in \set N_1$ ist die Funktion $f$ eine $n$-mal
    differenzierbare Funktion, und es existiert eine Polynomfunktion
    $P_n$ vom Grade $n - 1$ mit
    \[
    f^{(n)} = P_n \cdot f_{2n}.
    \]
    Daher (?) stellt die Taylorreihe von $f$ zum Entwicklungspunkt $0$
    die Funktion $f$ in keiner noch so kleinen Umgebung von $0$ dar.
  \end{enumerate}

\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
