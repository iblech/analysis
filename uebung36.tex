\documentclass{uebung}

\newcommand{\semester}{Wintersemester 2017/18}

\title{\bfseries 6.~Übung zur Analysis III}
\author{Prof.\ Dr.\ Marc\
  Nieper-Wißkirchen \and Caren\ Schinko, M.\ Sc.}
\date{20.~November
  2017\thanks{Die bearbeiteten Übungsblätter sind bis 12:00 Uhr am
    27.~November 2017 in den Analysis-Briefkasten einzuwerfen.}}

\begin{document}

\maketitle

\begin{enumerate}[start=249]

\item \writtenex
  Es seien $E$ und $F$ (reelle) Banachräume und $G$ eine offene
  Teilmenge von $E$.  Weiter sei $f\colon G \times \interval a b \to
  F$ eine stetige Funktion, welche nach der ersten Variablen $p$
  stetig differenzierbar sei.  Beweise, daß die Funktion
  \[
    I\colon G \to F, p \mapsto \int_a^b f(p, t) \, dt
  \]
  stetig differenzierbar ist, und in jedem Punkt $a \in G$ ihr
  Differential
  durch
  \[
    \forall v \in E\colon D_a I(v) = \int_a^b \frac{\partial
      f}{\partial p}(a, t) \cdot v \, dt
  \]
  gegeben ist.

  (Tip:  Sei $\epsilon \in \set R_+$.  1.~Schritt: Es existiert ein
  $\delta \in \set R_+$, so daß $U_\delta(a) \subseteq G$ und
  \[
    \forall (p, t) \in U_\delta(a) \times \interval a b\colon
    \anorm{\frac{\partial f}{\partial p}(p, t) - \frac{\partial
        f}{\partial p}(a, t)} \leq \frac{\epsilon}{b - a}.
  \]
  Wieso gilt das? Tubenlemma!  2.~Schritt:
  \[
    \forall (v, t) \in U_\delta(0) \times \interval a b \colon
    \anorm{f(a + v, t) - f(a, t) + \frac{\partial f}{\partial p}(a, t)
      \cdot v} \leq \frac{\epsilon}{b - a} \cdot \anorm v.
  \]
  Wieso? $g_t\colon v \mapsto f(a + v, t) - \frac{\partial f}{\partial
    p}(a, t) \cdot v$!
  3.~Schritt:
  \[
    \anorm{F(a + v) - F(a) - \int_a^b \frac{\partial f}{\partial p}(a,
      t) \cdot v \, dt} \leq ?.)
  \]

\item \oralex \textbf{Fundamentallemma der Variationsrechnung.}
  Es
  seien $I = \interval a b \subseteq \set R$ ein abgeschlossenes Intervall und $E$ ein
  (reeller) Banachraum.  Weiter betrachten wir den Untervektorraum
  \[
    \tilde E \coloneqq \{\xi \in \Diff 2(I, E) \mid \xi(a) = 0 \land
    \xi(b) = 0\}
  \]
  von $\Diff 2(I, E)$.  Es sei
  \[
    \omega\colon I \to \Lin(E; \set R), t \mapsto \omega_t
  \]
  eine stetige Funktion.  Gilt dann
  \[
    \forall \xi \in \tilde E\colon \int_a^b \omega_t(\xi(t)) \, dt =
    0,
  \]
  so ist $\omega = 0$, also $\omega_t \equiv 0$ für alle $t \in I$.

  (Tip:  Angenommen $\omega_{t_0} \neq 0$ für ein $t_0 \in
  \interval[open] a b$.  Dann existiert ein $v \in E$ mit
  $\omega_{t_0}(v) > 0$.  Dann existieren auch $c$, $d \in \set R$ mit
  $a \leq c < t_0 < d \leq b$, so daß $\forall t \in \interval c
  d\colon \omega_t (v) > 0$.  Nun erhälst Du einen Widenspruch zur
  Voraussetzung mit der zweimal stetig differenzierbaren (?) Funktion
  $\xi \in \tilde E$, die durch
  \[
    \xi(t) \coloneqq \begin{cases}
      (t - c)^3 \cdot (d - t)^3 \cdot v & \text{für $t \in \interval c
        d,$} \\
      0 & \text{sonst}
    \end{cases}
  \]
  definiert ist.)

\item \oralex Es sei $E = \prod_{k = 1}^n E_k$ ein Produkt reeller
  Banachräume, $F$ ein weiterer reeller Banachraum, $G \subseteq E$
  eine offene Teilmenge, $a \in G$ und $f\colon G \to F$ eine auf ganz
  $G$ differenzierbare Abbildung in einen Banachraum $F$, die in $a$
  sogar zweimal differenzierbar ist.  Zeige:
  \begin{enumerate}
  \item Für jedes $k = 1$, \dots, $n$ ist die Funktion
    \[
      \frac{\partial f}{\partial p_k}\colon G \to \Lin(E_k; F)
    \]
    in $a$ partiell nach $p_i$ differenzierbar.

    Wir definieren damit
    \[
      \frac{\partial^2 f}{\partial p_i \partial p_k} (a) \coloneqq
      \Phi_{1, 1}\Bigl(\frac{\partial}{\partial
        p_i}\Bigl(\frac{\partial f}{\partial p_k}\Bigr)\Bigr)(a) \in
      \Lin(E_i, E_k; F).
    \]
  \item
    Für $v_k \in E_k$ ist
    \[
      g\colon G \to F, p \mapsto \frac{\partial f}{\partial p_k}(p)
      \cdot v_k
    \]
    in $a$ partiell nach $p_i$ differenzierbar, und zwar mit
    \[
      \frac{\partial^2 f}{\partial p_i \partial p_k}(a) (v_i, v_k) =
      \frac{\partial g}{\partial p_i}(a) \cdot v_i.
    \]
  \item
    Es gilt folgende Verallgemeinerung des Schwarzschen
    Vertauschungssatzes:
    \[
      \forall i, k = 1, \dotsc, n\,
      \forall v_i \in E_i\,\forall v_k \in E_k\colon
      \frac{\partial^2 f}{\partial p_i \partial p_k}(a) (v_i, v_k)
      = \frac{\partial^2 f}{\partial p_k \partial p_i}(a) (v_k, v_i).
    \]
  \end{enumerate}
  (Tip: Aufgabe 229; insbesondere $\frac{\partial f}{\partial p_k}(p)
  \cdot v_k = D_p f(v)$ mit $v = (0, \dotsc, 0, v_k, 0, \dotsc, 0)$.)

\item \oralex
  \begin{enumerate}
  \item
    Sei $E$ ein reeller Banachraum, $G$ eine offene Teilmenge von $\set
    R \times E \times E$ und $I \subseteq \set R$ ein kompaktes
    Intervall.  Zeige:  Für $r \ge 1$ ist
    \[
      \Omega_r(G) \coloneqq \{\alpha \in \Diff r(I, E) \mid \forall t
      \in I\colon \hat\alpha(t) \coloneqq (t, \alpha(t), \alpha'(t))
      \in G\}
    \]
    ist eine offene Teilmenge von $\Diff r(I, E)$.

    (Tip: $K \coloneqq \hat\alpha(I)$, Aufgabe 1 aus Abschnitt 12.9.)
  \item
    Sei $G_0 \subseteq E$ eine konvexe Teilmenge, $J \subseteq \set R$
    ein offenes Intervall und $G \coloneqq J \times G_0 \times E$.
    Zeige, daß $\Omega_r(G)$ eine zusammenhängende Teilmenge von
    $\Diff r(J, E)$ ist.

    (Tip: Zu $\alpha$, $\beta \in \Omega_r(G)$ definiere $h(s, t)
    \coloneqq \alpha(t) + s \cdot (\beta(t) - \alpha(t))$.)
  \end{enumerate}

\item
  \begin{enumerate}
  \item \oralex Seien $G \colon \set R_+ \times \set R_+$ und $f$,
    $g\colon G \to \set R$ die Funktionen
    \begin{align*}
      f & \coloneqq x + y|G \\
      g & \coloneqq x^3 + y^3 - 3 x y|G.
    \end{align*}
    Bestimme alle lokalen Extrema von $f$ unter der Nebenbedingung $g(p) = 0$.
  \item \writtenex
    Bestimme den maximalen und den minimalen Wert der Funktion
    \[
      f\coloneqq 3x^2 + 3y^2 + z^2 + 2xy
    \]
    auf der Menge $\{p \in \set R^3 \mid x(p)^2 + y(p)^2 + z(p)^2 \leq
    4\}$.
  \end{enumerate}

\item
  Sei $E$ ein euklidischer Vektorraum mit $\dim E \ge 1$.  Weiter sei
  $A\colon E \to E$ ein selbstadjungierter Operator, also eine lineare
  Abbildung mit
  \[
    \forall v, w \in E\colon \scp{Av} w = \scp v {Aw}.
  \]
  Definieren wir
  \begin{align*}
    f&\colon E \to \set R, v \mapsto \scp{Av}{v}, \\
    g&\colon E \to \set R, v \mapsto \scp v v - 1, \\
    \set S(E)& \coloneqq \{v \in E\mid \scp v v = 1\},
  \end{align*}
  so gilt
  \begin{enumerate}
  \item
    Für alle $a \in \set S(E)$ ist $\grad_a g \neq 0$.
  \item
    Es ist $a$ genau dann ein kritischer Punkt von $f$ unter
    Nebenbedingung $g(p) = 0$, wenn $a$ ein normierter Eigenvektor von
    $A$ ist.
  \item
    Es ist $\max f(\set S(E))$ der größte und $\min f(\set S(E))$ der
    kleinste Eigenwert von $A$.
  \end{enumerate}

  Insbesondere haben wir gezeigt, daß jeder selbstadjungierte
  Endomorphismus auf $E$ reelle Eigenwerte besitzt.

\item \writtenex
  Sei $B$ eine nicht entartete symmetrische
  Bilinearform auf einem Vektorraum $E$ der Dimension $n + 1$.
  Zeige, daß die Quadrik
  \[
    Q \coloneqq \{p \in E \mid B(p, p) = 1\}
  \]
  eine $n$-dimensionale $\Diff \infty$-Untermannigfaltigkeit von $E$
  ist und daß gilt:
  \[
    \forall q \in Q\colon q + T_q Q = \{p \in E\mid B(p, q) = 1\};
  \]
  dabei ist $q + T_q Q = \{q + v \mid v \in T_q Q\}$.  (Wir "`heften"'
  also den Tangentialraum von $Q$ in $q$ an dem Punkte $q$ an.)  Man
  vergleiche dies mit der bekannten (?) Formel für die Tangente an die
  Ellipse
  $(x/a)^2 + (y/b)^2 = 1$.

\item \oralex
  Es sei
  \[
    f\colon \set R^2 \to \set R, (x, y) \mapsto \frac 1 {1 + x^2 +
      y^4}.
  \]
  Berechne die infinitesimale Änderung von $f$ in radialer Richtung,
  sowie in der dazu senkrechten Richtung; das soll heißen:  Berechne
  für $\tilde f \coloneqq f \circ h$ die partiellen Ableitungen
  $\frac{\partial \tilde f}{\partial r}$ und $\frac{\partial \tilde
    f}{\partial \phi}$; dabei ist
  \[
    h \colon \set R_+ \times R \to \set R^2, (r, \phi) \mapsto
    r \cdot (\cos(\phi), \sin(\phi))
  \]
  die Polarkoordinatenabbildung (vgl.\ Abschnitt~7.19 und Aufgabe 239 (a)).

\item
  \oralex \textbf{Über Egons schöne Beine.}
  Es war an einem wunderschönen Sommernachmittag, von dem wir zur Zeit
  nur träumen können, als der italienische Mathematiker Joseph-Louis
  Lagrange sich auf den Weg zur Universität machte.  Schon vor einiger
  Zeit war ihm zu Ohren gekommen, daß der schöne Egon wegen seiner
  langen Beine bei seinen Kommilitoninnen so beliebt war.  Heute
  wollte er sich selbst davon überzeugen und stellte sich, am Hörsaal
  angekommen, in die Nähe von Egon, um seine Beine genauer zu
  betrachten.  Dabei stellte er fest, daß es ungünstig war, zu weit
  von Egon entfernt zu sein, auber auch in der Nähe von Egon war nicht
  so gut möglich, seine Beine genau zu erkennen.

  Das brachte ihn auf die Idee, diejenige Entfernung zu ermitteln, bei
  der er Egons Beine möglichst groß sieht (d.\,h.~bei der der Winkel,
  unter dem Egons Beine ihm erscheinen, am größten ist).  Nach einem
  kurzen Blick in sein Analysis-Skript und sein Geometrie-Schulbuch
  hatte er eine einfache geometrische Lösung.

  \begin{center}
    \begin{tikzpicture}
      \draw (-1,0) -- (3,0); \draw (0,0) node [below] {$(0, 0)$}
      coordinate (E) -- (0,2) node [left] {$(0, L)$} coordinate (A);
      \draw (0,0) -- (2,3) node [right] {$(p_1, p_2)$} coordinate (C);
      \draw (2,3) -- (0,2); \draw (2,3) -- node [right] {$h$} (2,0);
      \pic [draw, ->, "$\phi(p)$", angle eccentricity=2.5]
      {angle=A--C--E};

    \end{tikzpicture}
  \end{center}

  (Tip: In der Skizze bezeichnet $\phi(p)$ für $p \in \set R^2$ den
  Winkel, unter dem Egons Beine von $p$ aus gesehen werden, $L$ die
  Lönge von $h$ die Augenhöhe von Lagrange.  Man skizziere $\{p \in
  \set R^2 \mid \phi(p) = c\}$ mit $c \in \interval[open] 0 \pi$ und
  $\{p \in \set R\mid y(p) - h = 0\}$.)

\item \oralex
  Sei eine Abbildung $f\colon X \to Y$ gegeben.  Zeige:
  \begin{enumerate}
  \item
    Ist
    $\mathfrak B$ eine $\sigma$-Algebra auf $Y$, so ist $\{f^{-1}(B)
    \mid B \in \mathfrak B\}$ eine $\sigma$-Algebra auf $X$.
  \item
    \label{it:image_alg}
    Ist $\mathfrak A$ eine $\sigma$-Algebra auf $X$, so ist $\{B \in
    \powerset{Y} \mid f^{-1}(B) \in \mathfrak A\}$ eine
    $\sigma$-Algebra auf $Y$.
  \item
    Sei $\mathfrak A$ eine $\sigma$-Algebra auf $X$ und $\mathfrak S$
    ein derartiges System von Teilmengen $B \subseteq Y$, so daß
    $f^{-1}(B) \in \mathfrak A$ für jedes $B \in \mathfrak S$ gilt.
    Dann gilt auch $f^{-1}(B) \in \mathfrak A$ für jedes $B \in
    \algebra {\mathfrak S}$.

    (Tip: Beachte~\ref{it:image_alg} und Beispiel (f) aus Abschnitt
    15.1.)
  \end{enumerate}

\item \writtenex Es muß wohl kurz vor Weihnachten und nach einem sehr
  langen Arbeitstag gewesen sein, als der Oberbürgermeister der Stadt
  Augsburg beschloß: "`Augsburg braucht ein neues Schwimmbad! Und zwar
  soll es die schnellste Rutschbahn haben, die möglich ist!"'

  Nun, als die Ingenieure der Stadt Augsburg über die Sache
  nachdachten, wurden sie gewahr, daß der Bürgermeister sie mit einer
  gar nicht so leichten Aufgabe konfrontiert hatte.  Ihnen war nicht
  klar, wie eine Rutschbahn auszusehen hatte, damit man auf ihr
  möglichst schnell rutscht.  Eine Gerade zwischen dem Start- und dem
  Endpunkt der Rutsche ist jedenfalls nicht die optimale Lösung
  (obwohl dies die kürzeste Verbindung zwischen den beiden Punkten
  ist).  Ist die Rutschbahn nämlich in der Nähe des Startpunktes
  steiler, so nimmt zwar die Länge der Rutschbahn zu, aber sie wird in
  einem größeren Teilstück schneller durchrutscht.  Aber wie nun mußte
  die genaue Form der Rutschbahn sein? Die Ingenieure wußten keinen
  Rat und wandten sich daher an die Hörer der Analysis III.  Jetzt
  sind Sie dran!

  \paragraph{Ein Abstecher in die Gefilde der Variationsrechnung: Das
    Bra\-chis\-to\-chro\-nen-Pro\-blem.}  Im Jahre 1696 veröffentlichte
  \name{Johann Bernoulli} das Bra\-chis\-to\-chro\-nen-Pro\-blem (griechisch für
  "`kürzeste Zeit"'): Es seien zwei Punkte $A$ und $B$ einer über dem
  Boden senkrechten Ebene vorgegeben, die nicht vertikal übereinander
  liegen.  Man finde, falls möglich, diejenige Kurve, die die beiden
  Punkte $A$ und $B$ so verbindet, daß ein Massenpunkt, der in $A$ mit
  Geschwindigkeit $0$ startet und längs dieser Kurve unter dem Einfluß
  der Schwerkraft unter Vernachlässigung von Reibungskräften von $A$
  nach $B$ gleitet, in der kürzest möglichen Zeit in $B$ eintrifft.

  \name{Johann Bernouilli} gab an, bereits eine Lösung zu kennen,
  forderte jedoch die großen Mathematiker seiner Zeit auf, ihrerseits
  das Problem zu lösen.

  Dieser neue Aufgabentypus führte zur Entwicklung einer neuen
  mathematischen Theorie: der Variationsrechnung.

  \paragraph{Zur Formulierung und Lösung des Problems.}
  Wir gehen davon aus, daß das Problem eine Lösung besitzt.  Wir
  betrachten die Ebene $\set R^2$ mit horizontaler $x$-Achse und
  vertikaler $y$-Achse, wobei die positive Richtung der $y$-Achse in
  Richtung des Erdmittelpunktes zeigt.

  Es sei $A = (0, 0)$ und $B = (a, b)$ mit $a > 0$ und $b \ge 0$
  (warum nicht $b < 0$?).  Wir nehmen an, daß die Spur der gesuchten
  Kurve durch den Graphen einer stetigen Funktion $\phi_0\colon
  \interval 0 a \to \set R$ beschrieben wird, daß also $\gamma\colon
  \interval 0 a \to \set R^2, t \mapsto (t, \phi_0(t))$ eine
  Parametrisierung der gesuchten Kurven ist, daß
  $\phi_0|\interval[open] 0 a \in \Diff 2(\interval[open] 0 a, \set
  R)$ und $\phi_0|\interval 0 a > 0$; somit befindet sich die Kurve
  abgesehen von Anfangs- und Endpunktpunkt unterhalb des Niveaus von
  $A$.

  Aus der Mechanik wissen wir, daß die "`Gleitzeit"' eines
  Massepunktes entlang einer Kurve $\interval 0 a \to \set R^2, t
  \mapsto (t, \phi(t))$ durch
  \[
    T(\phi) = \int_0^a \Bigl(\frac{1 + \phi'^2(x)}{2 g \cdot
      \phi(x)}\Bigr)^{\frac 1 2} \, dx
  \]
  gegeben ist, wobei $g = 9{,}81 m/s^2$ die Erdbeschleunigung ist.
  Hierbei handelt es sich um ein beiderseitig uneigentliches
  Integral.

  Wir definieren nun
  \[
    L\colon G \coloneqq \set R \times \set R_+ \times \set R \to \set
    R,
    (t, q, \dot q) \mapsto \Bigl(\frac{1 + \dot q^2} q\Bigr)^{1/2}
  \]
  und
  \[
    \Omega_2(G) \coloneqq \{\psi \in \Diff 2(\interval{t_0}{t_1}, \set
    R) \mid \psi > 0\}
  \]
  für alle $t_0$, $t_1 \in \set R$ mit $t_0 < t_1$ und
  \[
    S_2^{t_0, t_1}\colon \Omega_2(G) \to \set R, \phi \mapsto
    \frac 1 {\sqrt {2g}} \int_{t_0}^{t_1} L(t, \phi(t), \phi'(t)) \,
    dt.
  \]
  Schließlich definieren wir für jede auf einem Intervall
  $I \subseteq \set R$ definierte $\Diff 1$-Funktion
  $\phi\colon I \to \set R$ ihre \emph{Prolongation}
  \[
    \hat \phi \colon I \to I \times \set R \times \set R, t \mapsto
    (t, \phi(t), \phi'(t)).
  \]
  Zur Behandlung des Problems benötigen wir ein wesentliches Ergebnis
  aus der Variationsrechnung, das wir hier ohne Beweis angeben,
  welches sich aber ohne weiteres aus den Ergebnissen der Vorlesung
  herleiten läßt:

  \paragraph{Die Euler--Lagrangesche Differentialgleichung.}
  Die gesuchte Funktion $\phi_0$ erfüllt die folgende implizite
  Differentialgleichung zweiter Ordnung:
  \[
    \frac{d}{dt}\Bigl(\frac{\partial L}{\partial \dot q} \circ \hat
    \phi_0\Bigr) = \frac{\partial L}{\partial q} \circ \hat\phi_0.
  \]
  (Theoretischen Physikern ist diese Gleichung wohlvertraut.  Sie ist
  eine der wichtigsten Hilfsmittel zur Beschreibung des Verhaltens
  physikalischer Systeme.)

  Zeige nun:
  \begin{enumerate}
  \item
    \label{it:var1}
    Zu jeder Wahl von $s'$, $s'' \in \set R$ und $r \in \set R
    \setminus \{0\}$ existieren eindeutig bestimmte Parameter $c$, $d
    \in \set R$, so daß die Funktion $f = (x - r) \cdot (c x + d)
    \cdot x^3$ in $r$ die Ableitungen
    \[
      f'(r) = s'\quad\text{und}\quad f''(r) = s''
    \]
    hat.  Außerdem gilt für $f$, daß
    \[
      f(0) = f'(0) = f''(0) = f(r) = 0
    \]
    und für alle $t \in \interval 0 r$ gilt
    \[
      \abs{f'(t)} \leq M \cdot (\abs r \cdot \abs {s''} + \abs{s'})
    \]
    mit $M = 19$, also (?)
    \[
      \abs{f(t)} \leq M \cdot \abs r \cdot (\abs r \cdot \abs{s''} +
      \abs {s'}).
    \]
  \item
    Für alle $t_0$, $t_1 \in \interval[open]0 a$ mit $t_0 < t_1$ ist
    $\Diff 2$-Funktion $\phi_0|\interval{t_0}{t_1}$ ein Minimum von
    $S_2^{t_0, t_1}|\Omega_2(G; \phi_0(t_0), \phi_0(t_1))$ mit
    \[
      \Omega_2(G; h_0, h_1) \coloneqq \{\psi \in \Omega_2(G) \mid
      \psi(t_0) = h_0 \land \psi(t_1) = h_1\}.
    \]

    (Tip: Angenommen, $\phi_0|\interval{t_0}{t_1}$ sei kein Minimum
    von $S_2^{t_0, t_1}$.  Also existiert ein
    $\psi \in \Omega^{t_0, t_1}$ mit
    $\delta \coloneqq S_2^{t_0, t_1}(\phi|\interval{t_0}{t_1}) -
    S_2^{t_0, t_1}(\psi) > 0$.  Nun konstruiere (zu hinreichend
    kleinem $r \in \set R_+$) eine Funktion
    $\phi\colon \interval 0 a \to \set R$, die auf
    $\interval{t_0}{t_1}$ mit $\psi$ und auf
    $\interval 0 {t_0 - r} \cup \interval{t_1 + r} a$ mit $\phi_0$
    übereinstimmt, die auf $\interval{t_0 - r}{t_0}$ und
    $\interval {t_1}{t_1 + r}$ jeweils durch eine Summe von $\phi_0$
    mit einer Funktion $f$ wie in~\ref{it:var1} gegeben ist und deren
    Einschränkung $\phi|\interval[open] 0 a$ zweimal stetig
    differenzierbar ist.  Für genügend kleines $r$ ist (?) dann
    $T(\phi) < T(\phi_0)$.  Widerspruch!)
  \item
    Für die Funktion
    \[
      g\colon G \to \set R, (t, q, \dot q) \mapsto
      \frac{\partial L}{\partial \dot q}(t, q, \dot q) \cdot \dot q -
      L(t, q, \dot q)
    \]
    ist $g \circ \hat \phi_0$ konstant.

    (Tip: Verwende die Euler--Lagrangesche Differentialgleichung und
    über $L$ nur die Information, daß $L$ nicht von $t$ abhängt.)

    \paragraph{Bemerkung.}
    Dieser "`Erhaltungssatz"' ist ein Spezialfall des noetherschen
    Theorems:  Symmetrien von $L$ bedingen Erhaltungssätze.
  \item
    Die Funktion $\phi_0|\interval[open] 0 a$ ist eine Lösung der
    expliziten Differentialgleichung
    \begin{equation}
    \label{eq:dgl2}
      y'' = - \frac{1 + y'^2}{2 y}
    \end{equation}
    zweiter Ordnung; außerdem gibt es eine Konstante $c \in \set R_+$,
    so daß $\phi_0|\interval[open] 0 a$ eine Lösung der impliziten
    Differentialgleichung
    \begin{equation}
    \label{eq:dgl1}
      y \cdot (1 + y'^2) = c
    \end{equation}
    erster Ordnung ist.
    Im folgenden sei $c$ die so festgelegte Konstante.

    (Tip: Leite zunächst mit Hilfe des
    Erhaltungssatzes~\eqref{eq:dgl1} her und folgere daraus mit Hilfe
    der Euler--Lagrangeschen Differentialgleichung~\eqref{eq:dgl2}.)
  \item
    Für jede Lösung $\phi\colon J \to \set R$ von~\eqref{eq:dgl1}
    gilt:
    \begin{enumerate}
    \item
      $\phi(J) \subseteq \interval[open left] 0 c$ und $\phi'(t) = 0
      \iff \phi(t) = c$.
    \item
      Ist $\phi$ zweimal differenzierbar und $\phi \not\equiv c$, so
      löst $\phi$ auch die Differentialgleichung~\eqref{eq:dgl2} und
      nimmt $c$ höchstens an einer Stelle $t \in J$ an.
    \item
      Für alle $t_* \in \set R$ ist auch $t_* + J \to \set R, t
      \mapsto \phi(t - t_*)$ eine Lösung von~\eqref{eq:dgl1}.
    \end{enumerate}
  \item
    Für die Funktionen
    \[
      u_c \coloneqq \frac c 2 \cdot (x - \sin)\quad\text{und}\quad
      v_c \coloneqq \frac c 2 \cdot (1 - \cos)
    \]
    gilt:
    \begin{enumerate}
    \item
      $u_c$ besitzt eine Umkehrfunktionn $\check u_c\colon \set R \to
      \set R$.
    \item
      Die Funktion $\psi_c \coloneqq v_c \circ \check u_c\colon \set R
      \to \set R$ ist stetig und
      \[
        \psi_c(0) = 0,\quad \psi_c(c \pi/2) = c,\quad
        \psi_c(c \pi) = 0\quad\text{und}\quad\psi_c|\interval[open] 0
        {c \pi} > 0.
      \]
    \item
      $\psi_c|\interval[open] 0 {c\pi}$ ist eine $\Diff
      \infty$-Funktion mit $\lim_{t \to 0+} \psi'_c(t) = \infty$ und
      $\lim_{t \to c \pi-} \psi'_c(t) = -\infty$.
    \item
      $\psi_c|\interval[open] 0 {c\pi}$ ist eine nicht weiter
      fortsetzbare Lösung von~\eqref{eq:dgl1} und~\eqref{eq:dgl2}.
    \item
      Die Kurve $\alpha \coloneqq (u_c, v_c)$ ist die kanonische
      Parameterisierung einer Zykloide und daher parameterisiert $t
      \mapsto (t, \psi_c(t))$ dieselbe Zykloide.
    \end{enumerate}
  \item
    Eine $\Diff 2$-Funktion $\phi\colon J \to \set R$ mit $\phi
    \not\equiv c$ ist genau dann einer Lösung von~\eqref{eq:dgl1},
    wenn es ein $t_* \in \set R$ gibt, so daß $\phi$ eine
    Einschränkung von $\interval[open]{t_*, t_* + c\pi} \to \set R,
    t \mapsto \psi_c(t - t_*)$ ist.

    (Tip: Zu einem beliebig vorgegebenem $s \in J$ existiert ein
    $\tilde s \in \interval [open] 0 {c \pi}$, so daß $\phi'(s) =
    \psi'_c(\tilde s)$ ist; wegen~\eqref{eq:dgl1} ist auch $\phi(s)
    = \psi_c(\tilde s)$.  Daher (?) gilt die Behauptung
    wegen~\eqref{eq:dgl2} mit $t_* \coloneqq s - tilde s$.)
  \item
    Es ist $\phi_0 = \psi_c|[0, a]$.  Daher ist die gesuchte
    Brachistochrone ein Teilbogen einer Zykloide.
  \item
    Für alle $t_0$, $t_1 \in \interval[open] 0 {c \pi}$ mit $t_0 <
    t_1$ gilt
    \[
      \int_{t_0}^{t_1} L \circ \hat\psi_c = \int_{t_0}^{t_1}
      \frac{\sqrt c}{\psi_c} = \sqrt{c} \cdot \bigl(\check u_c (t_1)
      - \check u_c(t_0)\bigr).
    \]
    Daher beträgt die "`Gleitzeit"' längs des vollständigen
    Zykloidenbogens $T(\psi_c) = \sqrt{2 c/g} \cdot \pi$.
  \item
    Wie lange benötigt der Oberbürgermeister, wenn er entlang eines
    vollständigen Zykloidenbogens rutscht, deren Endpunkte $A$ und
    $B$ insgesamt $100 \, m$ voneinander entfernt liegen; wie tief
    gerät er dabei unter das "`Nullniveau"'; und wie groß ist die
    erreichte Maximalgeschwindigkeit?  (Benutze den
    Energieerhaltungssatz.)
  \item
    Warum wurden die Pläne des Augsburger Oberbürgermeisters trotz
    allem nicht realisiert?
    \begin{enumerate}
    \item
      Man kann sich am Anfang der Rutschbahn nicht hinsetzen.
    \item
      Rutschen macht Spaß; deshalb sollte es lange dauern.
    \item
      Die Stadt Augsburg spart, indem sie die Materialkosten
      minimiert.
    \item
      Um dafür Sorge zu tragen, daß die Reibungskräfte
      vernachlässigt werden können, wird zuviel Schmierseife benötigt.
    \end{enumerate}
  \end{enumerate}
\end{enumerate}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
